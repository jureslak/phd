% !TeX spellcheck = en_US
\documentclass{beamer}

\usefonttheme[onlymath]{serif}

\usepackage[slovene]{babel}    % slovenian language and hyphenation
\usepackage[T1]{fontenc}       % make čšž work on output
\usepackage[utf8]{inputenc}
\usepackage{url}               % \url and \href for links
\usepackage{units}
\usepackage{framed}
\usepackage{bibentry}

\newtheorem{izrek}{Izrek}
\newtheorem{posledica}{Posledica}

\theoremstyle{definition}
\newtheorem{definicija}{Definicija}
\newtheorem{opomba}{Opomba}
\newtheorem{zgled}{Zgled}

% basic sets
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
% \renewcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\T}{\ensuremath{\mathsf{T}}}

\newcommand{\X}{\mathcal{X}}

% greek letters
\let\oldphi\phi
\let\oldtheta\theta
\newcommand{\eps}{\varepsilon}
\renewcommand{\phi}{\varphi}
\renewcommand{\theta}{\vartheta}

% vektorska analiza
\newcommand{\grad}{\operatorname{grad}}
\newcommand{\rot}{\operatorname{rot}}
\renewcommand{\div}{\operatorname{div}}
\newcommand{\dpar}[2]{\frac{\partial #1}{\partial #2}}

\renewcommand{\b}{\boldsymbol}

\usepackage{minted}

\usetheme[outer/progressbar=foot]{metropolis}

\institute{
  Doctoral thesis defense
  \\

  24.\ 9.\ 2020}
\title{Adaptive RBF-FD method \\ (Adaptivna RBF-FD metoda)}
\date{}
\author{Jure Slak}
\hypersetup{pdftitle={Adaptive RBF-FD method},
pdfauthor={Jure Slak}, pdfsubject={}, pdfkeywords={}}  % setup pdf metadata

\newcommand{\ijs}{\hfill JSI \raisebox{-4pt}{\includegraphics[height=16pt]{ijs_logo_inverse.pdf}}}

% \pagestyle{empty}              % vse strani prazne
% \setlength{\parindent}{0pt}    % zamik vsakega odstavka
% \setlength{\parskip}{10pt}     % prazen prostor po odstavku
\setlength{\overfullrule}{0pt}  % oznaci predlogo vrstico z veliko črnine
%\usepackage{libertine}

\definecolor{Purple}{HTML}{911146}
\definecolor{Orange}{HTML}{CF4A30}
\definecolor{Dark}{HTML}{333333}
\setbeamercolor{frametitle}{bg=Dark}

\setbeamercolor{alerted text}{fg=Orange}
\setbeamercolor{normal text}{fg=Dark}

\metroset{block=fill}

\renewcommand{\b}{\boldsymbol}
\newcommand{\x}{\b{x}}
\newcommand{\s}{\b{s}}
\newcommand{\p}{\b{p}}
\newcommand{\f}{\b{f}}
\newcommand{\g}{\b{g}}
\renewcommand{\t}{\b{t}}
\renewcommand{\u}{\b{u}}
\renewcommand{\L}{\mathcal{L}}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\begin{frame}{Overview \ijs}
  \begin{itemize}
    \setlength{\parskip}{10pt}
    \item PhD: \url{http://e6.ijs.si/~jslak/files/phd/phd.pdf}
    \item Repository (includes this presentation): \\
     {\small \url{https://gitlab.com/jureslak/phd}}
    \item RBF-FD is a numerical method for solving partial differential
    equations
    \item Thesis overview
    \begin{enumerate}
      \item RBFs and their properties
      \item Local strong form operator approximations
      \item Domain discretization generation
      \item Fully automatic adaptivity
      \item Implementation
    \end{enumerate}
  \end{itemize}
\end{frame}


\begin{frame}{Classification of RBF-FD \ijs}
  \vspace{-0.5cm}
\begin{table}
  \hspace*{-0.75cm}
  \renewcommand*{\arraystretch}{1.7}
\begin{tabular}{p{4cm}||c|c}
  & mesh/grid-based & meshless \\ \hline \hline
  strong form \newline $\nabla^2u = f$ & FDM & FPM, \textbf{RBF-FD}, GFDM \\
  \hline
  \rule[-4pt]{0pt}{12pt}weak form \newline $\displaystyle \int\!\!\nabla
  u\!\cdot\!\nabla
  v\,d\Omega=\int\!\! fv\,d\Omega$
  &
  FEM, IGA & \hspace{-39pt} BEM \hspace{20pt} EFG, MLPG
  \\
\end{tabular}
\end{table}
\vspace{-0.45cm}
\begin{figure}
  \includegraphics[width=0.35\linewidth]{fig/domain-mesh.png}
  \includegraphics[width=0.35\linewidth]{fig/domain-meshless.png}
\end{figure}
\end{frame}


\begin{frame}{RBF-FD method: RBFs \ijs}
  RBF: a function of the form $\phi(\|x\|)$
  \begin{figure}
    \includegraphics[width=0.65\linewidth]{fig/rbfs.png}
%    \includegraphics[width=0.45\linewidth]{fig/domain_theoretical.pdf}
  \end{figure}
  RBFs have desirable mathematical properties.
\end{frame}

\begin{frame}{RBF-FD method: operator approximations \ijs}
  Strong form approximations:
  \[
    (\L u)(p) \approx \sum_{i=1}^n w_i u(p_i)
  \]
  Enforce exactness for a class of functions:
  \begin{equation*}
  \renewcommand*{\arraycolsep}{1pt}
  \begin{bmatrix}
  \phi(\|p_1 - p_1\|) &\cdots & \phi(\|p_n - p_1\|)  \\
  \vdots & \ddots & \vdots    \\
  \phi(\|p_1 - p_n\|) &\cdots & \phi(\|p_n - p_n\|)
  \end{bmatrix}
  \begin{bmatrix}
  w_1 \\ \vdots \\ w_n
  \end{bmatrix}
  =
  \begin{bmatrix}
    \L\phi(\|p-p_1\|) \\
    \vdots \\
    \L\phi(\|p-p_n\|) \\
  \end{bmatrix},
  \end{equation*}
  Obtain $\b w_{\L, p} \approx \L|_p$.

  Solvable?  \textbf{Yes!}
\end{frame}

\begin{frame}{RBF-FD method: accuracy and conditioning \ijs}
  Sometimes computable in closed form: {\footnotesize
  \[
    \textstyle w=\left\{\frac{4 h^2 e^{\frac{3 h^2}{\sigma ^2}}}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2},-\frac{2 \left(\sigma ^2 e^{\frac{4 h^2}{\sigma ^2}}+e^{\frac{2 h^2}{\sigma ^2}} \left(4 h^2-2 \sigma
    ^2\right)+\sigma ^2\right)}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2},\frac{4 h^2 e^{\frac{3 h^2}{\sigma ^2}}}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2}\right\}
  \]}
  Approximation error:
  \vspace{-1ex}
  \begin{figure}
    \includegraphics[width=0.7\linewidth]{fig/cantilever_beam_conv.pdf}
  \end{figure}
\end{frame}

\begin{frame}{RBF-FD method: accuracy and conditioning \ijs}
  A solution:

  \begin{itemize}
    \item Polyharmonic RBF $\phi(r) = r^k$ and monomial augmentation
  \end{itemize}

  \begin{figure}
    \includegraphics[width=0.9\linewidth]{fig/phs-error.pdf}
  \end{figure}

  \vspace{-10pt}

  Challenges: computational time, stability wrt.\ nodal positioning
\end{frame}

\begin{frame}{Solving a boundary value problem \ijs}
  \begin{enumerate}
    \item Discretization
    \item Weights
    \item Linear system
  \end{enumerate}

  Solution $(N \approx 10^5)$:

  \includegraphics[width=1\linewidth,trim=0 0 0 160,
  clip]{fig/cantilever_beam_with_holes.png}
\end{frame}

\begin{frame}{The goal}
  Adaptive solution procedure:

  \begin{enumerate} \setlength{\parskip}{0pt}
    \item Discretize the domain
    \item Solve the problem
    \item Estimate the error
    \item If the error is below threshold, return the solution.
    \item Otherwise: Refine the discretization
    \item Go to 2
  \end{enumerate}
\end{frame}

\begin{frame}{Domain discretization}
  Discretization = boundary nodes + internal nodes + stencils

  Requirements:
  \begin{itemize}
    \item Sufficient quality
    \item Variable density
    \item 2D and 3D (and more?)
    \item Boundaries and interiors
    \item Computational complexity
  \end{itemize}

  \textbf{Result:} new algorithms for node generation
  \setlength{\FrameSep}{5pt}
  \begin{framed}
  {\footnotesize
  J.\ Slak and G.\ Kosec, \emph{On generation of node distributions for
  meshless PDE discretizations}, SIAM J.\ Sci.\ Comput.\
  \textbf{41}(5):A3202–A3229,
  2019.}
  \end{framed}
  \vspace{-12pt}
  \begin{framed}
  {\scriptsize
   U. Duh, G. Kosec, and J. Slak, \emph{Fast variable density node generation
  on para\-metric surfaces with applications to mesh-free methods},
  arXiv:2005.08767, 2020}
  \end{framed}
\end{frame}

\begin{frame}{Node generation in domain interior \ijs}
  \href{http://e6.ijs.si/~matjaz/projects/parallel-node-fill/latest/}{DEMO}
  \vspace{-0.8cm}
  \begin{center}
    \includegraphics[height=4cm]{fig/nodes_initial.pdf}
    \includegraphics[height=4cm]{fig/nodes_candidates.pdf}
    \includegraphics[height=4cm]{fig/nodes_progress.pdf}
    \includegraphics[height=4cm]{fig/nodes_final.pdf}
  \end{center}
\end{frame}

\newcommand{\blambda}{\b{\lambda}}
\renewcommand{\r}{\b{r}}
\begin{frame}{Time complexity and spacing guarantees\ijs}
  \textbf{Time complexity:} using a structure $S$:
  \[
  T_S(N) =  I_S(N) + O(N(T_h + n_c(T_\Omega + Q_S(N)))) + N I_S(N)
  \]

  $k$-d tree: $O(N\log N)$, $k$-d grid (uniform data): $O(N)$ \\

  \textbf{Spacing guarantees:} minimal spacing proven. Main result:
  \begin{align*}
    |\Delta h(\blambda, \s)| &\le \frac{\sqrt{d_\Lambda}}{2} h_M^2
    \frac{\sigma_{1,M}
      (\nabla\nabla\r)}{\sigma_{d_\Lambda, m}^2(\nabla\r)},
  \end{align*}
  where \vspace*{-5mm}
  \begin{align*}
    h_M^2 &= \max_{\blambda \in \Lambda} h(\r(\blambda))^2,\\
    \sigma_{1,M} (\nabla\nabla\r) &= \max_{i=1,\ldots,d_\Lambda}
    \max_{\blambda
      \in \Lambda} \sigma_1((\nabla\nabla r_i)(\blambda)), \\
    \sigma_{d_\Lambda, m}(\nabla\r) &= \min_{\blambda \in \Lambda}
    \sigma_{d_\Lambda}(\nabla \r(\blambda)),
  \end{align*}

  Maximal spacing / coverage proofs: future work
\end{frame}

%\begin{frame}{Execution time \ijs}
%  \includegraphics[width=0.47\linewidth]{fig/node_set_time2.pdf}
%  \includegraphics[width=0.47\linewidth]{fig/node_set_time3.pdf}
%\end{frame}

\begin{frame}{Generalization to boundaries and other surfaces \ijs}

  More general embedded manifolds: non-orientable, co-dimension \\

  \includegraphics[width=0.43\linewidth]{fig/manifolds.png}
  \includegraphics[width=0.55\linewidth]{fig/mobius.pdf}
\end{frame}


\begin{frame}{Variable density \ijs}
  \begin{center}
    \includegraphics[width=0.35\linewidth]{fig/earth_example_1.png}
    \includegraphics[width=0.35\linewidth]{fig/earth_example_2.png}
    \includegraphics[height=3.5cm]{fig/domain_3d_var_all.pdf}
    \includegraphics[height=3.5cm]{fig/domain_3d_var_bnd.pdf}
    \includegraphics[height=4cm]{fig/domain_3d_var_param.pdf}
  \end{center}
\end{frame}


\begin{frame}{RBF-FD sensitivity to generated nodes \ijs}

  \begin{figure}[h]
    \centering
    \includegraphics[width=0.49\linewidth]{fig/node_set_accuracy_2d.pdf}
    \includegraphics[width=0.49\linewidth]{fig/node_set_accuracy_3d.pdf}
  \end{figure}
  \vspace{-5mm}
  Solving a Poisson problem with RBF-FD under uniform discretization.
  \vspace{5mm}

  \textbf{Takeaway:} The node placing and the method work well
  together.
\end{frame}

\begin{frame}{Spectra of the differentiation matrices \ijs}

  \begin{figure}[h]
    \centering
    \includegraphics[width=0.49\linewidth]{fig2/lap_matrix_3d.png}
    \includegraphics[width=0.49\linewidth]{fig2/eigs_3d.png}
  \end{figure}

  Eigenvalues have negative real parts and small imaginary parts.
\end{frame}

\begin{frame}{Adaptivity \ijs}
  With robust node generation, we can move on to adaptivity.

  \textbf{Result:} A fully automatic $h$-adaptive ``re-meshing'' procedure
  for RBF-FD.

  \setlength{\FrameSep}{5pt}
  \begin{framed}
  {\footnotesize
  J. Slak and G. Kosec. \emph{Refined Meshless Local Strong Form
  solution of Cauchy–Navier equation on an irregular domain},
  Eng. Anal. Bound. Elem. \textbf{100}:3--13, 2019.}
  \end{framed}
  \vspace{-10pt}
  \begin{framed}
  {\footnotesize
  J. Slak and G. Kosec, \emph{Adaptive radial basis
  function-generated
  finite differences method for contact problems}, Int. J Numer. Methods Eng.
  \textbf{119}(7):661–686, 2019.}
  \end{framed}
\end{frame}

\newcommand{\hde}{h_{\text{d}}}
\newcommand{\hre}{h_{\text{r}}}
\newcommand{\ade}{\alpha_{\text{d}}}
\newcommand{\are}{\alpha_{\text{r}}}
\newcommand{\ede}{\eps_{\text{d}}}
\newcommand{\ere}{\eps_{\text{r}}}
\renewcommand{\it}[1]{^{(#1)}}
\newcommand{\eh}{\hat{e}}
\begin{frame}{Adaptive procedure \ijs}
  Adapt the spacing function, not the discretization.

  \begin{equation*} \label{eq:local-spac-mod}
  h_i\it{j+1} := \max\{\min\{h_i\it{j} / f_i\it{j}, \hde(\x_i) \},
  \hre(\x_i)\},
  \end{equation*}
  where the density increase factor $f_i\it{j}$ is defined as
  \begin{equation*}
  \label{eq:adapt-rho-factor}
  f_i\it{j} = \begin{cases}
  1 + \frac{\ede - \eh_i\it{j}}{\ede - m\it{j}} (\frac{1}{\ade} - 1), &
  \eh_i\it{j}
  \leq \, \ede, \quad \\
  1, & \ede < \eh_i\it{j} < \ere,  \quad \\
  1 + \frac{\eh_i\it{j} - \ere}{M\it{j} - \ere} (\are - 1), & \eh_i\it{j} \geq
  \ere, \quad
  \end{cases}
  \end{equation*}
\end{frame}

\begin{frame}{Fully automatic adaptivity -- sample \ijs}
\includegraphics[width=0.9\linewidth]{fig/adaptivity-sample.png}
\end{frame}

\begin{frame}{Fully automatic adaptivity -- final spacing \ijs}
  \includegraphics[width=0.8\linewidth]{fig/adaptive_demo_final_distr.pdf}

  Spacing adapts to the error (proportional to 2nd derivative).
\end{frame}

\begin{frame}{Classical examples \ijs}
  $L$-shaped domain and Fichera corner -- good for analysis

  \includegraphics[width=0.45\linewidth]{fig/lshape_anal.png}
%  \includegraphics[width=0.4\linewidth]{fig/fichera_anal.png}
\hspace{2mm}
%\includegraphics[width=0.4\linewidth]{fig/Lshape_adaptive_alpha_analysis_err.pdf}
\includegraphics[width=0.45\linewidth]{fig/Lshape_adaptive_alpha_analysis_first_mid.pdf}
\end{frame}

\begin{frame}{Hertzian contact \ijs}
\includegraphics[height=3.7cm]{fig/hertzian.pdf}
\includegraphics[height=2.7cm]{fig2/hertzian_solution_deformed_vm_noaxes.png}

$H \approx 1923 a$

Closed form solution is complicated, but known.
\end{frame}

\begin{frame}{Hertzian contact \ijs}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles1.pdf}
\includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles2.pdf}
\includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles3.pdf}
\includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles4.pdf}
\end{frame}

\begin{frame}{Hertzian contact \ijs}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles5_zoom.pdf}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles6_zoom.pdf}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles7_zoom.pdf}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles8_zoom.pdf}
\end{frame}

\begin{frame}{Hertzian contact \ijs}
  \parbox[b]{0.5\linewidth}{
  \includegraphics[width=\linewidth]{fig/hertzian_manual_density.png}
  \includegraphics[width=\linewidth]{fig/hertzian_adaptive_density.png}
}\parbox[t]{0.5\linewidth}{
  \includegraphics[width=0.9\linewidth]{fig/hertzian_adaptive_hist.pdf}
}
Nodes with $\rho_i < 1$ cover 97\% of the domain, but 95\% of all nodes
are in $[-3a, 3a] \times [-3a, 0]$ (which is 0.000027\% of domain area).
\end{frame}

\begin{frame}{Technical aspects \ijs}
\textbf{Result:}
an open source Medusa library for solving PDEs with strong-form methods.
Submitted to TOMS, response:
minor rev.

\begin{framed}
J. Slak and G. Kosec, \emph{Medusa: A C++ library for solving PDEs using
strong form mesh-free methods}, arXiv:1912.13282
\end{framed}

\vspace{2mm}

\begin{columns}
  \begin{column}{0.2 \linewidth}
    \includegraphics[width=\linewidth]{fig/logo-medusa.png}
  \end{column}
  \begin{column}{0.7\linewidth}
    \textbf{\Large Medusa} \\
    Coordinate Free  Meshless Method implementation \\
    DEMO: \url{http://e6.ijs.si/medusa/}
  \end{column}
\end{columns}
\end{frame}

\begin{frame}{Dimensional independence \ijs}
Sample problem:
\setlength{\FrameSep}{-10pt}
\begin{framed}
\begin{align*}
  8 \, (-1, -1, -1) \cdot \nabla u + 2 \, \nabla^2 u &= -1
  &&\text{ in } \Omega \\
  u &= 0 &&\text{ on } \partial\Omega
\end{align*}
\end{framed}

C++ source: \\[3mm]
\includegraphics[width=0.9\linewidth]{fig2/code.pdf}

{\scriptsize (see p.\ 128 for full code)}

\end{frame}

\begin{frame}{Dimensional independence \ijs}
  \centering
  \includegraphics[height=3.5cm]{fig2/adv-dif-1d.pdf} \hspace{4mm}
  \includegraphics[height=3.5cm]{fig2/adv-dif-2d-surf.pdf} \\[2mm]
  \includegraphics[height=3.5cm]{fig2/adv-dif-3d-iso.png} \hspace{4mm}
  \includegraphics[height=3.5cm]{fig2/adv-dif-3d-slice.png}
\end{frame}

\begin{frame}{Future work \ijs}
  \begin{itemize}
    \item Node generation from CAD models
    \item Parallel node generation
    \item Proofs on upper bound on $h_{X, \Omega}$
    \item Partial discretization modification
    \item Error indicators
    \item Approximation types RBF-FD vs.\ WLS
    \item Effect of stencil size
    \item Sensitivity to nodal positions (scattered uniform)
    \item Sensitivity to nodal positions (gradual density increase)
    \item $h$-based adaptivity instead of $k$-nn
    \item $hp$-adaptivity
    \item Time-dependent equations
  \end{itemize}
\end{frame}

\begin{frame}{ \ijs}
  \centering
  \scalebox{2}{Thank you for your attention.}
\end{frame}

\end{document}
% vim: syntax=tex
% vim: spell spelllang=sl
% vim: foldlevel=99
% Latex template: Jure Slak, jure.slak@gmail.com

