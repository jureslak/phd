% !TeX spellcheck = en_US
\documentclass{beamer}

\usefonttheme[onlymath]{serif}

\usepackage[slovene]{babel}    % slovenian language and hyphenation
\usepackage[T1]{fontenc}       % make čšž work on output
\usepackage[utf8]{inputenc}
\usepackage{url}               % \url and \href for links
\usepackage{units}
\usepackage{bibentry}

\newtheorem{izrek}{Izrek}
\newtheorem{posledica}{Posledica}

\theoremstyle{definition}
\newtheorem{definicija}{Definicija}
\newtheorem{opomba}{Opomba}
\newtheorem{zgled}{Zgled}

% basic sets
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
% \renewcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\T}{\ensuremath{\mathsf{T}}}

\newcommand{\X}{\mathcal{X}}

% greek letters
\let\oldphi\phi
\let\oldtheta\theta
\newcommand{\eps}{\varepsilon}
\renewcommand{\phi}{\varphi}
\renewcommand{\theta}{\vartheta}

% vektorska analiza
\newcommand{\grad}{\operatorname{grad}}
\newcommand{\rot}{\operatorname{rot}}
\renewcommand{\div}{\operatorname{div}}
\newcommand{\dpar}[2]{\frac{\partial #1}{\partial #2}}

\renewcommand{\b}{\boldsymbol}

\usepackage{minted}

\usetheme[outer/progressbar=foot]{metropolis}

\institute{
  Presentation of thesis results before the final defence
  \\
  
  8.\ 7.\ 2020}
\title{Adaptive RBF-FD method \\ (Adaptivna RBF-FD metoda)}
\date{}
\author{Jure Slak}
\hypersetup{pdftitle={Adaptive RBF-FD method},
pdfauthor={Jure Slak}, pdfsubject={}, pdfkeywords={}}  % setup pdf metadata

\newcommand{\ijs}{\hfill JSI \raisebox{-4pt}{\includegraphics[height=16pt]{ijs_logo_inverse.pdf}}}

% \pagestyle{empty}              % vse strani prazne
% \setlength{\parindent}{0pt}    % zamik vsakega odstavka
% \setlength{\parskip}{10pt}     % prazen prostor po odstavku
\setlength{\overfullrule}{0pt}  % oznaci predlogo vrstico z veliko črnine
%\usepackage{libertine}

\definecolor{Purple}{HTML}{911146}
\definecolor{Orange}{HTML}{CF4A30}
\definecolor{Dark}{HTML}{333333}
\setbeamercolor{frametitle}{bg=Dark}

\setbeamercolor{alerted text}{fg=Orange}
\setbeamercolor{normal text}{fg=Dark}

\metroset{block=fill}

\renewcommand{\b}{\boldsymbol}
\newcommand{\x}{\b{x}}
\newcommand{\s}{\b{s}}
\newcommand{\p}{\b{p}}
\newcommand{\f}{\b{f}}
\newcommand{\g}{\b{g}}
\renewcommand{\t}{\b{t}}
\renewcommand{\u}{\b{u}}
\renewcommand{\L}{\mathcal{L}}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\begin{frame}{Overview \ijs}
  \begin{itemize}
    \setlength{\parskip}{10pt}
    \item Thesis repo (includes this presentation): \\
     {\small \url{https://gitlab.com/jureslak/phd}}
    \item Thesis overview 
    \begin{enumerate}
      \item RBFs and their properties
      \item Local strong form operator approximations
      \item Domain discretization generation
      \item Fully automatic adaptivity
      \item Implementation
    \end{enumerate}
    \item Main results (chapters 3, 4, and 5)
    \begin{itemize}
      \item Node generation algorithms
      \item $h$-adaptivity for RBF-FD
      \item Implementation
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Classification of RBF-FD \ijs}
\begin{table}
  \renewcommand*{\arraystretch}{1.7}
\begin{tabular}{c||c|c}
  & mesh/grid-based & meshless \\ \hline \hline 
  strong-form & FDM & FPM, RBF-FD, GFDM \\ \hline 
  weak form & FEM, IGA & \hspace{-35pt} BEM \hspace{20pt} EFG, MLPG 
  \\   
\end{tabular}
\end{table}
\begin{figure}
  \includegraphics[width=0.35\linewidth]{fig/domain-mesh.png}
  \includegraphics[width=0.35\linewidth]{fig/domain-meshless.png}
\end{figure}
\end{frame}


\begin{frame}{RBF-FD method: RBFs \ijs}
  RBF: function of form $\phi(\|x\|)$
  \begin{figure}
    \includegraphics[width=0.65\linewidth]{fig/rbfs.png}
%    \includegraphics[width=0.45\linewidth]{fig/domain_theoretical.pdf}
  \end{figure}
  [Wen04] H. Wendland, \emph{Scattered data approximation}, Cambridge 
  Monographs on 
  Applied and Computational Mathematics \textbf{17}, Cambridge University 
  Press, 2004
\end{frame}

\begin{frame}{RBF-FD method: operator approximations \ijs}
  Strong form approximations:
  \[
    (\L u)(p) \approx \sum_{i=1}^n w_i u(p_i)
  \]
  Enforce exactness for a class of functions:
  \begin{equation*}
  \renewcommand*{\arraycolsep}{1pt}
  \begin{bmatrix}
  \phi(\|p_1 - p_1\|) &\cdots & \phi(\|p_n - p_1\|)  \\
  \vdots & \ddots & \vdots    \\
  \phi(\|p_1 - p_n\|) &\cdots & \phi(\|p_n - p_n\|)
  \end{bmatrix}
  \begin{bmatrix}
  w_1 \\ \vdots \\ w_n
  \end{bmatrix}
  =
  \begin{bmatrix}
    \L\phi(\|p-p_1\|) \\
    \vdots \\
    \L\phi(\|p-p_n\|) \\
  \end{bmatrix},
  \end{equation*}
  Obtain $\b w_{\L, p} \approx \L|_p$.
  
  Solvable?  \textbf{Yes!}
\end{frame}

\begin{frame}{RBF-FD method: accuracy and conditioning}
  Sometimes computable in closed form: {\footnotesize
  \[
    \textstyle w=\left\{\frac{4 h^2 e^{\frac{3 h^2}{\sigma ^2}}}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2},-\frac{2 \left(\sigma ^2 e^{\frac{4 h^2}{\sigma ^2}}+e^{\frac{2 h^2}{\sigma ^2}} \left(4 h^2-2 \sigma
    ^2\right)+\sigma ^2\right)}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2},\frac{4 h^2 e^{\frac{3 h^2}{\sigma ^2}}}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2}\right\}  
  \]}
  Approximation error:
  \[\textstyle
    w^\T \b u =  u''(x_0)+h^2 \left(\frac{u(x_0)}{\sigma ^4}+\frac{u''(x_0)}{\sigma ^2}+\frac{1}{12} u^{(4)}(x_0)\right)+O\left(h^3\right)
  \]
  \vspace{-4ex}
  \begin{figure}
    \includegraphics[width=0.7\linewidth]{fig/cantilever_beam_conv.pdf}
  \end{figure}
\end{frame}

\begin{frame}{RBF-FD method: accuracy and conditioning}
  Some proposed solutions: 
  
  \begin{itemize}
    \item Stable computation of $\b w_{\L, p}$
    \item Polyharmonic RBF $\phi(r) = r^k$ and monomial augmentation
  \end{itemize}
  
  \begin{figure}
    \includegraphics[width=0.8\linewidth]{fig/phs-error.pdf}
  \end{figure} 

  \vspace{-10pt} 

  Challenges: computational time, stability wrt.\ nodal positioning
\end{frame}

\begin{frame}{The goal}
  Develop a fully automatic adaptive solution procedure for RBF-FD.
  
  \begin{enumerate} \setlength{\parskip}{0pt}
  \item Discretize the domain
  \item Solve the problem
  \item Estimate the error
  \item If the error is below threshold, return the solution. Else:
  \item Refine the discretization
  \item Go to 2    
\end{enumerate}
  
  Many different ways of estimation and refinement.
  
  Our choice:
  $h$-refinement, ``re-meshing'' approach, gradient-based error indicators
\end{frame}

\begin{frame}{Domain discretization}
  Domain discretization: boundary nodes + internal nodes + stencils
  
  Stencils: $k$ closest nodes
  
  Domain generation algorithm requirements:
  \begin{itemize}
    \item Sufficient quality
    \item Variable density
    \item 2D and 3D (and more?)
    \item Boundaries and interiors
    \item Computational complexity
  \end{itemize}  
 
  \textbf{Result:} new algorithms for node generation
  
  {\footnotesize
  J.\ Slak and G.\ Kosec, \emph{On generation of node distributions for 
  meshless PDE discretizations}, SIAM J.\ Sci.\ Comput.\  41(5):A3202–A3229, 
  2019. \\  
  U. Duh, G. Kosec, and J. Slak, \emph{Fast variable density node generation 
  on parametric surfaces with application to mesh-free methods},
  arXiv:2005.08767, 2020}
  
\end{frame}

\begin{frame}{Node set regularity \ijs}
  For uniform nodes:
  \begin{definition}[Fill distance]
    \[
    h_{X, \Omega} = 2 \sup_{\x\,\in\,\Omega} \,\min_{j = 1, \ldots, n}
    \|\x-\x_j\|
    \]
  \end{definition}
  
  \begin{definition}[Separation distance]
    \[
    s_X = \min_{1 \,\leq\, i \,<\, j \,\leq\, n} \|\x_i - \x_j\|.
    \]
  \end{definition}
  \begin{definition}[Node set ratio]
  \begin{equation*}
  \gamma_{X, \Omega} =  \frac{h_{X, \Omega}}{s_X}.
  \end{equation*}
\end{definition}
\end{frame}

\begin{frame}{Node set regularity \ijs}
  Approximation theorems: convergence: $h_{X, \Omega}$, stability: $s_X$.
  \begin{center}
  \includegraphics[width=6cm]{fig/node_set_fill_sep_dist_demo.pdf}
  \end{center}
\end{frame}


\begin{frame}{Node generation in domain interior \ijs}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        \item Enqueue the ``seed nodes''
        \item In each iteration, remove a node $p$ 
        and generate candidates at a radius $h(p)$.
        \item Enqueue the accepted candidates
        \item Repeat until the queue is empty
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \includegraphics[width=\linewidth]{fig/pds.pdf}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Node generation in domain interior \ijs}
  \begin{center}
    \includegraphics[height=4cm]{fig/nodes_initial.pdf}
    \includegraphics[height=4cm]{fig/nodes_candidates.pdf}
    \includegraphics[height=4cm]{fig/nodes_progress.pdf}
    \includegraphics[height=4cm]{fig/nodes_final.pdf}
  \end{center}
\end{frame}


\begin{frame}{Time complexity and finiteness\ijs}
  \textbf{Time complexity:} using a structure $S$:
  \[
  T_S(N) =  I_S(N) + O(N(T_h + n_c(T_\Omega + Q_S(N)))) + N I_S(N)
  \]
  
  $k$-d tree: $O(N\log N)$, $k$-d grid (uniform data): $O(N)$ \\
  
  \textbf{Finiteness:}
  Bounded domain and positive $h$ are not enough. $h$ must be bounded away 
  from 0.
\end{frame}


\begin{frame}{Minimal spacing \ijs}
  \begin{columns}
    \begin{column}{0.49\linewidth}
      Assume seed notes are valid.
      Constant $h$:
      \[
      \|p_i - p_j\| \geq h
      \]
      \begin{theorem}[Minimal spacing guarantee]
        \[
        \|p_i - p_j\| \geq h(p_{\beta(j)})
        \]
      \end{theorem}
      
      Other options $h$:
      \[
      \|p_i - p_j\| \geq \max(h_i, h_j), \min(h_i, h_j), h_i, h_j
      \]
    \end{column}
    \begin{column}{0.49\linewidth}
      \includegraphics[width=\linewidth]{fig/candidates_randomized_forest.pdf}
    \end{column}
  \end{columns}
\end{frame}


\renewcommand{\r}{\b{r}}
\begin{frame}{Examples \ijs}
  \includegraphics[width=0.4\linewidth]{fig/domain_2d.pdf}
  \includegraphics[width=0.55\linewidth]{fig/domain_3d.png}
  \begin{align*}
  r_2(\varphi) &= \frac14 (3+\cos(3\varphi), \
  r_3(\varphi, \vartheta) = \frac14 \left(3+ \frac{\vartheta^2 (\pi -
    \vartheta)^2}{4} (2 +\cos(3\varphi))\right) \\
  \r_2(\varphi) &= \begin{bmatrix}
  r_2(\varphi) \cos(\varphi) \\
  r_2(\varphi) \sin(\varphi)
  \end{bmatrix}\!\!,\
  \r_3(\varphi, \vartheta) = \begin{bmatrix}
  r_3(\varphi, \vartheta) \sin(\vartheta) \cos(\varphi) \\
  r_3(\varphi, \vartheta) \sin(\vartheta) \sin(\varphi) \\
  r_3(\varphi, \vartheta) \cos(\vartheta)
  \end{bmatrix}
  \end{align*}
\end{frame}

\begin{frame}{Quasi-uniformity \ijs}
  \includegraphics[width=0.47\linewidth]{fig/node_set_fill_sep_dist_2d.pdf}
  \includegraphics[width=0.47\linewidth]{fig/node_set_fill_sep_dist_3d.pdf}
  %\end{frame}
  
  %\begin{frame}{Primerjava \ijs}
  \includegraphics[width=0.47\linewidth]{fig/node_set_fill_qu_2d.pdf}
  \includegraphics[width=0.47\linewidth]{fig/node_set_fill_qu_3d.pdf}
\end{frame}

\begin{frame}{Execution time \ijs}
  \includegraphics[width=0.47\linewidth]{fig/node_set_time2.pdf}
  \includegraphics[width=0.47\linewidth]{fig/node_set_time3.pdf}
\end{frame}

\renewcommand{\s}{\vec{s}}
\begin{frame}{Generalization to surfaces \ijs}
  Regular parametrization:
    \[
  \r\colon\Lambda
  \subseteq \R^{d_\Lambda} \to \partial\Omega \subseteq \R^d
  \]
  
  Place parameters, map to points.
  
  \includegraphics[width=0.9\linewidth]{fig/surface_node_placing.pdf}
  \[
  \r(\b \mu) = \r(\b\lambda + \alpha \s) =
  \r(\b \lambda) + \alpha \nabla\r(\b\lambda)\s + \b R(\b\lambda, \alpha, \s)
  %    \approx
  %    \r(\b \lambda) + \alpha \nabla\r(\b\lambda)\s,
  \]
\end{frame}


\begin{frame}{Generalization to surfaces \ijs}
  Finding the distance $\alpha$:
  \[
  h(\r(\b\lambda)) = \|\r(\b\lambda) - \r(\b\lambda) - \alpha
  \nabla\r(\b\lambda)\s\|
  = \alpha \| \nabla\r(\b\lambda)\s\|,
  \]
  \[
  \b\mu = \b\lambda + \frac{h(\r(\b\lambda))}{\|\nabla\r(\b\lambda)\s\|}\s,
  \quad  \alpha = \frac{h(\r(\b\lambda))}{\|\nabla\r(\b\lambda)\s\|}.
  \]
  Actual distance:
  \[
  \hat h(\b\lambda, \s) = \left\|\r(\b\lambda) - \r\left(\b\lambda +
  \frac{h(\r(\b\lambda))}{\|\nabla\r(\b\lambda)\s\|}\s\right)\right\|.
  \]
\end{frame}

\begin{frame}{Sample execution \ijs}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_domain_1.pdf}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_domain_2.pdf}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_domain_3.pdf}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_domain_4.pdf}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_domain_5.pdf}
  
  \includegraphics[width=0.2\linewidth]{fig/execution_illustration_param_1.pdf}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_param_2.pdf}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_param_3.pdf}
  \includegraphics[width=0.19\linewidth]{fig/execution_illustration_param_4.pdf}
  \includegraphics[width=0.18\linewidth]{fig/execution_illustration_param_5.pdf}
\end{frame}


\begin{frame}{Further extensions \ijs}
  More general embedded manifolds: non-orientable, co-dimension \\
  
  \includegraphics[width=0.43\linewidth]{fig/manifolds.png}
  \includegraphics[width=0.55\linewidth]{fig/mobius.pdf}
\end{frame}

\newcommand{\blambda}{\b{\lambda}}
\begin{frame}{Minimal spacing \ijs}
    \begin{align*}
    |\Delta h(\blambda, \s)| &\le  \max_{\blambda \in \Lambda} \left(
    \frac{\sqrt{d_\Lambda}}{2}
    h(\p)^2
    \frac{
      \displaystyle \max_{i=1,\ldots,d_\Lambda}
      \max_{\b \zeta \in \bar B(\blambda, \rho_{\blambda}) \cap \Lambda}
      \sigma_1((\nabla\nabla r_i)(\b \zeta))}{\sigma_{d_\Lambda}(\nabla
      \r(\blambda))^2}\right) \\
    &\le \frac{\sqrt{d_\Lambda}}{2} h_M^2 \frac{\sigma_{1,M}
      (\nabla\nabla\r)}{\sigma_{d_\Lambda, m}^2(\nabla\r)},
    \end{align*}
    where \vspace*{-1cm}
    \begin{align*}
    h_M^2 &= \max_{\blambda \in \Lambda} h(\r(\blambda))^2,\\
    \sigma_{1,M} (\nabla\nabla\r) &= \max_{i=1,\ldots,d_\Lambda} 
    \max_{\blambda
      \in \Lambda} \sigma_1((\nabla\nabla r_i)(\blambda)), \\
    \sigma_{d_\Lambda, m}(\nabla\r) &= \min_{\blambda \in \Lambda}
    \sigma_{d_\Lambda}(\nabla \r(\blambda)),
    \end{align*}
\end{frame}

%\begin{frame}{Minimal spacing in practice \ijs}
%  
%\includegraphics[width=0.49\linewidth]{fig/node_set_fill_sep_dist_bnd_int_2d.pdf}
%  
%\includegraphics[width=0.49\linewidth]{fig/node_set_fill_sep_dist_bnd_int_3d.pdf}
%\end{frame}

\begin{frame}{Variable density \ijs}
  \begin{center}
    \includegraphics[width=0.35\linewidth]{fig/earth_example_1.png}
    \includegraphics[width=0.35\linewidth]{fig/earth_example_2.png}
    \includegraphics[height=3.5cm]{fig/domain_3d_var_all.pdf}
    \includegraphics[height=3.5cm]{fig/domain_3d_var_bnd.pdf}
    \includegraphics[height=4cm]{fig/domain_3d_var_param.pdf}
  \end{center}
\end{frame}

\begin{frame}{Other aspects \ijs}
  Also explored in the thesis:
  \begin{itemize}
    \item Node quality for surface node placing
    \item Node quality for variable density distributions
    \item Better node quality measures
    \item Execution time for variable density distributions
    \item Time complexity for surface nodes
  \end{itemize}
  One final item: Behavior of the method on generated nodes.
\end{frame}

\begin{frame}{PDE accuracy \ijs}
  
  \begin{figure}[h]
    \centering
    \includegraphics[width=0.49\linewidth]{fig/node_set_accuracy_2d.pdf}
    \includegraphics[width=0.49\linewidth]{fig/node_set_accuracy_3d.pdf}
  \end{figure}
  Solving a Poisson problem in $\Omega_2$ and
  $\Omega_3$ using RBF-FD with $\phi(r) = 
  r^3$, $n$ closest neighbor stencils and uniform discretization.
  Each gray point is one run.
  
  The node placing algorithm and the method work well together.
\end{frame}

\begin{frame}{Adaptivity \ijs}
  With robust node generation, we can move on to adaptivity.
  
  \textbf{Result:} A fully automatic $h$-adaptive ``re-meshing'' procedure 
  for RBF-FD.
  
  [SK19a] J. Slak and G. Kosec. \emph{Refined Meshless Local Strong Form 
  solution of Cauchy–Navier equation on an irregular domain} ,
  Engineering analysis with boundary elements \textbf{100}:3--13, 2019.
  
  [SK19b] J. Slak and G. Kosec, \emph{Adaptive radial basis 
  function-generated 
  finite differences method for contact problems}, International Journal for 
  Numerical Methods in
  Engineering \textbf{119}(7):661–686, 2019.
\end{frame}

\newcommand{\hde}{h_{\text{d}}}
\newcommand{\hre}{h_{\text{r}}}
\newcommand{\ade}{\alpha_{\text{d}}}
\newcommand{\are}{\alpha_{\text{r}}}
\newcommand{\ede}{\eps_{\text{d}}}
\newcommand{\ere}{\eps_{\text{r}}}
\renewcommand{\it}[1]{^{(#1)}}
\newcommand{\eh}{\hat{e}}
\begin{frame}{Adaptive procedure \ijs}
  Adapt the spacing function, not the discretization. 
  
  \begin{equation*} \label{eq:local-spac-mod}
  h_i\it{j+1} := \max\{\min\{h_i\it{j} / f_i\it{j}, \hde(\x_i) \}, 
  \hre(\x_i)\},
  \end{equation*}
  where the density increase factor $f_i\it{j}$ is defined as
  \begin{equation*}
  \label{eq:adapt-rho-factor}
  f_i\it{j} = \begin{cases}
  1 + \frac{\ede - \eh_i\it{j}}{\ede - m\it{j}} (\frac{1}{\ade} - 1), &
  \eh_i\it{j}
  \leq \, \ede, \quad \\
  1, & \ede < \eh_i\it{j} < \ere,  \quad \\
  1 + \frac{\eh_i\it{j} - \ere}{M\it{j} - \ere} (\are - 1), & \eh_i\it{j} \geq
  \ere, \quad
  \end{cases}
  \end{equation*}
\end{frame}

\begin{frame}{Refinement \ijs}
  Testing how the method behaves with variable density.
    
  \includegraphics[width=0.48\linewidth]{fig/hertzian_manual_density.png}
  \includegraphics[width=0.48\linewidth]{fig/hertzian_solution_deformed_vm.png}
  
  \includegraphics[width=0.52\linewidth]{fig/fwo_sample_contour.png}
  \includegraphics[width=0.45\linewidth]{fig/cantilever_beam_with_holes.png}
  
  \footnotesize
  [SK19] Slak, Jure, and Gregor Kosec. ``Refined Meshless Local Strong Form 
solution of Cauchy–Navier equation on an irregular domain.'' 
\emph{Engineering analysis with boundary elements} 100 (2019): 3--13.
\end{frame}

\begin{frame}{Fully automatic adaptivity -- sample \ijs}
\includegraphics[width=0.9\linewidth]{fig/adaptivity-sample.png}
\end{frame}

\begin{frame}{Fully automatic adaptivity -- final spacing \ijs}
  \includegraphics[width=0.8\linewidth]{fig/adaptive_demo_final_distr.pdf}

  Spacing adapts to the error (proportional to 2nd derivative).
\end{frame}

\begin{frame}{Classical examples \ijs}
  $L$-shaped domain and Fichera corner.
  
  \includegraphics[width=0.4\linewidth]{fig/lshape_anal.png}
  \includegraphics[width=0.5\linewidth]{fig/fichera_anal.png}
  
  Not the cases where adaptivity truly shines, because we still get 
  convergence with uniform refinement. Good for analysis.
\end{frame}

\begin{frame}{Analysis \ijs}
  \includegraphics[width=0.4\linewidth]{fig/Lshape_adaptive_alpha_analysis_err.pdf}
  \hspace{2mm}
  \includegraphics[width=0.45\linewidth]{fig/Lshape_adaptive_alpha_analysis_node_den.pdf}
  \\[2mm]
  \includegraphics[width=0.4\linewidth]{fig/Lshape_adaptive_alpha_analysis_first_mid.pdf}
  \hspace{2mm}
  \includegraphics[width=0.47\linewidth]{fig/Lshape_adaptive_alpha_analysis_iter_time.pdf}
\end{frame}

\begin{frame}{Compressed disk \ijs}
  \hspace{1cm}
  \includegraphics[width=0.25\linewidth]{fig/disk.pdf}
  \includegraphics[width=0.40\linewidth]{fig/disk_001_errors.pdf}
  
  \includegraphics[width=0.4\linewidth]{fig/disk_adaptive_001_profiles.pdf}
  \includegraphics[width=0.4\linewidth]{fig/disk_adaptive_001_profiles_zoom.pdf}
\end{frame}

\begin{frame}{Hertzian contact \ijs}
\includegraphics[height=4.5cm]{fig/hertzian.pdf}

$H \approx 1923 a$

Closed form solution is complicated, but known.
\end{frame}

\begin{frame}{Hertzian contact \ijs}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles1.pdf}
\includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles2.pdf}
\includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles3.pdf}
\includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles4.pdf}
\end{frame}

\begin{frame}{Hertzian contact \ijs}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles5_zoom.pdf}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles6_zoom.pdf}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles7_zoom.pdf}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_adaptive_profiles8_zoom.pdf}
  \end{frame}

\begin{frame}{Hertzian contact \ijs}
  \includegraphics[height=4cm]{fig/hertzian_adaptive_error_vs_N.pdf}
\includegraphics[height=4cm]{fig/hertzian_adaptive_error_iter.pdf}
\includegraphics[height=4cm]{fig/hertzian_adaptive_error_only_N.pdf}

Derefinement works. Extreme refinement, distance ratio of 3 million.
\end{frame}

\begin{frame}{Hertzian contact \ijs}
  \parbox[b]{0.5\linewidth}{
  \includegraphics[width=\linewidth]{fig/hertzian_manual_density.png}
  \includegraphics[width=\linewidth]{fig/hertzian_adaptive_density.png}
}\parbox[t]{0.5\linewidth}{
  \includegraphics[width=0.9\linewidth]{fig/hertzian_adaptive_hist.pdf}
}
Nodes with $\rho_i < 1$ cover 97\% of the domain, but 95\% of all nodes
are in $[-3a, 3a] \times [-3a, 0]$ (which is 0.000027\% of domain area).
\end{frame}

\begin{frame}{3D case -- point contact \ijs}
\includegraphics[width=0.9\linewidth]{fig/3d-adaptive.png}
\end{frame}

\begin{frame}{Technical aspects}
\textbf{Result:} 
an open source Medusa library for solving PDEs with strong-form methods. 
Submitted to TOMS, response: 
minor rev.

[SK20] J. Slak and G. Kosec, Medusa: A C++ library for solving PDEs using 
strong form mesh-free methods, arXiv:1912.13282 \\[2ex]

\begin{columns}  
  \begin{column}{0.2 \linewidth}
    \includegraphics[width=\linewidth]{fig/logo-medusa.png}
  \end{column}
  \begin{column}{0.7\linewidth}
    \textbf{\Large Medusa} \\ 
    Coordinate Free  Meshless Method implementation \\
    \url{http://e6.ijs.si/medusa/}
  \end{column}
\end{columns}
\vspace{0.5cm}
More details about the design and further examples in the thesis.
\end{frame}


\begin{frame}{Future work \ijs}
  \begin{itemize}
    \item Node generation from CAD models
    \item Parallel node generation
    \item Proofs on upper bound on $h_{X, \Omega}$
    \item Partial discretization modification
    \item Error indicators
    \item Approximation types RBF-FD vs.\ WLS
    \item Effect of stencil size
    \item Sensitivity to nodal positions (scattered uniform)
    \item Sensitivity to nodal positions (gradual density increase)
    \item $h$-based adaptivity instead of $k$-nn
    \item $hp$-adaptivity
    \item Time-dependent equations
  \end{itemize}
  
  Thank you for your attention. <jjNaturally, feel free to ask questions.
\end{frame}

\end{document}
% vim: syntax=tex
% vim: spell spelllang=sl
% vim: foldlevel=99
% Latex template: Jure Slak, jure.slak@gmail.com

