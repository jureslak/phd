# Adaptive RBF-FD method

**Abstract**
Radial-basis-function-generated finite differences (RBF-FD) is a method for
solving partial differential equations (PDEs), which is developed into a
fully automatic adaptive method during the course of this work. RBF-FD is a
strong form meshless method, which means that it does not require a mesh of
the problem domain, but uses  only a set of nodes as the basis for the
discretization. A large part of this PhD is dedicated to algorithms for
meshless node generation. A new algorithm
for construction of variable density meshless discretizations in
arbitrary spatial dimensions is developed. It can generate points in the
interior and on the boundary, has provable minimal spacing requirements,
can generate $N$ points in $O(N\log N)$ time and the resulting node sets are
compatible with RBF-FD. This algorithm is used as the basis of a newly
proposed $h$-adaptive procedure for elliptic problems. The behavior of the
procedure is analyzed on classical 2D and 3D adaptive Poisson problems.
Furthermore, several contact problems from linear elasticity are solved,
demonstrating successful adaptive derefinement and refinement, with the
densest parts of the discretization being more than a million times denser than
the coarsest. Finally, the software developed for this work and broader
research is presented and published online as an open source library for
solving PDEs with strong form methods.

This repository includes the document produces in pursuit of the degree.
These include (in chronological order):

1. disposition (thesis topic proposal)
1. topic presentation (public presentation of the thesis proposal)
1. the main results and the text of the thesis
1. the presentation of the main results
1. final thesis defense

and are located in their corresponding folders.
The main results are located under 3 in the `analyses/` folder. Refer
to the README there for further explanation.

Jure Slak, 2020
