auto op_v = storage.explicitVectorOperators();
auto op_s = storage.explicitOperators();
for (int time_step = 0; time_step < max_steps; ++time_step) {
    for (int i : interior) {
        u2[i] = u1[i] + dt * (-1 / rho * op_s.grad(p, i) + mu / rho * op_v.lap(u1, i) - op_v.grad(u1, i) * u1[i]);
    }
    for (int i : all) {
        p[i] = p[i] - dt * dl*dl*rho*op_v.div(u2, i);
    }
    u1.swap(u2);
}

