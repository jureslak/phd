#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/BasicRelax.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <Eigen/IterativeLinearSolvers>
using namespace mm;

template <int dim>
void solve() {
    typedef Vec<double, dim> vec;  // Define the point type.
    BallShape<vec> c(0.0, 1.0), hole(0.1, 0.3); // Define the domain shape.

    // Discretize the domain. The nodal spacing is increased in higher dimensions
    // to keep the number of nodes small enough.
    double dx = 0.001*dim*dim*dim;
    DomainDiscretization<vec> domain = (c-hole).discretizeBoundaryWithStep(dx);
    GeneralFill<vec> fill; fill(domain, dx);
    int N = domain.size(); prn(N);
    Monomials<vec> mon(2);  // Monomial basis.
    domain.findSupport(FindClosest(2*mon.size()));  // Stencil size = 2*(number of mon.)

    // Construct the approximation.
    RBFFD<Polyharmonic<double, 3>, vec, ScaleToClosest> approx({}, mon);
    auto storage = domain.template computeShapes<sh::lap|sh::d1>(approx);

    // Define matrices and construct the operators interface.
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs(N); rhs.setZero();
    auto op = storage.implicitOperators(M, rhs);
    M.reserve(storage.supportSizes());

    // Write the discretized equations into M and rhs.
    for (int i : domain.interior()) {
        8.0*op.grad(i, -1) + 2.0*op.lap(i) = -1.0;
    }
    for (int i : domain.boundary()) {
        op.value(i) = 0.0;
    }

    // Solve the system.
    Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
    solver.preconditioner().setDroptol(1e-4);
    solver.preconditioner().setFillfactor(20);
    solver.compute(M);
    ScalarFieldd u = solver.solve(rhs);   // The solution as a scalar field of doubles.

    // Save the solution.
    std::ofstream out_file(format("example_%dd_data.m", dim));
    out_file << "positions = " << domain.positions() << ";" << std::endl;
    out_file << "solution = " << u << ";" << std::endl;
    out_file.close();
}

int main() {
    solve<1>();
    solve<2>();
    solve<3>();
    return 0;
}

