Eigen::SparseMatrix<double, Eigen::RowMajor> M(2*N, 2*N);
Eigen::VectorXd rhs(2*N); rhs.setZero();
M.reserve(shapes.supportSizesVec());

auto op = shapes.implicitVectorOperators(M, rhs);

for (int i : domain.interior()) {
    (lam+mu)*op.graddiv(i) + mu*op.lap(i) = 0.0;
}
for (int i : domain.types() == RIGHT) {
    double y = domain.pos(i, 1);
    op.value(i) = {(P*y*(3*D*D*(1+nu) - 4*(2+nu)*y*y)) / (24.*E*I), -(L*nu*P*y*y) / (2.*E*I)};
}
for (int i : domain.types() == LEFT) {
    double y = domain.pos(i, 1);
    op.traction(i, lam, mu, {-1, 0}) = {0, -P*(D*D - 4*y*y) / (8.*I)};
}
for (int i : domain.types() == TOP) {
    op.traction(i, lam, mu, {0, 1}) = 0.0;
}
for (int i : domain.types() == BOTTOM) {
    op.traction(i, lam, mu, {0, -1}) = 0.0;
}
