prepare

casename = 'point_contact_3d';
file = ['data/' casename '.h5'];

info = h5info(file);

a = h5readatt(file, '/conf', 'num.a');
P = h5readatt(file, '/conf', 'case.P');
E = h5readatt(file, '/conf', 'case.E');
nu = h5readatt(file, '/conf', 'case.nu');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');
e = h5readatt(file, '/conf', 'num.eps');

ncases = length(info.Groups)-1;
name = sprintf('/%03d', ncases-1)

pos = h5read(file, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);

stress = h5read(file, [name, '/stress']);
sxx = stress(:, 1);
syy = stress(:, 2);
szz = stress(:, 3);
sxy = stress(:, 4);
sxz = stress(:, 5);
syz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);


f = 3;
I = -f*e < x & x < -e & -f*e < y & y < -e & -f*e < z & z < -e;

lpos = [x(I), y(I), z(I)];
sv = sv(I);

save([plotdatapath casename '_lastiter.mat'], 'lpos', 'sv', 'e', 'a', 'f');

