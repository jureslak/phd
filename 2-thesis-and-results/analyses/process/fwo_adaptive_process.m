prepare

casename = 'fwo_adaptive';

file = ['data/' casename '.h5'];
info = h5info(file);

lam = h5readatt(file, '/conf', 'phy.lam');
mu = h5readatt(file, '/conf', 'phy.mu');
p0 = h5readatt(file, '/conf', 'phy.p0');
a = h5readatt(file, '/conf', 'phy.a');
nu = h5readatt(file, '/conf', 'phy.nu');

ng = length(info.Groups) - 1;

data.niter = ng;
data.profiles = {};
for g = 1:ng
    gname = info.Groups(g).Name;
    
    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);
    data.iters{g}.pos = pos;
    data.iters{g}.vm = von_mises(sxx, syy, sxy, nu);

    f = 3;
    if g <= 2, f = 10; end
    I = -f*a < x & x < f*a & y > -1e-10;    
    [sortx, SI] = sort(x(I));
    lsxx = sxx(I);
    sortsxx = lsxx(SI);
    data.profiles{g}.sxx = sortsxx;
    data.profiles{g}.x = sortx;
    
    [~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);
    rho = -log10(d/max(d));
    rhomax = max(rho);
    
    ref = h5readatt(file, gname, 'ref');
    deref = h5readatt(file, gname, 'deref');
    same = h5readatt(file, gname, 'same');
    ref_limit = h5readatt(file, gname, 'ref_limit');
    deref_limit = h5readatt(file, gname, 'deref_limit');
    fprintf('%d & %d & %d & %d & %d & %d & %d & %.2f\\\\ \n',...
        g, N, ref, ref_limit, same, deref, deref_limit, rhomax);
end

save([plotdatapath casename '.mat'], 'data', 'a', 'p0');

