prepare

d = 3;
casename = sprintf('eigen_specter_%dd', d);
file = [datapath casename '_wip.h5'];
h5info(file);


ord = h5readatt(file, '/conf', 'approx.aug');
n = h5readatt(file, '/conf', 'approx.n');
types = h5read(file, '/d/types');
I = types > 0;

data = h5read(file, '/M');
M = spconvert(data);
M = M(I, I);
[i, j, v] = find(M);
v = sign(v).*log(abs(v));

Mv = sign(M).*log(abs(M));
N = length(M)

f1 = setfig('b1', [400 400]);
scatter(i, j, 2, v, '.k');
daspect([1 1 1])
xlim([-inf, inf])
ylim([-inf, inf])
xlabel('');
ylabel('');
tk = [1 1000 2000 3000 N];
xticks(tk);
yticks(tk);
set(gca,'TickDir','out');
axis ij
title(sprintf('%dD, order $= %d$, $n = %d$', d, ord, n));

exportf(f1, [imagepath 'lap_matrix_' num2str(d) 'd.png'], '-m4');
pause


lam = eigs(M, length(M));
save([plotdatapath sprintf('eigs_%d.mat', d)], 'lam', 'ord', 'n');

setfig('a4');
scatter(real(lam), imag(lam), 15, 'kx');
