prepare

casename = 'hertzian_adaptive';
% casename = 'disk_uniform_001';

file = ['data/' casename '.h5'];
info = h5info(file);

lam = h5readatt(file, '/conf', 'phy.lam');
mu = h5readatt(file, '/conf', 'phy.mu');
p0 = h5readatt(file, '/conf', 'phy.p0');
a = h5readatt(file, '/conf', 'phy.a');
H = h5readatt(file, '/conf', 'case.H');

ng = length(info.Groups) - 1;

% independent points
den = 1000;
f = 3;
X = linspace(-f*a, f*a, 2*den);
Y = zeros(size(X));

[ASXX, ~, ~] = hertzian_analytical(X, Y, a, p0);

nerr = 3;
l1 = 1;
energy = 2;
linf = 3;
errleg = {'$e_1$', '$e_E$', '$e_\infty$'};

n = h5readatt(file, '/conf', 'approx.n');

data.x = X;
data.asxx = ASXX;
data.profiles = {};
data.niter = ng;

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
%     fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    f = 3;
    if g <= 2, f = 15; end
    I = -f*a < x & x < f*a & y > -1e-10;    
    [sortx, SI] = sort(x(I));
    lsxx = sxx(I);
    sortsxx = lsxx(SI);
    data.profiles{g}.sxx = sortsxx;
    data.profiles{g}.x = sortx;
end

save([plotdatapath casename '_profiles.mat'], 'data', 'a', 'p0');
