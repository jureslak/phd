prepare

casename = 'hertzian_adaptive';

file = ['data/' casename '.h5'];
info = h5info(file);

lam = h5readatt(file, '/conf', 'phy.lam');
mu = h5readatt(file, '/conf', 'phy.mu');
p0 = h5readatt(file, '/conf', 'phy.p0');
a = h5readatt(file, '/conf', 'phy.a');
H = h5readatt(file, '/conf', 'case.H');

ng = length(info.Groups) - 1;

gname = sprintf('/%03d', ng-1)
pos = h5read(file, [gname '/domain/pos']);
x = pos(:, 1);
y = pos(:, 2);
N = length(x)

[~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);

den = 250;
f = 4;
xx = linspace(-f*a, f*a, 2*den);
yy = linspace(-f*a, 0, den);
[X, Y] = meshgrid(xx, yy);

rho = -log10(d/max(d));
RHO = griddata(x, y, rho, X, Y);

data.x = xx;
data.y = yy;
data.rho = RHO;
data.maxd = max(rho)


f = 3;
sum(-f*a < x & x < f*a & -f*a < y)/N


edges = linspace(0, 7, 100);
[counts, ~, bin] = histcounts(rho, edges);
save([plotdatapath casename '_density.mat'], 'data', 'a', 'p0', 'edges', 'counts');


setfig('a4');
rough_edges = [0 1 2 3 4 5 6 7];
histogram(rho, rough_edges);

nbin = length(rough_edges) - 1;
[counts, ~, bin] = histcounts(rho, rough_edges);
xx = linspace(-H, H, 1000);
yy = linspace(-H, 0, 500);
[X, Y] = meshgrid(xx, yy);
B = griddata(x, y, bin, X, Y);
B = int32(B);
levelcounts = zeros(nbin, 1);
for i = 1:numel(B)
    levelcounts(B(i)) = levelcounts(B(i)) + 1;
end
levelcounts/sum(levelcounts)*100

