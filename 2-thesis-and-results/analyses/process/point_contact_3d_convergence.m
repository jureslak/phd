prepare

casename = 'point_contact_3d';
file = ['data/' casename '.h5'];

info = h5info(file);

a = h5readatt(file, '/conf', 'num.a');
P = h5readatt(file, '/conf', 'case.P');
E = h5readatt(file, '/conf', 'case.E');
nu = h5readatt(file, '/conf', 'case.nu');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');
e = h5readatt(file, '/conf', 'num.eps');

f = 3;
[AX, AY, AZ] = meshgrid(deal(linspace(max(-f*e, -1), -e, 100)));

ncases = length(info.Groups) - 1;

Ns = zeros(ncases, 1);

nerr = 5;
ul1 = 1;
ulinf = 2;
senergy = 3;
sl1 = 4;
slinf = 5;
errleg = {'$e_1^{(j)}(\vec{u})$', '$e_\infty^{(j)}(\vec{u})$', '$e_E$',...
          '$e_1^{(j)}(\sigma)$', '$e_\infty^{(j)}(\sigma)$'};

errors = zeros(ncases, nerr);

for i = 1:ncases-1

name = sprintf('/%03d', i-1);

pos = h5read(file, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);
Ns(i) = length(pos);

types = h5read(file, [name '/domain/types']);

sol = h5read(file, [name '/displ']);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);
dnorm = sum(sol.^2, 2);

stress = h5read(file, [name, '/stress']);
sxx = stress(:, 1);
syy = stress(:, 2);
szz = stress(:, 3);
sxy = stress(:, 4);
sxz = stress(:, 5);
syz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

[AU, AV, AW, ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ] = point_contact_3d_analytical(AX, AY, AZ, P, E, nu);
asxx = ASXX(:); asyy = ASYY(:); aszz = ASZZ(:); asxy = ASXY(:); asxz = ASXZ(:); asyz = ASYZ(:);
astress = [asxx asyy aszz asxy asxz asyz];
ASV = von_mises(ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ);  asv = ASV(:);

U = sinterp3(x, y, z, u, AX, AY, AZ);
V = sinterp3(x, y, z, v, AX, AY, AZ);
W = sinterp3(x, y, z, w, AX, AY, AZ);

eu = abs(U-AU); eu = eu(:); au = AU(:);
ev = abs(V-AV); ev = ev(:); av = AV(:);
ew = abs(W-AW); ew = ew(:); aw = AW(:);

SXX = sinterp3(x, y, z, sxx, AX, AY, AZ);
SYY = sinterp3(x, y, z, syy, AX, AY, AZ);
SZZ = sinterp3(x, y, z, szz, AX, AY, AZ);
SXY = sinterp3(x, y, z, sxy, AX, AY, AZ);
SXZ = sinterp3(x, y, z, sxz, AX, AY, AZ);
SYZ = sinterp3(x, y, z, syz, AX, AY, AZ);
SV = von_mises(SXX, SYY, SZZ, SXY, SXZ, SYZ);

ESXX = abs(SXX - ASXX); esxx = ESXX(:);
ESYY = abs(SYY - ASYY); esyy = ESYY(:);
ESZZ = abs(SZZ - ASZZ); eszz = ESZZ(:);
ESXY = abs(SXY - ASXY); esxy = ESXY(:);
ESXZ = abs(SXZ - ASXZ); esxz = ESXZ(:);
ESYZ = abs(SYZ - ASYZ); esyz = ESYZ(:);
estress = [esxx, esyy, eszz, esxy, esxz, esyz];
% esv = abs(ASV - SV); esv = esv(:);

e1 = l1_norm([eu, ev, ew]) / l1_norm([au, av, aw]);
einf = linf_norm([eu, ev, ew]) / linf_norm([au, av, aw]);

e1s = l1_norm(estress) / l1_norm(astress);
einfs = linf_norm(estress) / linf_norm(astress);
eE = energy_norm3(esxx, esyy, eszz, esxy, esxz, esyz, lam, mu) / ...
     energy_norm3(asxx, asyy, aszz, asxy, asxz, asyz, lam, mu);
 
errors(i, ul1) = e1;
errors(i, ulinf) = einf;

errors(i, sl1) = e1s;
errors(i, slinf) = einfs;
errors(i, senergy) = eE;


   [~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);
    rho = -log10(d/max(d));
    rhomax = max(rho);
    
    ref = h5readatt(file, name, 'ref');
    deref = h5readatt(file, name, 'deref');
    same = h5readatt(file, name, 'same');
    ref_limit = h5readatt(file, name, 'ref_limit');
    deref_limit = h5readatt(file, name, 'deref_limit');
    fprintf('%d & %.4f & %d & %d & %d & %d & %d & %d & %.2f \\\\ \n',...
        i-1, errors(i, senergy), N, ref, ref_limit, same, deref, deref_limit, rhomax);

end

%%

save([plotdatapath casename '_error.mat'], 'errors', 'Ns',...
     'nerr', 'errleg', 'P', 'a', 'e', 'ul1', 'ulinf', 'sl1', 'slinf', 'senergy');

