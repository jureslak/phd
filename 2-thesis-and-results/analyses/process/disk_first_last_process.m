prepare

casename = 'disk_adaptive_01';

file = ['data/' casename '.h5'];
info = h5info(file);

P = h5readatt(file, '/conf', 'case.P');
R = h5readatt(file, '/conf', 'case.R');
eps = h5readatt(file, '/conf', 'case.eps');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');

ng = length(info.Groups) - 1;

% independent points
l = R-eps;
lim = l;
den = 100;
xx = linspace(0, lim, den);
yy = linspace(l-lim, l, den);
[X, Y] = ndgrid(xx, yy);
% X = X(:); Y = Y(:);
% valid = X.^2 + Y.^2 <= (R-eps).^2;
% X = X(valid); Y = Y(valid);

[ASXX, ASYY, ASXY] = disk_anal(X, Y, P, R);
AV = von_mises(ASXX, ASYY, ASXY);
l1a = l1_norm([ASXX ASYY ASXY]);
Ea = energy_norm(ASXX, ASYY, ASXY, lam, mu);
lia = linf_norm([ASXX ASYY ASXY]);

nerr = 3;
l1 = 1;
energy = 2;
linf = 3;
errleg = {'$e_1$', '$e_E$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

data.iter = cell(ng, 1);

for g = [1, ng]
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    SXX = sinterp(x, y, sxx, X, Y);
    SXY = sinterp(x, y, sxy, X, Y);
    SYY = sinterp(x, y, syy, X, Y);
   
    data.iter{g}.pos = pos;
    data.iter{g}.err = energy_kernel(SXX-ASXX, SYY-ASYY, SXY-ASXY, lam, mu) / Ea;

end
niter = ng;

a.first = data.iter{1};
a.last = data.iter{ng};
data = a;

save([plotdatapath casename '_first_last.mat'], 'P', 'R', 'eps', 'niter', 'data', 'xx', 'yy');
