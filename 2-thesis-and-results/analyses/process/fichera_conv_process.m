prepare

casename = 'fichera_uniform';
casename = 'fichera_adaptive';
datapath = 'data/';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;


% independent points

l = 0.05;
den = 100;
xx = linspace(-l, l, den);
yy = linspace(-l, l, den);
zz = linspace(-l, l, den);
[X, Y, Z] = meshgrid(xx, yy, zz);
X = X(:); Y = Y(:); Z = Z(:);
valid = ~(X > 0 & Y > 0 & Z > 0);
X = X(valid); Y = Y(valid); Z = Z(valid);
P = [X Y Z];

A = fichera_anal(X, Y, Z);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    N = length(pos);
    sol = h5read(file, [gname '/sol']);
    
%     S = rbffd_interp(pos', sol', P', 'RBFFunction', 'cubic', 'Neighbours', n)';
%     S = mls_interp(pos', sol', P', 'WeightFunction', 'gaussian',...
%                'WeightConstant', 100, 'Neighbours', 9)';
%     [shapes, I] = mls2d(pos, gau);
%     S = sheppard(pos, sol, P, n);

%     S = sinterp(pos(:, 1), pos(:, 2), sol, X, Y);
    
    S = griddata(pos(:, 1), pos(:, 2), pos(:, 3), sol, X, Y, Z);

%     S = sol;
%     A = fichera_anal(pos(:, 1), pos(:, 2), pos(:, 3));
%     size(S)
%     size(A)
    
    Ns(g) = N;
    
    errors(g, l1) = norm(S-A, 1) / norm(A, 1);
    errors(g, l2) = norm(S-A, 2) / norm(A, 2);
    errors(g, linf) = norm(S-A, 'inf') / norm(A, 'inf');
    
end

save([plotdatapath casename '_error.mat'], 'errors', 'l1', 'l2', 'linf', 'Ns', 'nerr', 'errleg');