prepare

casename = 'hertzian_adaptive';
% casename = 'disk_uniform_001';

file = ['data/' casename '.h5'];
info = h5info(file);

lam = h5readatt(file, '/conf', 'phy.lam');
mu = h5readatt(file, '/conf', 'phy.mu');
p0 = h5readatt(file, '/conf', 'phy.p0');
a = h5readatt(file, '/conf', 'phy.a');
H = h5readatt(file, '/conf', 'case.H');

ng = length(info.Groups) - 1;

% independent points
den = 250;
f = 10;
xx = linspace(-f*a, f*a, 2*den);
yy = linspace(-f*a, 0, den);
[X, Y] = meshgrid(xx, yy);

[ASXX, ASYY, ASXY] = hertzian_analytical(X, Y, a, p0);
AV = von_mises(ASXX, ASYY, ASXY);

l1a = l1_norm([ASXX ASYY ASXY]);
Ea = energy_norm(ASXX, ASYY, ASXY, lam, mu);
lia = linf_norm([ASXX ASYY ASXY]);

nerr = 3;
l1 = 1;
energy = 2;
linf = 3;
errleg = {'$e_1$', '$e_E$', '$e_\infty$'};

n = h5readatt(file, '/conf', 'approx.n');

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
%     fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    SXX = sinterp(x, y, sxx, X, Y);
    SXY = sinterp(x, y, sxy, X, Y);
    SYY = sinterp(x, y, syy, X, Y);

    Ns(g) = N;

    errors(g, l1) = l1_norm([SXX SYY SXY] - [ASXX ASYY ASXY]) / l1a;
    errors(g, energy) = energy_norm(SXX-ASXX, SYY-ASYY, SXY-ASXY, lam, mu) / Ea;
    errors(g, linf) = linf_norm([SXX SYY SXY] - [ASXX ASYY ASXY]) / lia;
    
    [~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);
    rho = -log10(d/max(d));
    rhomax = max(rho);
    
    ref = h5readatt(file, gname, 'ref');
    deref = h5readatt(file, gname, 'deref');
    same = h5readatt(file, gname, 'same');
    ref_limit = h5readatt(file, gname, 'ref_limit');
    deref_limit = h5readatt(file, gname, 'deref_limit');
    fprintf('%d & %.4f & %d & %d & %d & %d & %d & %d & %.2f \\\\ \n',...
        g-1, errors(g, linf), N, ref, ref_limit, same, deref, deref_limit, rhomax);
end

save([plotdatapath casename '_error.mat'], 'errors', 'l1', 'energy', 'linf', 'Ns', 'nerr', 'errleg', 'a', 'p0');
