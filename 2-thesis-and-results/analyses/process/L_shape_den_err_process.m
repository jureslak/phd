prepare

casename = 'L_shape_adaptive';
datapath = 'data/';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

omega = 3*pi/2;

% independent points

xx = linspace(-1, 1, 75);
yy = linspace(-1, 1, 75);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
valid = ~(X > 0 & Y < 0);
X = X(valid); Y = Y(valid);
P = [X Y];

A = L_anal(X, Y, omega);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

adapt = {};

for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    N = length(pos);
    sol = h5read(file, [gname '/sol']);
    
%     S = sheppard(pos, sol, P, n);
    S = rbffd_interp(pos', sol', P', 'RBFFunction', 'cubic', 'Neighbours', n)';
    
    E = abs(S-A);
    
    errind = h5read(file, [gname '/error_indicator']);
    
    Eh = rbffd_interp(pos', errind', P', 'RBFFunction', 'cubic', 'Neighbours', n)';
    
    [~, D] = knnsearch(pos, pos, 'K', 2); d = D(:, 2);
    D = rbffd_interp(pos', -log10(d/max(d))', P', 'RBFFunction', 'cubic', 'Neighbours', n)';
    
    adapt{g}.E = E;
    adapt{g}.Eh = Eh;
    adapt{g}.D = D;
    adapt{g}.N = N;
    adapt{g}.iter = g-1;
end

save([plotdatapath casename '_err_den.mat'], 'adapt', 'xx', 'yy');