prepare

casename = 'disk_adaptive_001';
casename = 'disk_uniform_001';

file = ['data/' casename '.h5'];
info = h5info(file);

P = h5readatt(file, '/conf', 'case.P');
R = h5readatt(file, '/conf', 'case.R');
eps = h5readatt(file, '/conf', 'case.eps');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');

ng = length(info.Groups) - 1;

% independent points
l = R-eps;
lim = 0.1;
den = 500;
xx = linspace(0, lim, den);
yy = linspace(l-lim, l, den);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
valid = X.^2 + Y.^2 <= (R-eps).^2;
X = X(valid); Y = Y(valid);

[ASXX, ASYY, ASXY] = disk_anal(X, Y, P, R);
AV = von_mises(ASXX, ASYY, ASXY);
l1a = l1_norm([ASXX ASYY ASXY]);
Ea = energy_norm(ASXX, ASYY, ASXY, lam, mu);
lia = linf_norm([ASXX ASYY ASXY]);

nerr = 3;
l1 = 1;
energy = 2;
linf = 3;
errleg = {'$e_1$', '$e_E$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

if 0
f1 = setfig('b2', [900 1200]);
end

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    SXX = sinterp(x, y, sxx, X, Y);
    SXY = sinterp(x, y, sxy, X, Y);
    SYY = sinterp(x, y, syy, X, Y);

    Ns(g) = N;

    errors(g, l1) = l1_norm([SXX SYY SXY] - [ASXX ASYY ASXY]) / l1a;
    errors(g, energy) = energy_norm(SXX-ASXX, SYY-ASYY, SXY-ASXY, lam, mu) / Ea;
    errors(g, linf) = linf_norm([SXX SYY SXY] - [ASXX ASYY ASXY]) / lia;

end

save([plotdatapath casename '_error.mat'], 'errors', 'l1', 'energy', 'linf', 'Ns', 'nerr', 'errleg', 'P', 'R', 'eps');
