prepare

d = 2;
cs = 'var';
cs = 'bnd_and_int';
casename = sprintf('quasi_uniform_%s_sample_%dd', cs, d);
data = [datapath casename '.h5'];
h5info(data);

pos = h5read(data, '/PDS-RF/domain/pos');
N = length(pos)
types = h5read(data, '/PDS-RF/domain/pos');

try
    h0 = 1./h5readatt(data, '/conf', 'num.n0');
    h1 = 1./h5readatt(data, '/conf', 'num.n1');
catch
    h0 = 1./h5readatt(data, '/conf', 'num.n');
    h1 = h0;
end
p1 = zeros(1, d);
p2 = zeros(1, d);
p3 = zeros(1, d);
p1(1) = 0.25; p1(2) = sqrt(3)/4;
p2(1) = 0.25; p2(2) = -sqrt(3)/4;
p3(1) = -0.5; p3(2) = 0;

ds = @(pos, p) sqrt(sum((pos - p).^2, 2));
h = @(pos) h0 + (h1-h0)*min(ds(pos, p1), min(ds(pos, p2), ds(pos, p3)));

hp = h(pos);
if d == 2, k = 6; else k = 12; end
[~, D] = knnsearch(pos, pos, 'K', k+1);
D = D(:, 2:end);
mu = mean(D, 2);
m = min(D, [], 2);
M = max(D, [], 2);
ds = D ./ hp;

edges = 0.8:0.02:2;
counts = histcounts(ds, edges);

bar = mean(ds, 2);
m = min(ds, [], 2);
M = max(ds, [], 2);
md = mean(bar);
sd = std(bar);
mm = mean(M-m);
sm = std(M-m);

if strcmp(cs, 'var'), name = 'variable'; else name = 'constant'; end

fprintf(['%s & %s & %s & %s   & %s   & %s   & %s \\\\\n'...
         '%s & %d & %d & %.4f & %.4f & %.4f & %.4f \\\\\n'],...
    '$h$', '$d$', '$N$', '$\text{mean}(\bar{d}_i)$', '$\text{std}(\bar{d}_i)$',...
    '$\text{mean}((d^\Delta_i)'')$', '$\text{std}((d^\Delta_i)'')$',...
    name, d, N, md, sd, mm, sm);

save([plotdatapath 'node_hist_' cs '_' num2str(d) 'd.mat'], 'edges', 'counts', 'k', 'd', 'N', 'h0', 'h1');
