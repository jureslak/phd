prepare

casename = 'disk_adaptive_01';

file = ['data/' casename '.h5'];
info = h5info(file);

P = h5readatt(file, '/conf', 'case.P');
R = h5readatt(file, '/conf', 'case.R');
eps = h5readatt(file, '/conf', 'case.eps');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');

ng = length(info.Groups) - 1;

n = h5readatt(file, '/conf', 'approx.n');

yd_all = linspace(0, R-eps, 1000);
xd_all = R-eps-yd_all;

[~, ~, asxy] = disk_anal(xd_all, yd_all, P, R);

[~, I] = max(asxy);
yM = yd_all(I);

if strcmp(casename, 'disk_adaptive_001')
xl = [0.4982 0.4988];
else
xl = [0.482 0.487];
end

yd_zoom = linspace(xl(1), xl(2), 1000);
xd_zoom = R-eps-yd_zoom;

profiles = cell(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    warning off
    sxy_all = rbffd_interp(pos', sxy', [xd_all; yd_all], 'RBFFunction', 'cubic', 'Neighbours', 13)';
    sxy_zoom = rbffd_interp(pos', sxy', [xd_zoom; yd_zoom], 'RBFFunction', 'cubic', 'Neighbours', 13)';
    warning on
    
    profiles{g}.sxy_all = sxy_all;
    profiles{g}.sxy_zoom = sxy_zoom;
end

save([plotdatapath casename '_profiles.mat'], 'R', 'P', 'eps', 'yd_all', 'yd_zoom', 'profiles');
