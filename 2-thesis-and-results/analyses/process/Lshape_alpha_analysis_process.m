prepare


casename = 'Lshape_adaptive_alpha_analysis';


datapath = 'data/';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

n = h5readatt(file, '/conf', 'approx.n');
ars = h5readatt(file, '/conf', 'num.alphas_r');
ars = str2num(ars);
na = length(ars);


l = 1;
den = 1000;
xx = linspace(-l, l, den);
yy = linspace(-l, l, den);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
valid = ~(X > 0 & Y < 0);
X = X(valid); Y = Y(valid);
P = [X Y];

A = L_anal(X, Y, 3*pi/2);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};

data = cell(na, 1);

for g = 1:ng
    niter = length(info.Groups(g).Groups);
    data{g}.errs = nan(niter, nerr);
    data{g}.Ns = nan(niter, 1);
    data{g}.niter = niter;
    
    for i = 1:niter
        gname = info.Groups(g).Groups(i).Name;
        fprintf('%s\n', gname);

        try       
            pos = h5read(file, [gname '/domain/pos']);
        catch, break, end
        N = length(pos);
        sol = h5read(file, [gname '/sol']);
       
%         S = rbffd_interp(pos', sol', P', 'RBFFunction', 'cubic', 'Neighbours', n)';
        S = sinterp(pos(:, 1), pos(:, 2), sol, X, Y);
        
        [~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);

        data{g}.Ns(i) = N;
        md = max(-log10(d/max(d)));
        data{g}.den = md;
    
        data{g}.errors(i, l1) = norm(S-A, 1) / norm(A, 1);
        data{g}.errors(i, l2) = norm(S-A, 2) / norm(A, 2);
        data{g}.errors(i, linf) = norm(S-A, 'inf') / norm(A, 'inf');
    end
    
    time = h5readatt(file, gname, 'time');
    data{g}.time = time;
end


save([plotdatapath casename '.mat'], 'data', 'ars', 'nerr', 'l1', 'l2', 'linf', 'errleg');
