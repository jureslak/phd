prepare

casename = 'point_contact_3d';
file = ['data/' casename '.h5'];

info = h5info(file);

a = h5readatt(file, '/conf', 'num.a');
P = h5readatt(file, '/conf', 'case.P');
E = h5readatt(file, '/conf', 'case.E');
nu = h5readatt(file, '/conf', 'case.nu');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');
e = h5readatt(file, '/conf', 'num.eps');

f = 3;
AX = linspace(-a, -e, 500);
AY = AX; AZ = AX;
[AU, AV, AW, ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ] = point_contact_3d_analytical(AX, AY, AZ, P, E, nu);
ASV = von_mises(ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ);

AXZ = linspace(-2.5*e, -e, 500);
AYZ = AXZ; AZZ = AXZ;
[AUZ, AVZ, AWZ, ASXXZ, ASYYZ, ASZZZ, ASXYZ, ASXZZ, ASYZZ] = point_contact_3d_analytical(AXZ, AYZ, AZZ, P, E, nu);
ASVZ = von_mises(ASXXZ, ASYYZ, ASZZZ, ASXYZ, ASXZZ, ASYZZ);

ncases = length(info.Groups) - 1;

data.niter = ncases;
data.x = AX;
data.xz = AXZ;
data.asv = ASV;
data.asvz = ASVZ;
data.profiles = cell(ncases, 1);

for i = 1:ncases

name = sprintf('/%03d', i-1)

pos = h5read(file, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);

types = h5read(file, [name '/domain/types']);

sol = h5read(file, [name '/displ']);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);

stress = h5read(file, [name, '/stress']);
sxx = stress(:, 1);
syy = stress(:, 2);
szz = stress(:, 3);
sxy = stress(:, 4);
sxz = stress(:, 5);
syz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

SV = sinterp3(x, y, z, sv, AX, AY, AZ);
SVZ = sinterp3(x, y, z, sv, AXZ, AYZ, AZZ);
data.profiles{i}.sv = SV;
data.profiles{i}.svz = SVZ;

end

save([plotdatapath casename '_profiles.mat'], 'data', 'e', 'a');

