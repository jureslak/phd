prepare

casename = 'L_shape_uniform';
% casename = 'L_shape_adaptive';
datapath = 'data/';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

omega = 3*pi/2;

% independent points

xx = linspace(-1, 1, 200);
yy = linspace(-1, 1, 200);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
valid = ~(X > 0 & Y < 0);
X = X(valid); Y = Y(valid);
P = [X Y];

A = L_anal(X, Y, omega);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    N = length(pos);
    sol = h5read(file, [gname '/sol']);

    S = rbffd_interp(pos', sol', P', 'RBFFunction', 'cubic', 'Neighbours', n)';
    
    Ns(g) = N;
    
    errors(g, l1) = norm(S-A, 1) / norm(A, 1);
    errors(g, l2) = norm(S-A, 2) / norm(A, 2);
    errors(g, linf) = norm(S-A, 'inf') / norm(A, 'inf');
    
    x = pos(:, 1); y = pos(:, 2);
    
end

save([plotdatapath casename '_error.mat'], 'errors', 'l1', 'l2', 'linf', 'Ns', 'nerr', 'errleg');