prepare

casename = 'disk_adaptive_wip';
casename = 'disk_uniform_001';

file = ['data/' casename '.h5'];
info = h5info(file);

P = h5readatt(file, '/conf', 'case.P');
R = h5readatt(file, '/conf', 'case.R');
eps = h5readatt(file, '/conf', 'case.eps');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');

ng = length(info.Groups) - 1;

% independent points
l = R-eps;
lim = 0.1;
den = 50;
xx = linspace(0, lim, den);
yy = linspace(l-lim, l, den);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
valid = X.^2 + Y.^2 <= (R-eps).^2;
X = X(valid); Y = Y(valid);

[ASXX, ASYY, ASXY] = disk_anal(X, Y, P, R);
AV = von_mises(ASXX, ASYY, ASXY);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

if 0
f1 = setfig('b2', [900 1200]);
end

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    SXX = sinterp(x, y, sxx, X, Y);
    SXY = sinterp(x, y, sxy, X, Y);
    SYY = sinterp(x, y, syy, X, Y);

    Ns(g) = N;

    errors(g, l1) = l1_norm([SXX SYY SXY] - [ASXX ASYY ASXY]) / l1_norm([ASXX ASYY ASXY]);
%     errors(g, l2) = norm(S-A, 2) / norm(A, 2);
%     errors(g, linf) = norm(S-A, 'inf') / norm(A, 'inf');

    if g == ng+1
        sv = von_mises(sxx, syy, sxy);

        setfig('a2');
        scatter(x, y, 10, sv, 'filled');
        scatter(X, Y, 3, AV, 'filled');
        colormap viridis
        axis equal

        setfig('a4');
        [~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);
        rho = -log10(d/max(d));
        scatter(x, y, 10, rho, 'filled');
        colorbar
        colormap viridis
        axis equal

        yd = linspace(R-eps-    0.01, R-eps, 10000);
        xd = R-eps-yd;

        [~, ~, asxy] = disk_anal(xd, yd, P, R);
%         sxy = sinterp(x, y, sxy, xd, yd);
        warning off
        sxy = rbffd_interp(pos', sxy', [xd; yd], 'RBFFunction', 'cubic', 'Neighbours', 13)';
        warning on
        setfig('a1');
        plot(yd, asxy, '-r');
        plot(yd, sxy, '-k');
    end

end

setfig('a4', [400 350]);
ll
plot(Ns, errors(:, l1), 'o-', 'DisplayName', 'L1'); leg = {'L1'};
% k = polyfit(log(Ns), log(errors(:, l1)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
% plot(Ns, errors(:, l2), 'o-', 'DisplayName', 'L2'); leg{end+1} = 'L2';
% k = polyfit(log(Ns), log(errors(:, l2)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
% plot(Ns, errors(:, linf), 'o-', 'DisplayName', 'Linf'); leg{end+1} = 'Linf';
% k = polyfit(log(Ns), log(errors(:, linf)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));

