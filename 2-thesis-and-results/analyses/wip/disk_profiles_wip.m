prepare

casename = 'disk_adaptive_wip';

file = ['data/' casename '.h5'];
info = h5info(file);

P = h5readatt(file, '/conf', 'case.P');
R = h5readatt(file, '/conf', 'case.R');
eps = h5readatt(file, '/conf', 'case.eps');
lam = h5readatt(file, '/conf', 'case.lam');
mu = h5readatt(file, '/conf', 'case.mu');

ng = length(info.Groups) - 1;

% independent points
l = R-eps;
lim = 0.1;
den = 50;
xx = linspace(0, lim, den);
yy = linspace(l-lim, l, den);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
valid = X.^2 + Y.^2 <= (R-eps).^2;
X = X(valid); Y = Y(valid);

[ASXX, ASYY, ASXY] = disk_anal(X, Y, P, R);
AV = von_mises(ASXX, ASYY, ASXY);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

f1 = setfig('a1', [900 1200]);
yd = linspace(R-eps-0.01, R-eps, 10000);
xd = R-eps-yd;
[~, ~, asxy] = disk_anal(xd, yd, P, R);

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    SXX = sinterp(x, y, sxx, X, Y);
    SXY = sinterp(x, y, sxy, X, Y);
    SYY = sinterp(x, y, syy, X, Y);

    Ns(g) = N;

    errors(g, l1) = l1_norm([SXX SYY SXY] - [ASXX ASYY ASXY]) / l1_norm([ASXX ASYY ASXY]);

    warning off
    sxy = rbffd_interp(pos', sxy', [xd; yd], 'RBFFunction', 'cubic', 'Neighbours', 13)';
    warning on
    plot(yd, sxy, '-', 'Color', (ng-g)/ng/1.5*[1 1 1], 'DisplayName', sprintf('iteration %d', g-1));
end

plot(yd, asxy, '-r', 'DisplayName', 'analytical');
legend({}, 'Location', 'NW');

