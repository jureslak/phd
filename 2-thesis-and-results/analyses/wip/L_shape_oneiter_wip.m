prepare

casename = 'L_shape_uniform_wip';
casename = 'L_shape_adaptive_wip';
datapath = 'data/';
file = [datapath casename '.h5'];
info = h5info(file);

iter = '/000';

pos = h5read(file, [iter '/domain/pos']);
N = length(pos)
x = pos(:, 1);
y = pos(:, 2);
sol = h5read(file, [iter '/sol']);
types = h5read(file, [iter '/domain/types']);

setfig('b1');
scatter(x, y, 15, sol, 'filled');
colorbar
axis equal
title('Solution');

setfig('b3');
[~, d] = knnsearch(pos, pos, 'K', 2);
d = d(:, 2);

% scatter(x, y, 15, log(d)/log(max(d)), 'filled');
% scatter(x, y, 15, types, 'filled');
plot_domain(file, [iter '/domain']);

colorbar
axis equal
title('Node density');


omega = 3*pi/2;

% independent points

xx = linspace(-1, 1, 100);
yy = linspace(-1, 1, 100);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
invalid = X > 0 & Y < 0;
X = X(~invalid); Y = Y(~invalid);
P = [X Y];

A = L_anal(X, Y, omega);
n = h5readatt(file, '/conf', 'approx.n');
% S = sheppard(pos, sol, P, n);
S = rbffd_interp(pos', sol', P', 'RBFFunction', 'cubic', 'Neighbours', n)';

setfig('b2');
anal = L_anal(x, y, omega);
err = h5read(file, [iter '/error_indicator']);
% scatter(x, y, 15, abs(anal-sol), 'filled');
scatter(x, y, 15, err, 'filled');
colorbar
axis equal
title('Error');


setfig('b4');
scatter(X, Y, 15, abs(A-S), 'filled');
% scatter(x, y, 15, 'ok', 'filled');
colorbar
axis equal
title('Error');