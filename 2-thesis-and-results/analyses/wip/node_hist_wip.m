prepare

d = 3;
casename = sprintf('quasi_uniform_var_sample_%dd', d);
data = [datapath casename '_wip.h5'];
h5info(data);

pos = h5read(data, '/PDS-RF/domain/pos');
N = length(pos)
types = h5read(data, '/PDS-RF/domain/pos');

h0 = 1./h5readatt(data, '/conf', 'num.n0');
h1 = 1./h5readatt(data, '/conf', 'num.n1');

p1 = zeros(1, d);
p2 = zeros(1, d);
p3 = zeros(1, d);
p1(1) = 0.25; p1(2) = sqrt(3)/4;
p2(1) = 0.25; p2(2) = -sqrt(3)/4;
p3(1) = -0.5; p3(2) = 0;

ds = @(pos, p) sqrt(sum((pos - p).^2, 2));
h = @(pos) h0 + (h1-h0)*min(ds(pos, p1), min(ds(pos, p2), ds(pos, p3)));

if 1
    hp = h(pos);
    k = 12;
    [~, D] = knnsearch(pos, pos, 'K', k+1);
    D = D(:, 2:end);
    mu = mean(D, 2);
    m = min(D, [], 2);
    M = max(D, [], 2);
    idxs = (1:length(mu))';
    save(['wip/node_hist_' num2str(d) 'd.mat'], 'D', 'hp', 'k');
else
    load(['wip/node_hist_' num2str(d) 'd.mat']);
end

dn = D ./ hp;
dd = D(:, 1);

x = pos(:, 1);
y = pos(:, 2);

setfig('b1', [450 300]);
histogram(dn(:), 50, 'normalization', 'count', 'FaceColor', 'w', 'FaceAlpha', 1)
% ylim([0 1.48e6])
xticks(0.8:0.2:1.8)
xlim([0.8 1.85])
ylabel('');
xlabel('$d_{i,j}$')
title(sprintf('$N = %d$, $d = %d$, $n = %d$', N, d, k));
% yticks((0:10).*10^4)


