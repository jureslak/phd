close all
filename = 'data/node_demo_3d_roman_coarse.h5';

pos = h5read(filename, '/domain/pos');
[N, dim] = size(pos);
fprintf('%d\n', N);

types = h5read(filename, '/domain/types');

bnd = find(types < 0);
int = find(types > 0);
x = pos(:, 1);
y = pos(:, 2);
if dim == 3, z = pos(:, 3); end

figure
hold on; box on; grid on;
daspect([1 1 1])
if dim == 2
    scatter(x(bnd), y(bnd), 5, 'or', 'filled');
    scatter(x(int), y(int), 5, 'ob', 'filled');
elseif dim == 3
    view(3)
    scatter3(x(bnd), y(bnd), z(bnd), 5, 'or', 'filled');
    scatter3(x(int), y(int), z(int), 5, 'ob', 'filled');
end

