prepare

casename = 'fwo_adaptive_wip';

file = ['data/' casename '.h5'];
info = h5info(file);

lam = h5readatt(file, '/conf', 'phy.lam');
mu = h5readatt(file, '/conf', 'phy.mu');
p0 = h5readatt(file, '/conf', 'phy.p0');
a = h5readatt(file, '/conf', 'phy.a');

name = '/003';

setfig('a1');
plot_domain(file, [name '/domain']);
xlim([-5*a, 5*a])
ylim([-5*a, a])

pos = h5read(file, [name '/domain/pos']);
types = h5read(file, [name '/domain/types']);
% sup = h5read(datafile, [name '/domain/supp'])+1;

x = pos(:, 1);
y = pos(:, 2);
N = length(x)


f1 = setfig('a4');
try
displ = h5read(file, [name '/displ']);

% u = displ(1, :);
% v = displ(2, :);
stress = h5read(file, [name '/stress']);
sxx = stress(:, 1);
syy = stress(:, 2);
sxy = stress(:, 3);

h = scatter(x*1000, y*1000, 5, types, 'filled');
daspect([1 1 1])
title(sprintf('$N = %d$', N))
colorbar
colormap viridis
title('$\rho$')

v = von_mises(sxx, syy, sxy);

h.CData = v/1e6;
caxis([0, 500])
% caxis([-1, 2])
% h.CData = sqrt(u.^2+v.^2);
catch, end

[~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);
rho = -log10(d./max(d));
% h.CData = rho;


f = 4;
xx = linspace(-f*a, f*a, 100);
yy = linspace(-f*a, 0, 100);
[X, Y] = meshgrid(xx, yy);
V = sinterp(x, y, rho, X, Y);

setfig('a2');
contourf(X/a, Y/a, V, 100, 'EdgeColor', 'none');
% scatter(x/a, y/a, 15, 'ok', 'filled');
daspect([1 1 1])
colormap viridis
xlim([-f, f])
ylim([-f, 0])
xlabel('$x/a$')
ylabel('$y/a$')
colorbar



xx = linspace(-f*a, f*a, 1000);
yy = zeros(size(xx));
setfig('a3');
ssxx = griddata(x, y, sxx, xx, yy);
plot(xx, ssxx, '-');


