prepare

casename = 'fichera_uniform_wip';
casename = 'fichera_adaptive_wip';
datapath = 'data/';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;


% independent points

l = 0.05;
den = 50;
xx = linspace(-l, l, den);
yy = linspace(-l, l, den);
zz = linspace(-l, l, den);
[X, Y, Z] = meshgrid(xx, yy, zz);
X = X(:); Y = Y(:); Z = Z(:);
valid = ~(X > 0 & Y > 0 & Z > 0);
X = X(valid); Y = Y(valid); Z = Z(valid);
P = [X Y Z];

A = fichera_anal(X, Y, Z);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

if 0
f1 = setfig('b2', [900 1200]);
end

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    N = length(pos);
    sol = h5read(file, [gname '/sol']);
    
%     S = rbffd_interp(pos', sol', P', 'RBFFunction', 'cubic', 'Neighbours', n)';
%     S = mls_interp(pos', sol', P', 'WeightFunction', 'gaussian',...
%                'WeightConstant', 100, 'Neighbours', 9)';
%     [shapes, I] = mls2d(pos, gau);
%     S = sheppard(pos, sol, P, n);

%     S = sinterp(pos(:, 1), pos(:, 2), sol, X, Y);
    
    S = griddata(pos(:, 1), pos(:, 2), pos(:, 3), sol, X, Y, Z);

%     S = sol;
%     A = fichera_anal(pos(:, 1), pos(:, 2), pos(:, 3));
%     size(S)
%     size(A)
    
    Ns(g) = N;
    
    errors(g, l1) = norm(S-A, 1) / norm(A, 1);
    errors(g, l2) = norm(S-A, 2) / norm(A, 2);
    errors(g, linf) = norm(S-A, 'inf') / norm(A, 'inf');
    
    x = pos(:, 1); y = pos(:, 2);
    
    if 0
    
    errind = h5read(file, [gname '/error_indicator']);
    
    subplot(ng, 3, 3*g-2);
    sizes = [5 5 5 3 3 2 2 2];
    scatter(X, Y, sizes(g), log10(abs(S-A)), 'filled');
    axis equal
    colormap jet
    colorbar
    caxis([-9 -1])
    xlim([-1 1])
    ylim([-1 1])
    
    subplot(ng, 3, 3*g-1);
    sizes = [5 5 5 3 3 2 2 2];
    scatter(x, y, sizes(g), errind, 'filled');
    axis equal
    colormap jet
    colorbar
    %caxis([-5 0])
    xlim([-1 1])
    ylim([-1 1])
%     text(140, 70, sprintf('iter %d', g), 'Units', 'points')
%     text(125, 55, sprintf('$N = %d$', Ns(g)), 'Units', 'points')
    
    subplot(ng, 3, 3*g);
    [~, D] = knnsearch(pos, pos, 'K', 2); d = D(:, 2);
    scatter(x, y, sizes(g), -log10(d/max(d)), 'filled');
    axis equal
    colormap jet
    colorbar
    xlim([-1 1])
    ylim([-1 1])
    
    end
end

casenameU = 'fichera_uniform_error';
U = load([plotdatapath casenameU '.mat']);

setfig('b1', [400 350]);
plot(Ns, errors(:, l1), 'o-', 'DisplayName', 'L1'); leg = {'L1'};
% k = polyfit(log(Ns), log(errors(:, l1)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
plot(Ns, errors(:, l2), 'o-', 'DisplayName', 'L2'); leg{end+1} = 'L2';
% k = polyfit(log(Ns), log(errors(:, l2)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
plot(Ns, errors(:, linf), 'o-', 'DisplayName', 'Linf'); leg{end+1} = 'Linf';
% k = polyfit(log(Ns), log(errors(:, linf)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));

ll;
xlim([1e3 1e6]);
ylim([1e-3 1e1]);
leg = {};
for i = 1:U.nerr
    plot(U.Ns, U.errors(:, i), [MARKS{i} '-'], 'Color', 0.5*COLORS(i, :)); leg{end+1} = U.errleg{i};
    
%     k = polyfit(log(U.Ns), log(U.errors(:, i)), 1);
%     plot(U.Ns, exp(k(2)).*U.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end
U.Ns(end) = U.Ns(end)*1.1;
plot(U.Ns, 2*U.Ns.^-0.333, 'k--'); leg{end+1} = '$O(h)$';
% plot(U.Ns, 20*U.Ns.^-1, 'k--'); leg{end+1} = '$O(h^2)$';
legend(leg, 'Location', 'SW');
title('Uniform');
xticks(10.^(3:6))
yticks(10.^(-6:1));

