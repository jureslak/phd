prepare

casename = 'point_contact_3d_wip';
datafile = ['data/' casename '.h5'];

info = h5info(datafile);

a = h5readatt(datafile, '/conf', 'num.a');
P = h5readatt(datafile, '/conf', 'case.P');
E = h5readatt(datafile, '/conf', 'case.E');
nu = h5readatt(datafile, '/conf', 'case.nu');
lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');
e = h5readatt(datafile, '/conf', 'num.eps');

f = 3;
[AX, AY, AZ] = meshgrid(deal(linspace(max(-f*e, -1), -e, 10)));

ncases = length(info.Groups) - 1;

Ns = zeros(ncases, 1);
errors = zeros(ncases, 4);

for i = 1:ncases

name = sprintf('/%03d', i-1);

pos = h5read(datafile, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);
Ns(i) = length(pos);
[~, d] = knnsearch(pos, pos, 'K', 2); d = d(:, 2);
rho = -log10(d/max(d));
maxrho = max(rho)

types = h5read(datafile, [name '/domain/types']);

sol = h5read(datafile, [name '/displ']);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);
dnorm = sum(sol.^2, 2);

stress = h5read(datafile, [name, '/stress']);
sxx = stress(:, 1);
syy = stress(:, 2);
szz = stress(:, 3);
sxy = stress(:, 4);
sxz = stress(:, 5);
syz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

[AU, AV, AW, ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ] = point_contact_3d_analytical(AX, AY, AZ, P, E, nu);
asxx = ASXX(:); asyy = ASYY(:); aszz = ASZZ(:); asxy = ASXY(:); asxz = ASXZ(:); asyz = ASYZ(:);
astress = [asxx asyy aszz asxy asxz asyz];
ASV = von_mises(ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ);  asv = ASV(:);

U = sinterp3(x, y, z, u, AX, AY, AZ);
V = sinterp3(x, y, z, v, AX, AY, AZ);
W = sinterp3(x, y, z, w, AX, AY, AZ);

eu = abs(U-AU); eu = eu(:); au = AU(:);
ev = abs(V-AV); ev = ev(:); av = AV(:);
ew = abs(W-AW); ew = ew(:); aw = AW(:);

SXX = sinterp3(x, y, z, sxx, AX, AY, AZ);
SYY = sinterp3(x, y, z, syy, AX, AY, AZ);
SZZ = sinterp3(x, y, z, szz, AX, AY, AZ);
SXY = sinterp3(x, y, z, sxy, AX, AY, AZ);
SXZ = sinterp3(x, y, z, sxz, AX, AY, AZ);
SYZ = sinterp3(x, y, z, syz, AX, AY, AZ);
SV = von_mises(SXX, SYY, SZZ, SXY, SXZ, SYZ);

ESXX = abs(SXX - ASXX); esxx = ESXX(:);
ESYY = abs(SYY - ASYY); esyy = ESYY(:);
ESZZ = abs(SZZ - ASZZ); eszz = ESZZ(:);
ESXY = abs(SXY - ASXY); esxy = ESXY(:);
ESXZ = abs(SXZ - ASXZ); esxz = ESXZ(:);
ESYZ = abs(SYZ - ASYZ); esyz = ESYZ(:);
estress = [esxx, esyy, eszz, esxy, esxz, esyz];
% esv = abs(ASV - SV); esv = esv(:);

e1 = l1_norm([eu, ev, ew]) / l1_norm([au, av, aw]);
einf = linf_norm([eu, ev, ew]) / linf_norm([au, av, aw]);

e1s = l1_norm(estress) / l1_norm(astress)
einfs = linf_norm(estress) / linf_norm(astress);
eE = energy_norm3(esxx, esyy, eszz, esxy, esxz, esyz, lam, mu) / ...
     energy_norm3(asxx, asyy, aszz, asxy, asxz, asyz, lam, mu);
 
errors(i, 1) = e1;
errors(i, 2) = einf;

errors(i, 3) = e1s;
errors(i, 4) = einfs;
errors(i, 5) = eE;


end

%%

close all
setfig('a2');
plot(Ns, errors(:, 1), 'o-');
plot(Ns, errors(:, 2), 'x-');
plot(Ns, errors(:, 3), '^-');
plot(Ns, errors(:, 4), '*-');
plot(Ns, errors(:, 5), 'h-');
plot(Ns, Ns.^(-1/3), 'k--');
plot(Ns, Ns.^(-2/3), 'k--');
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xlabel('$N$');
title('Point contact 3D');
ylabel('error');
legend('$e_1$ displ', '$e_\infty$ displ', '$e_1$ stress', '$e_\infty$ stress',...
       '$e_E$', '$O(h)$', '$O(h^2)$');

setfig('a4'); view([126.9000   25.2000]);
scatter3(x, y, z, 5, sv, 'filled');
colorbar
daspect([1 1 1])
colormap jet

setfig('a3'); view([126.9000   25.2000]);
I = -f*e < x & x < -e & -f*e < y & y < -e & -f*e < z & z < -e;
scatter3(x(I), y(I), z(I), 5, sv(I), 'filled');
colorbar
daspect([1 1 1])
f = 5;
xlim([-f*e, -e])
ylim([-f*e, -e])
zlim([-f*e, -e])
colormap viridis
caxis([0, max(asv)])


