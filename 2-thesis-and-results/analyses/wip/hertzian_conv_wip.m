prepare

casename = 'hertzian_adaptive_wip';
% casename = 'disk_uniform_001';

file = ['data/' casename '.h5'];
info = h5info(file);

lam = h5readatt(file, '/conf', 'phy.lam');
mu = h5readatt(file, '/conf', 'phy.mu');
p0 = h5readatt(file, '/conf', 'phy.p0');
a = h5readatt(file, '/conf', 'phy.a');

ng = length(info.Groups) - 1;

% independent points
f = 15;
xx = linspace(-f*a, f*a, 200);
yy = linspace(-f*a, 0, 100);
[X, Y] = meshgrid(xx, yy);

[ASXX, ASYY, ASXY] = hertzian_analytical(X, Y, a, p0);
AV = von_mises(ASXX, ASYY, ASXY);

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};


n = h5readatt(file, '/conf', 'approx.n');

if 0
f1 = setfig('b2', [900 1200]);
end

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;
    fprintf('%s\n', gname);

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(pos);
    stress = h5read(file, [gname '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    sxy = stress(:, 3);

    SXX = sinterp(x, y, sxx, X, Y);
    SXY = sinterp(x, y, sxy, X, Y);
    SYY = sinterp(x, y, syy, X, Y);

    Ns(g) = N;

    errors(g, l1) = l1_norm([SXX SYY SXY] - [ASXX ASYY ASXY]) / l1_norm([ASXX ASYY ASXY]);
%     errors(g, l2) = norm(S-A, 2) / norm(A, 2);
%     errors(g, linf) = norm(S-A, 'inf') / norm(A, 'inf');

end

setfig('a4', [400 350]);
ll
plot(Ns, errors(:, l1), 'o-', 'DisplayName', 'L1'); leg = {'L1'};
setfig('a2', [400 350]);
set(gca, 'yscale', 'log')
plot(1:ng, errors(:, l1), 'o-', 'DisplayName', 'L1'); leg = {'L1'};
setfig('ao8')
plot(1:ng, Ns, 'o-');
% k = polyfit(log(Ns), log(errors(:, l1)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
% plot(Ns, errors(:, l2), 'o-', 'DisplayName', 'L2'); leg{end+1} = 'L2';
% k = polyfit(log(Ns), log(errors(:, l2)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
% plot(Ns, errors(:, linf), 'o-', 'DisplayName', 'Linf'); leg{end+1} = 'Linf';
% k = polyfit(log(Ns), log(errors(:, linf)), 1);
% plot(Ns, exp(k(2)).*Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));

