prepare

casename = 'fichera_uniform_wip';
casename = 'fichera_adaptive_wip';
datapath = 'data/';
file = [datapath casename '.h5'];
info = h5info(file);

iter = '/003';

pos = h5read(file, [iter '/domain/pos']);
N = length(pos)
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);
sol = h5read(file, [iter '/sol']);
types = h5read(file, [iter '/domain/types']);

setfig('b1'); view([-228.3000   43.6000])
scatter3(x, y, z, 15, sol, 'filled');
colorbar
daspect([1 1 1])
title('Solution');

setfig('b3'); view([-228.3000   43.6000]);
[~, d] = knnsearch(pos, pos, 'K', 2);
d = d(:, 2);

scatter3(x, y, z, 15, -log10(d/max(d)), 'filled');
% scatter(x, y, 15, types, 'filled');
% plot_domain(file, [iter '/domain']);

colorbar
axis equal
title('Node density');

setfig('b2'); view([-228.3000   43.6000])
a = fichera_anal(x, y, z);
err = abs(sol-a);
ul = quantile(err, 0.95);
scatter3(x, y, z, 15, err, 'filled');
caxis([0, ul])
colorbar
daspect([1 1 1])
title('error');


[X, Y, Z] = meshgrid(deal(linspace(-1, 1, 50)));
V = griddata(x, y, z, err, X, Y, Z, 'nearest');
setfig('b4'); view([-228.3000   43.6000]);
h = slice(X, Y, Z, V, 0, 0, 0, 'nearest');
set(h,'edgecolor','none');
% caxis([0, ul])
colorbar
daspect([1 1 1])
title('error');


if 0

omega = 3*pi/2;

% independent points

xx = linspace(-1, 1, 100);
yy = linspace(-1, 1, 100);
[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
invalid = X > 0 & Y < 0;
X = X(~invalid); Y = Y(~invalid);
P = [X Y];

A = fichera_anal(X, Y, Z);
n = h5readatt(file, '/conf', 'approx.n');
% S = sheppard(pos, sol, P, n);
S = rbffd_interp(pos', sol', P', 'RBFFunction', 'cubic', 'Neighbours', n)';

setfig('b2');
anal = L_anal(x, y, omega);
err = h5read(file, [iter '/error_indicator']);
% scatter(x, y, 15, abs(anal-sol), 'filled');
scatter(x, y, 15, err, 'filled');
colorbar
axis equal
title('Error');

setfig('b4');
scatter(X, Y, 15, abs(A-S), 'filled');
% scatter(x, y, 15, 'ok', 'filled');
colorbar
axis equal
title('Error');

end