# Main results

To reproduce the main results, the [Medusa
library](https://gitlab.com/e62Lab/medusa) @ `3e7409f6` must be installed and
the paths in the `CMakeLists.txt` file must point to the installation directory.
The `CMakeLists.txt` file lists the main executables.

The folder `src/` contains the C++ source files and the `params/` folder the run
time parameters.  When compiled, the executables are located in the `bin/`
directory and can be run to produce the results in the folders as supplied by
the corresponding `params/` files.  The scripts in the `process/` directory
process the results, extract and save relevant data for plotting in the
`plotdata/` folder. The `plotscripts/` folder contains scripts for ploting these
results.

All Matlab scripts are intended to be run from the `analyses/` folder.  Before
running, the `common/` and `scripts/` directories must be added to PATH.
