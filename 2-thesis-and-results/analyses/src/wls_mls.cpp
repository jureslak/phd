#include <medusa/Medusa.hpp>
#include <iostream>

using namespace mm;
using namespace std;
using namespace Eigen;

template <typename vec_t>
class QuarticWeight {
    double R;
public:
    typedef vec_t vector_t;
    typedef typename vec_t::scalar_t scalar_t;
    QuarticWeight(double R) : R(R) {}
    double operator()(const vec_t& p) const {
        double u = p.norm()/R;
        return (u < 1) ? (1 - 3*u*u + 2*u*u*u) : 0;
    }
};


int main() {

    vector<vector<double>> pts_data = CSV::read2d("plotdata/sample_data_2d.txt", ' ');
    VectorXd val = CSV::readEigen("plotdata/sample_values_2d.txt");
    
    int n = pts_data.size();
    Range<Vec2d> pts(n);
    for (int i = 0; i < n; ++i) {
        pts[i] = {pts_data[i][0], pts_data[i][1]};
    }
        
    Vec2d c = {0.5, 0.5};
    double R = 0.25;
    double sigma = 0.25;
    WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, NoScale> wls(2, sigma);
    WLS<Monomials<Vec2d>, QuarticWeight<Vec2d>, NoScale> modwls(2, R);
    auto appr = wls.getApproximant(c, pts, val);
    
    KDTree<Vec2d> tree(pts);
    
    int nn = 50;
    MatrixXd V(nn, nn), V2(nn, nn), X(nn, nn), Y(nn, nn), V3(nn, nn), V4(nn, nn);
    Range<double> x = linspace(0., 1., nn);
    Range<double> y = linspace(0., 1., nn);
    for (int i = 0; i < nn; ++i) {
        for (int j = 0; j < nn; ++j) {
            Vec2d p = {x[i], y[j]};
            X(i, j) = x[i];
            Y(i, j) = y[j];
            V(i, j) = appr(p);
            
            auto mls_appr = wls.getApproximant(p, pts, val);
            V2(i, j) = mls_appr(p);

            
            Range<int> valid;
            for (int k = 0; k < n; ++k) {
                if ((p - pts[k]).norm() < R) {
                    valid.push_back(k);
                }
            }
//             prn(p);
            prn(valid.size());
//             auto mod_mls_appr = modwls.getApproximant(p, pts[valid].asRange(), val(valid));
            auto mod_mls_appr = modwls.getApproximant(p, pts, val);
            V4(i, j) = mod_mls_appr(p);
            
            auto id = tree.query(p, 9).first;
            auto kmls_appr = wls.getApproximant(p, pts[id].asRange(), val(id));
            V3(i, j) = kmls_appr(p);
        }
    }
    
    HDF hdf("plotdata/wls_mls.h5", HDF::DESTROY);
    hdf.writeEigen("wls_val", V);
    hdf.writeEigen("mls_val", V2);
    hdf.writeEigen("k-mls_val", V3);
    hdf.writeEigen("mod-mls_val", V4);
//     hdf.writeEigen("X", X);
//     hdf.writeEigen("Y", Y);
    hdf.writeDoubleArray("x", x);
    hdf.writeDoubleArray("y", y);


    return 0;
}
