#include "disk_case.hpp"
#include <medusa/bits/domains/GeneralFill.hpp>

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    DiskSolver<Vec2d> solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    double R = conf.get<double>("case.R");
    double eps = conf.get<double>("case.eps");
    BallShape<Vec2d> circ(0, R-eps);
    BoxShape<Vec2d> cut1({-R-1, -R-1}, {0, R+1});    
    BoxShape<Vec2d> cut2({-R-1, -R-1}, {R+1, 0});
    auto shape = circ - cut1 - cut2;


    vector<string> ns = split(conf.get<string>("num.nxs"), ",");
    int iter = 0;
    for (const string& s : ns) {
        int nx = stoi(s);
        cout << "-------- " << nx << " -----------\n";
        file.setGroupName(format("%02d", iter));

        Timer timer;
        timer.addCheckPoint("domain");

        double dx = 1.0/nx;
        GeneralFill<Vec2d> fill; fill.seed(conf.get<int>("num.seed"));
        auto h = [=](const Vec2d&) { return dx; };
//         DomainDiscretization<Vec2d> d = shape.discretizeWithDensity(, fill);

        auto d = shape.discretizeBoundaryWithDensity(h);
        
        // prefill with corase seed nodes
        fill(d, [&](const Vec2d& p) { return 10*h(p); });
        fill(d, h);
        
        solver.solve(d, conf, file, timer, h);

        file.atomic().writeDomain("domain", d);


        ++iter;
    }

    return 0;
}
