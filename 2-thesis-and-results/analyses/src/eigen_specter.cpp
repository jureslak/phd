#include <vector>
#include <queue>
#include <list>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <cmath>
#include <chrono>
#include <medusa/Medusa.hpp>
#include <medusa/bits/domains/GrainDropFill.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <medusa/bits/domains/GeneralSurfaceFill.hpp>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <medusa/bits/spatial_search/KDGrid.hpp>
#include "fill/demo_shapes.hpp"
#include "Poisson.hpp"

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif


using namespace std;
using namespace Eigen;
using namespace mm;


template <typename vec_t, typename func_t, typename sfill_t, typename fill_t>
void test_run(const XML& conf, HDF& file, const DemoShape<vec_t>& shape, sfill_t& sfill, fill_t& fill,  const func_t& delta_r) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;

    sfill.seed(get_seed());
    fill.seed(get_seed());
    DomainDiscretization<vec_t> d(shape);

    BoxShape<pvec_t> par_sh(0, 2 * PI);
    if (pdim == 2) {
        par_sh = BoxShape<pvec_t>({0, 0}, {PI, 2*PI});
    }
    DomainDiscretization<pvec_t> param_domain(par_sh);


    Timer t;
    t.addCheckPoint("start");

    sfill(d, param_domain, shape.template map<pdim>, shape.template dmap<pdim>, delta_r, -1);
    fill(d, delta_r, 1);

    int N = d.size();
    prn(N);
    d.findSupport(FindClosest(conf.get<int>("approx.n")));

    Polyharmonic<double, 3> phs;

    int aug = conf.get<int>("approx.aug");
    RBFFD<decltype(phs), vec_t, ScaleToClosest> approx(phs, Monomials<vec_t>(aug));
    auto storage = d.template computeShapes<sh::lap|sh::d1>(approx);

    SparseMatrix<double, RowMajor> M(N, N);
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);

    auto op = storage.implicitOperators(M, rhs);
    M.reserve(storage.supportSizes());

    for (int i : d.interior()) {
        op.lap(i) = 0;
    }
    for (int i : d.boundary()) {
        op.value(i) = 0;
    }

    file.atomic().writeDomain("d", d);
    file.atomic().writeSparseMatrix("M", M);
}

template <typename vec_t>
void run_all(const XML& conf, HDF& hdf) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;

    DemoShape<vec_t> sh;

    GeneralSurfaceFill<vec_t, pvec_t> sfill; sfill.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));

    int n = conf.get<int>("num.n");
    prn("nodes per side", n);

    auto fn = [=](const vec_t&) { return 1.0 / n; };
    test_run(conf, hdf, sh, sfill, fill_randomized, fn);
}

int main(int argc, const char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the parameter file.");
    XML conf(argv[1]);

    std::string out_file = conf.get<std::string>("meta.out_file");
    HDF hdf(out_file, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    int dim = conf.get<int>("case.dim");
    assert_msg(dim > 0, "Dimension must be positive, got %d.", dim);

    switch (dim) {
//        case 1: run_all<Vec1d>(conf, hdf); break;
        case 2: run_all<Vec2d>(conf, hdf); break;
        case 3: run_all<Vec3d>(conf, hdf); break;
//        case 4: run_all<Vec<double, 4>>(conf, hdf); break;
//        case 5: run_all<Vec<double, 5>>(conf, hdf); break;
        default: assert_msg(false, "Dimension %d not supported.", dim);
    }


    return 0;
}
