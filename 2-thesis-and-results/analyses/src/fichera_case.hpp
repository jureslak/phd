#ifndef ADAPTIVE_FICHERA_CASE_HPP
#define ADAPTIVE_FICHERA_CASE_HPP

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace std;
using namespace Eigen;

struct FicheraSolver {

    static double analytical(const Vec3d& p) {
        return std::sqrt(p.norm());
    }

    static double lap(const Vec3d& p) {
        return 0.75*std::pow(p.norm(), -1.5);
    }

    static double neumann(const Vec3d& p, const Vec3d& n) {
        return 0.5*std::pow(p.norm(), -1.5)*p.dot(n);
    }

    static void preprocess(XML& conf) {}

    template<typename vec_t, typename func_t>
    Eigen::VectorXd solve(DomainDiscretization<vec_t>& domain, const XML& conf, HDF& file, Timer& timer, const func_t& h) {
        int basis_size = conf.get<int>("approx.m");
        double sigmaB = conf.get<double>("approx.sigmaB");
        double sigmaW = conf.get<double>("approx.sigmaW");
        string basis = conf.get<string>("approx.basis_type");

        if (basis == "gau") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "mq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Multiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "phs") {
            int k = conf.get<int>("approx.k");
            int aug = conf.get<int>("approx.aug");
            RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(k, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "imq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<InverseMultiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "mon") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>(m), sigmaW);
            return solve_(conf, domain, file, timer, approx, h);
        } else if (basis == "mont") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>::tensorBasis(m), sigmaW);
            return solve_(conf, domain, file, timer, approx, h);
        }
        assert_msg(false, "Unknown basis type '%s'.", basis);
        throw "";
    }

    template<typename vec_t, typename approx_t, typename func_t>
    Eigen::VectorXd solve_(const XML& conf, DomainDiscretization<vec_t>& d, HDF& file, Timer& timer,
                           const approx_t& approx, const func_t& h) {
        double eps = 1e-5;

        int N = d.size();
        prn(N);
        
        Range<int> signs = {1, -1, 1};
        Range<int> interior = d.interior();
        Range<int> boundary = d.boundary();
        for (int i : boundary) {
            d.type(i) = -1;
            bool b = true;
            for (int j = 0; j < vec_t::dim; ++j) {
                if (d.pos(i, j) > -eps) { /* good */ } else {
                    b = false; break;
                }
            }
            if (b) d.type(i) = -2;
        }
        Range<int> dir = d.types() == -2;
        Range<int> neu = d.types() == -1;
//         Range<int> dir = d.types() < 0, neu = {};
        
        Range<int> gh = d.addGhostNodes(h, 0, neu);
        
        int ss = conf.get<int>("approx.n");
        d.findSupport(FindClosest(ss));
        
        timer.addCheckPoint("shapes");
        prn("shapes");

        auto storage = d.template computeShapes<sh::lap|sh::d1>(approx);

        timer.addCheckPoint("matrix");
        prn("matrix");

        int Nt = d.size();
        SparseMatrix<double, RowMajor> M(Nt, Nt);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(Nt);

        auto op = storage.implicitOperators(M, rhs);
        Range<int> reserve(Nt, ss);
        M.reserve(reserve);

        for (int i : interior) {
            op.lap(i) = lap(d.pos(i));
        }
        for (int i : neu) {
            op.neumann(i, d.normal(i)) = neumann(d.pos(i), d.normal(i));
            op.lap(i, gh[i]) = lap(d.pos(i));
        }
        for (int i : dir) {
            op.value(i) = analytical(d.pos(i));
        }

//        out_file.atomic().writeSparseMatrix("M", M);
//        out_file.atomic().writeDoubleArray("rhs", rhs);

        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();
        timer.addCheckPoint("compute");
        prn("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        prn("solve");

        VectorXd sol = solver.solve(rhs).head(N);

        timer.addCheckPoint("postprocess");
        file.atomic().writeEigen("sol", sol);

        d.removeNodes(d.types() == 0);
        d.findSupport(FindClosest(ss));
        
        return sol;
    }

};


#endif  // ADAPTIVE_FICHERA_CASE_HPP
