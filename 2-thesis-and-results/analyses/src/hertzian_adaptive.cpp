#include "estimators.hpp"
#include "adaptive_solver.hpp"
#include "hertzian_case.hpp"

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif


int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    HertzianSolver<Vec2d> solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    
    double H = conf.get<double>("case.H");
    BoxShape<Vec2d> shape({-H, -H}, {H, 0});

    Timer t;
    t.addCheckPoint("begin");

    DeviationEstimator estimator;

    double h0 = conf.get<double>("num.h0");
    double hr = conf.get<double>("num.hr");
    double hd = conf.get<double>("num.hd");
    auto h = [&](const Vec2d& v) { return h0; };
    auto limit_d = [&](const Vec2d& v) { return hd; };
    auto limit_r = [&](const Vec2d& v) { return hr; };

    adaptive_solve(conf, shape, solver, estimator, file, h, limit_d, limit_r);

    t.addCheckPoint("end");
    file.setGroupName("/");
    file.atomic().writeDoubleAttribute("total-time", t.duration("begin", "end"));
    prn(t.duration("begin", "end"));


    return 0;
}
