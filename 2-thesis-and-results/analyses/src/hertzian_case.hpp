#ifndef HERTZ_SOLVER_HPP
#define HERTZ_SOLVER_HPP

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace Eigen;
using namespace std;

const int LEFT = -1;
const int RIGHT = -2;
const int BOTTOM = -3;
const int TOP = -4;

template <typename vec_t>
struct HertzianSolver {
    typedef typename vec_t::scalar_t scalar_t;
    enum { dim = vec_t::dim };
    typedef std::pair<VectorField<scalar_t, dim>, VectorField<scalar_t, dim*(dim+1)/2>> solution_t;
    
    static Eigen::Matrix2d analytical(const Vec2d& p, double P, double R) {
        Matrix2d m;
        double x = p[0], y = p[1];
        double r1 = x*x + (R-y)*(R-y);
        double r2 = x*x + (R+y)*(R+y);
        double f = 2*P/M_PI;
        double sxx = -f*((R-y)*x*x/r1/r1 + (R+y)*x*x/r2/r2 - 1./R/2);
        double syy = -f*((R-y)*(R-y)*(R-y)/r1/r1 + (R+y)*(R+y)*(R+y)/r2/r2 - 1./R/2);
        double sxy = f*((R-y)*(R-y)*x/r1/r1 - (R+y)*(R+y)*x/r2/r2);
        m << sxx, sxy, sxy, syy;
        return m;
    };

    static void preprocess(XML& conf) {
        double E = conf.get<double>("phy.E");
        double nu = conf.get<double>("phy.nu");

        // parameter logic
        double mu = E / 2. / (1+nu);
        double lam = E * nu / (1-2*nu) / (1+nu);
        auto state = conf.get<std::string>("phy.state");
        assert_msg((state == "plane stress" || state == "plane strain"), "State is neither plane "
                "stress nor plane strain, but got '%s'.", state);
        if (state == "plane stress") {
            lam = 2 * mu * lam / (2 * mu + lam);  // plane stress
        }
        conf.set("phy.mu", mu);
        conf.set("phy.lam", lam);
        
        double F = conf.get<double>("phy.F");
        double R = conf.get<double>("phy.R");
    
        double Estar = E/(2*(1-nu*nu));
        double a = 2*std::sqrt(std::abs(F*R/(M_PI*Estar)));
        double p0 = std::sqrt(std::abs(F*Estar/(M_PI*R)));

        conf.set("phy.Estar", Estar);
        conf.set("phy.p0", p0);
        conf.set("phy.a", a);
    }

    template<typename func_t>
    solution_t solve(DomainDiscretization<vec_t>& domain, const XML& conf, HDF& file, Timer& timer, const func_t& h) {
        int basis_size = conf.get<int>("approx.m");
        double sigmaB = conf.get<double>("approx.sigmaB");
        double sigmaW = conf.get<double>("approx.sigmaW");
        string basis = conf.get<string>("approx.basis_type");

        if (basis == "gau") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "mq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Multiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "phs") {
            int k = conf.get<int>("approx.k");
            int aug = conf.get<int>("approx.aug");
            RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(k, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "imq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<InverseMultiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "mon") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>(m), sigmaW);
            return solve_(conf, domain, file, timer, approx, h);
        } else if (basis == "mont") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>::tensorBasis(m), sigmaW);
            return solve_(conf, domain, file, timer, approx, h);
        }
        assert_msg(false, "Unknown basis type '%s'.", basis);
        throw "";
    }

private:
    
    template<typename approx_t, typename func_t>
    solution_t solve_(const XML& conf, DomainDiscretization<vec_t>& d, HDF& file, Timer& timer,
                      const approx_t& approx, const func_t& h) {

        int N = d.size();
        prn(N);
        const double lam = conf.get<double>("phy.lam");
        const double mu = conf.get<double>("phy.mu");
        double p0 = conf.get<double>("phy.p0");
        double a = conf.get<double>("phy.a");
        double H = conf.get<double>("case.H");

        Range<int> interior = d.interior();
        for (int i : d.boundary()) {
            if (d.pos(i, 1) >= -1e-10 && -H+1e-10 < d.pos(i, 0) && d.pos(i, 0) < H-1e-10 ) {
                d.type(i) = -1;
            } else {
                d.type(i) = -2;
            }
        }
        Range<int> top = d.types() == -1, rest = d.types() == -2;
        Range<int> gh = d.addGhostNodes(h, 0, top);
        
        int Nt = d.size();
        
        const int ss = conf.get<int>("approx.n");
        d.findSupport(FindClosest(ss));
      
        timer.addCheckPoint("shapes");
        prn("shapes");

        auto storage = d.computeShapes(approx);

        timer.addCheckPoint("matrix");
        prn("matrix");

        const int dim = vec_t::dim;
        SparseMatrix<double, RowMajor> M(dim*Nt, dim*Nt);
//         Range<int> ss = storage.supportSizes();
//         ss.append(ss+ss); for (int& c : ss) { c *= dim; }
        
        M.reserve(Range<int>(dim*Nt, dim*ss));
        VectorXd rhs = Eigen::VectorXd::Zero(dim*Nt);

        auto op = storage.implicitVectorOperators(M, rhs);

        for (int i : interior) {
            (lam+mu)*op.graddiv(i) + mu*op.lap(i) = 0;
        }
        for (int i : top) {
            Vec2d normal = d.normal(i);
            double x = d.pos(i, 0);
            double traction = (std::abs(x) < a) ? -p0 * std::sqrt(1 - x*x/a/a) : 0;
            op.traction(i, lam, mu, normal) = traction*normal;
            (lam+mu)*op.graddiv(i, gh[i]) + mu*op.lap(i, gh[i]) = 0;
        }
        for (int i : rest) {
            op.value(i) = 0;
        }
        
//        out_file.atomic().writeSparseMatrix("M", M);
//        out_file.atomic().writeDoubleArray("rhs", rhs);

        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();
        timer.addCheckPoint("compute");
        prn("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        prn("solve");

        VectorXd sol = solver.solve(rhs);
        timer.addCheckPoint("postprocess");
        VectorField2d disp = VectorField2d::fromLinear(sol);
        VectorField<double, 3> stress(N);
        auto eop = storage.explicitVectorOperators();
        for (int i = 0; i < N; ++i) {
            auto grad = eop.grad(disp, i);
            Matrix2d eps = 0.5*(grad + grad.transpose());
            Matrix2d s = lam * eps.trace() * Matrix2d::Identity(2, 2) + 2*mu*eps;
            stress[i][0] = s(0, 0);
            stress[i][1] = s(1, 1);
            stress[i][2] = s(0, 1);
        }

        
        file.atomic().writeEigen("displ", disp.topRows(N));
        file.atomic().writeEigen("stress", stress.topRows(N));

        d.removeNodes(d.types() == 0);
        d.findSupport(FindClosest(ss));
        
        solution_t r;
        r.first = disp;
        r.second = stress;
        return r;
    }
};

#endif  // HERTZ_SOLVER_HPP
