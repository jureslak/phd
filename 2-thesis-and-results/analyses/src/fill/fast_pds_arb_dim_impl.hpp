#ifndef FILLALGORITMS_FAST_PDS_ARB_DIM_IMPL_HPP
#define FILLALGORITMS_FAST_PDS_ARB_DIM_IMPL_HPP

#include <medusa/Config.hpp>
#include <medusa/bits/spatial_search/KDGrid.hpp>
#include "fast_pds_arb_dim.hpp"
#include <vector>
#include <array>
#include <iostream>
#include <random>

template <typename G>
int get_random(const std::vector<int>& v, G& generator) {
    return std::uniform_int_distribution<int>(0, v.size() - 1)(generator);
}

void remove(std::vector<int>& v, int i) {
    v[i] = v.back();
    v.pop_back();
}

template <int dim, typename scalar_t, typename G>
Eigen::Matrix<scalar_t, dim, 1> generate_candidate(
        const Eigen::Matrix<scalar_t, dim, 1>& p, scalar_t r, G& generator) {
    Eigen::Matrix<scalar_t, dim, 1> c;
    std::normal_distribution<scalar_t> dist(0, 1);
    for (int i = 0; i < dim; ++i) {
        c[i] = dist(generator);
    }
    c.normalize();

    scalar_t rd = std::uniform_real_distribution<scalar_t>(std::pow(r, dim), std::pow(2*r, dim))(generator);
    return p + c*std::pow(rd, 1.0/dim);
}

template <int dim, typename scalar_t>
bool inside(const Eigen::Matrix<scalar_t, dim, 1>& p, const Eigen::Matrix<scalar_t, dim, 1>& bot, const Eigen::Matrix<scalar_t, dim, 1>& top) {
    for (int i = 0; i < dim; ++i) {
        if (!(bot[i] <= p[i] && p[i] <= top[i])) {
            return false;
        }
    }
    return true;
}

template <int dim, typename scalar_t, typename G>
std::vector<Eigen::Matrix<scalar_t, dim, 1>> fast_pds(
        const Eigen::Matrix<scalar_t, dim, 1>& bot,
        const Eigen::Matrix<scalar_t, dim, 1>& top,
        scalar_t r, G& generator, int n) {

    scalar_t cell_size = r / std::sqrt((double)dim);
    mm::KDGrid<Eigen::Matrix<scalar_t, dim, 1>> grid(bot, top, cell_size);

    Eigen::Matrix<scalar_t, dim, 1> p;
    for (int i = 0; i < dim; ++i) {
        std::uniform_real_distribution<scalar_t> d(bot[i], top[i]);
        p[i] = d(generator);
    }
    std::vector<int> active = { grid.insert(p) };
    while (!active.empty()) {
        int idx = get_random(active, generator);
        int cur_idx = active[idx];
//        std::cout << "cur_idx = " << cur_idx << std::endl;
//        std::cout << "active_size = " << active.size() << std::endl;
//        std::cout << "result_size = " << result.size() << std::endl;
//        std::cout << "considering point " << cur_idx << " with coordinates " << result[cur_idx].transpose() << std::endl;
        bool added = false;
        for (int j = 0; j < n; ++j) {
            Eigen::Matrix<scalar_t, dim, 1> c = generate_candidate(grid.point(cur_idx), r, generator);

//            std::cout << "considering candidate: " << c.transpose() << std::endl;
            if (!inside(c, bot, top)) continue;
            auto index = grid.compute_index(c);
//            if (!grid.grid().inBounds(index)) continue;
//            std::cout << "candidate is in the domain" << std::endl;
            if (grid.existsPointInSphere(c, r, index)) continue;
//            std::cout << grid << std::endl;

//            // TODO debug here
//            if (grid.grid()(index) != -1) {
//                Eigen::Matrix<scalar_t, dim, 1> a = grid.point(grid.grid()(index));
//                Eigen::Matrix<scalar_t, dim, 1> b = c;
//                prn(top)
//                prn(bot)
//                prn(a);
//                prn(b);
//                prn((a-b).norm());
//                prn(cell_size)
//                auto aindex = grid.compute_index(a);
//                auto bindex = grid.compute_index(b);
//                prn(aindex)
//                prn(bindex)
//            }

            active.push_back(grid.insert(c, index));
            added = true;
        }
        if (!added) {
            remove(active, idx);
        }
//        std::cout << "---------" << std::endl;
    }

//    std::cout << "total points: " << result.size() << std::endl;
    return grid.points();
}

#endif //FILLALGORITMS_FAST_PDS_ARB_DIM_IMPL_HPP
