//
// Created by jureslak on 23.10.2018.
//

#ifndef FILLALGORITMS_SKFFILL_HPP
#define FILLALGORITMS_SKFFILL_HPP

#include <medusa/Config.hpp>
#include <medusa/bits/domains/DomainDiscretization.hpp>
#include <medusa/bits/types/Range_fwd.hpp>
#include <medusa/bits/utils/randutils.hpp>
#include "fast_pds_arb_dim.hpp"
#include <random>
#include <vector>

namespace mm {

template <typename vec_t>
class DomainDiscretization;

/**
 * Poisson Disk Sampling based fill algorithm. The algorithm starts in random node and adds
 * nodes around it on a circle with radius defined in a supplied density function.
 * In each next iteration it selects one of the previously added nodes and repeats the adding.
 * The main bottleneck right now is a search structure. First step in optimization
 * would be better tree bookkeeping, i.e. determine which nodes cannot be in a way anymore
 * this might become tricky with variable nodal densities.
 */
template <typename vec_t>
class PDS {
    int max_points = 10000000;  ///< Max number of iterations performed.
    int n_samples = 15;  ///< number of random sample to try in each iteration
    int seed_;  ///<  User supplied seed if -1 use generate std::random_device.

  public:
    typedef DomainDiscretization<vec_t> domain_t;  ///< Domain discretization type.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar type;
    typedef vec_t vector_t;  ///< Vector type.
    enum {
        dim = vec_t::dim
    };

    PDS& seed(int seed) { seed_ = seed; return *this; }
    PDS() : seed_(get_seed()) {}


    /// Maximal number of points generated.
    PDS& maxPoints(int max_points) {
        this->max_points = max_points;
        return *this;
    }

    PDS& numSamples(int n_samples) {
        this->n_samples = n_samples;
        return *this;
    }

  public:
    /// Fills given domain with distribution `delta_r`.
    template <typename func_t>
    void operator()(domain_t& domain, const func_t& delta_r, int type) const {
        vec_t top, bottom;
        std::tie(top, bottom) = domain.shape().bbox();
        scalar_t r = delta_r(0);
        std::mt19937 gen(seed_);
        auto pts = fast_pds(top, bottom, r, gen, n_samples);

        // filter to domain
        if (type == 0) type = -1;
        double zeta = 1-1e-10;
        KDTree<vec_t> kdtree(domain.positions());
        for (const auto& f : pts) {
            if (!domain.shape().contains(f)) continue;
            if (kdtree.size() > 0) {
                scalar_t r2 = kdtree.query(f).second[0];
                if (r2 < zeta * zeta * r * r) continue;
            }
            domain.addInternalNode(f, type);
        }
    }

};

}
#endif //FILLALGORITMS_SKFFILL_HPP
