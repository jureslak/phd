#include <queue>
#include <list>
#include <iostream>
#include <tuple>
#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <medusa/bits/domains/GeneralSurfaceFill.hpp>
#include "PDS.hpp"
#include "demo_shapes.hpp"

using namespace std;
using namespace Eigen;
using namespace mm;

template <typename vec_t>
void run(const XML& conf, HDF& hdf) {
    double h0 = conf.get<double>("num.h0");

    auto h = [=](const vec_t&) { return h0; };
    
    auto br = [=](const Vec1d& t) {
        return Vec3d(
                0.5*(2+std::cos(t[0]/2))*std::cos(t[0]),
                0.5*(2+std::cos(t[0]/2))*std::sin(t[0]),
                0.5*std::sin(t[0]/2));
    };
    auto dbr = [=](const Vec1d& t) { return Vec3d(1./8*(-std::sin(t[0]/2) - 8*std::sin(t[0]) - 3*std::sin(1.5*t[0])), 
                                                  1./8*(std::cos(t[0]/2) + 8*std::cos(t[0]) + 3*std::cos(1.5*t[0])), 1./4*std::cos(t[0]/2)); };
    auto ir = [=](const Vec2d& u) { return Vec3d((1 + u[1]/2*std::cos(u[0]/2))*std::cos(u[0]), (1 + u[1]/2*std::cos(u[0]/2))*std::sin(u[0]), u[1]/2*std::sin(u[0]/2)); };
    auto dir = [=](const Vec2d& u) {
        Eigen::Matrix<double, 3, 2> j;
        j(0, 0) = -0.25*(8*std::cos(u[0]/2) + u[1]*(2+3*std::cos(u[0])))*std::sin(u[0]/2);
        j(0, 1) = 0.5*std::cos(u[0]/2)*std::cos(u[0]);
        j(1, 0) = std::cos(u[0]) + 0.125*u[1]*(std::cos(0.5*u[0]) + 3*std::cos(1.5*u[0]));
        j(1, 1) = 0.5*std::cos(u[0]/2)*std::sin(u[0]);
        j(2, 0) = 0.25*std::cos(u[0]/2)*u[1];
        j(2, 1) = 0.5*std::sin(u[0]/2);
        return j;
    };

    UnknownShape<vec_t> sh;
    DomainDiscretization<vec_t> domain(sh);

    BoxShape<Vec1d> bnd_par(0, 4*PI);
    DomainDiscretization<Vec1d> bnd_par_domain(bnd_par); bnd_par_domain.addInternalNode(0.0, 1);
    domain.addInternalNode(br(0.0), 1);
    
    GeneralSurfaceFill<vec_t, Vec1d> sfill_bnd; sfill_bnd.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    sfill_bnd(domain, bnd_par_domain, br, dbr, h, -1);


    BoxShape<Vec2d> par_sh({0, -1}, {2*PI, 1});
    DomainDiscretization<Vec2d> param_domain(par_sh); param_domain.addInternalNode({PI, 0.0}, 1);
    domain.addInternalNode(ir({PI, 0.0}), 1);

    GeneralSurfaceFill<vec_t, Vec2d> sfill; sfill.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    sfill(domain, param_domain, ir, dir, h, -1);

    hdf.atomic().writeDomain("domain", domain);
}

int main(int argc, const char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the parameter file.");
    XML conf(argv[1]);

    std::string out_file = conf.get<std::string>("meta.out_file");
    HDF hdf(out_file, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    run<Vec3d>(conf, hdf);

    return 0;
}
