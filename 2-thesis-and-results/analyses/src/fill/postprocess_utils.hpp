#ifndef PHD_POSTPROCESS_UTILS_HPP
#define PHD_POSTPROCESS_UTILS_HPP

#include <medusa/bits/spatial_search/KDTree.hpp>
#include <medusa/bits/domains/DomainShape.hpp>
#include <medusa/bits/utils/numutils.hpp>

template <typename vec_t>
typename vec_t::scalar_t min_dist(const std::vector<vec_t>& pts, const mm::KDTree<vec_t>& tree) {
    typename vec_t::scalar_t d = 1.0/0.0;
    int n = pts.size();
    for (int i = 0; i < n; ++i) {
        typename vec_t::scalar_t cd = tree.query(pts[i], 2).second[1];
        if (cd < d) {
            d = cd;
        }
    }
    return std::sqrt(d);
}

template <typename vec_t, typename fn_t>
typename vec_t::scalar_t min_dist_scaled(const std::vector<vec_t>& pts, const mm::KDTree<vec_t>& tree, const fn_t& h) {
    typename vec_t::scalar_t d = 1.0/0.0;
    int n = pts.size();
    for (int i = 0; i < n; ++i) {
        typename vec_t::scalar_t cd = tree.query(pts[i], 2).second[1], hd = h(pts[i]);
        if (cd/hd/hd < d) {
            d = cd/hd/hd;
        }
    }
    return std::sqrt(d);
}

template <typename vec_t>
typename vec_t::scalar_t min_dist(const std::vector<vec_t>& pts) {
    mm::KDTree<vec_t> tree(pts);
    return min_dist(pts, tree);
}

template <typename vec_t>
typename vec_t::scalar_t max_dist(const std::vector<vec_t>& pts, const mm::DomainShape<vec_t>& shape, const mm::KDTree<vec_t>& tree) {
    auto [bot, top] = shape.bbox();
    typename vec_t::scalar_t d = 0.0;
    mm::Range<vec_t> samples = mm::linspace(bot, top, 100);
    for (const vec_t& s : samples) {
        if (shape.contains(s)) {
            typename vec_t::scalar_t cd = tree.query(s).second[0];
            if (d < cd) {
                d = cd;
            }
        }
    }
    return 2*std::sqrt(d);
}

template <typename vec_t, typename fn_t>
typename vec_t::scalar_t max_dist_scaled(const std::vector<vec_t>& pts, const mm::DomainShape<vec_t>& shape, const mm::KDTree<vec_t>& tree, const fn_t& h) {
    auto [bot, top] = shape.bbox();
    mm::Range<vec_t> samples = mm::linspace(bot, top, 100);
    int N = pts.size();
    mm::Range<double> radii(N, 0.0);
    for (const vec_t& s : samples) {
        if (shape.contains(s)) {
            auto [I, nd] = tree.query(s);
            radii[I[0]] = std::max(radii[I[0]], nd[0]);
        }
    }
    typename vec_t::scalar_t d = 0.0;
    for (int i = 0; i < N; ++i) {
        typename vec_t::scalar_t hd = h(pts[i]);
        if (radii[i]/hd/hd > d) {
            d = radii[i]/hd/hd;
        }
    }
    return 2*std::sqrt(d);
}


template <typename vec_t>
typename vec_t::scalar_t max_dist(const std::vector<vec_t>& pts, const mm::DomainShape<vec_t>& shape) {
    mm::KDTree<vec_t> tree(pts);
    return max_dist(pts, shape, tree);
}


#endif //PHD_POSTPROCESS_UTILS_HPP
