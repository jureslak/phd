#include <vector>
#include <queue>
#include <list>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <cmath>
#include <chrono>
#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/GrainDropFill.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <Eigen/Core>
#include <medusa/bits/spatial_search/KDGrid.hpp>
#include "PDS.hpp"
#include "postprocess_utils.hpp"
#include "demo_shapes.hpp"

using namespace std;
using namespace Eigen;
using namespace mm;

using timer = std::chrono::steady_clock;

template <typename vec_t>
class GeneralGridFill : public GeneralFill<vec_t> {
  public:
    template <typename func_t>
    void operator()(DomainDiscretization<vec_t>& domain, const func_t& h, int type = 0) const {
        double h0 = h(0);
        int dim = vec_t::dim;
        KDGrid<vec_t> grid(domain.shape().bbox().first, domain.shape().bbox().second, h0/std::sqrt(dim));
        GeneralFill<vec_t>::operator()(domain, h, grid, type);
    }
};

template <typename vec_t, typename func_t, typename fill_t>
void test_run(const DomainShape<vec_t>& shape, const fill_t& fill,  const func_t& delta_r,  int runs,
         int i, Eigen::MatrixXd& Ns, Eigen::MatrixXd& times, Eigen::MatrixXd& hs, Eigen::MatrixXd& qs) {
    for (int j = 0; j < runs; ++j) {
        prn(j)
        DomainDiscretization<vec_t> d = shape.discretizeBoundaryWithDensity(delta_r);
        auto start = timer::now();
        fill(d, delta_r, 1);
        double sec = (timer::now() - start).count() / 1e9;
        Ns(i, j) = d.size();
        times(i, j) = sec;

        KDTree<vec_t> tree(d.positions());
        qs(i, j) = min_dist(d.positions(), tree);
        hs(i, j) = max_dist(d.positions(), shape, tree);
    }
}

template <typename vec_t>
void run_all(const XML& conf, HDF& hdf) {
//    vec_t bot = 0.0, top = 1.0;
//    BoxShape<vec_t> sh(bot, top);
    DemoShape<vec_t> sh;

    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf.get<int>("case.RFn")).seed(1337);

    GeneralGridFill<vec_t> fill_grid;
    fill_grid.numSamples(conf.get<int>("case.RFn")).seed(1337);

    auto [bot, top] = sh.bbox();
    GrainDropFill<vec_t> fill_ff(bot, top); fill_ff.maxPoints(1e7);

    PDS<vec_t> fill_skf;
    fill_skf.numSamples(conf.get<int>("case.SKFn")).seed(1337);

    int n_runs = conf.get<int>("num.nruns");
    vector<string> ns = split(conf.get<std::string>("num.ns"), ',');

    Eigen::MatrixXd PDS_RF_N(ns.size(), n_runs);
    Eigen::MatrixXd PDS_GRID_N(ns.size(), n_runs);
    Eigen::MatrixXd FF_N(ns.size(), n_runs);
    Eigen::MatrixXd SKF_N(ns.size(), n_runs);

    Eigen::MatrixXd PDS_RF_T(ns.size(), n_runs);
    Eigen::MatrixXd PDS_GRID_T(ns.size(), n_runs);
    Eigen::MatrixXd FF_T(ns.size(), n_runs);
    Eigen::MatrixXd SKF_T(ns.size(), n_runs);


    Eigen::MatrixXd PDS_RF_H(ns.size(), n_runs);
    Eigen::MatrixXd PDS_GRID_H(ns.size(), n_runs);
    Eigen::MatrixXd FF_H(ns.size(), n_runs);
    Eigen::MatrixXd SKF_H(ns.size(), n_runs);


    Eigen::MatrixXd PDS_RF_S(ns.size(), n_runs);
    Eigen::MatrixXd PDS_GRID_S(ns.size(), n_runs);
    Eigen::MatrixXd FF_S(ns.size(), n_runs);
    Eigen::MatrixXd SKF_S(ns.size(), n_runs);

    for (size_t i = 0; i < ns.size(); ++i) {
        fill_skf.seed(i);
        fill_randomized.seed(i);
        fill_grid.seed(i);
        fill_ff.seed(i);

        int n = std::stoi(ns[i]);
        prn("case", i);
        prn("nodes per side", n);

        fill_ff.initialSpacing(0.1/n);
        auto fn = [=](const vec_t&) { return 1.0 / n; };

        prn("PDS-RF")
        test_run(sh, fill_randomized, fn, n_runs, i, PDS_RF_N, PDS_RF_T, PDS_RF_H, PDS_RF_S);
        prn("PDS-RF-Grid")
        test_run(sh, fill_grid, fn, n_runs, i, PDS_GRID_N, PDS_GRID_T, PDS_GRID_H, PDS_GRID_S);
        prn("FF")
        test_run(sh, fill_ff, fn, n_runs, i, FF_N, FF_T, FF_H, FF_S);
        prn("SKF")
        test_run(sh, fill_skf, fn, n_runs, i, SKF_N, SKF_T, SKF_H, SKF_S);
    }

    hdf.reopen();
    hdf.openGroup("/FF");
    hdf.writeEigen("times", FF_T);
    hdf.writeEigen("Ns", FF_N);
    hdf.writeEigen("h", FF_H);
    hdf.writeEigen("s", FF_S);

    hdf.openGroup("/PDS-RF");
    hdf.writeEigen("times", PDS_RF_T);
    hdf.writeEigen("Ns", PDS_RF_N);
    hdf.writeEigen("h", PDS_RF_H);
    hdf.writeEigen("s", PDS_RF_S);

    hdf.openGroup("/PDS-RF-grid");
    hdf.writeEigen("times", PDS_GRID_T);
    hdf.writeEigen("Ns", PDS_GRID_N);
    hdf.writeEigen("h", PDS_GRID_H);
    hdf.writeEigen("s", PDS_GRID_S);

    hdf.openGroup("/SKF");
    hdf.writeEigen("times", SKF_T);
    hdf.writeEigen("Ns", SKF_N);
    hdf.writeEigen("h", SKF_H);
    hdf.writeEigen("s", SKF_S);
}

int main(int argc, const char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the parameter file.");
    XML conf(argv[1]);

    std::string out_file = conf.get<std::string>("meta.out_file");
    HDF hdf(out_file, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    int dim = conf.get<int>("case.dim");
    assert_msg(dim > 0, "Dimension must be positive, got %d.", dim);

    switch (dim) {
//        case 1: run_all<Vec1d>(conf, hdf); break;
        case 2: run_all<Vec2d>(conf, hdf); break;
        case 3: run_all<Vec3d>(conf, hdf); break;
//        case 4: run_all<Vec<double, 4>>(conf, hdf); break;
//        case 5: run_all<Vec<double, 5>>(conf, hdf); break;
        default: assert_msg(false, "Dimension %d not supported.", dim);
    }


    return 0;
}
