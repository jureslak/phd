#include <queue>
#include <list>
#include <iostream>
#include <tuple>
#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <medusa/bits/domains/GeneralSurfaceFill.hpp>
#include "PDS.hpp"
#include "demo_shapes.hpp"

using namespace std;
using namespace Eigen;
using namespace mm;

template <typename vec_t>
void run(const XML& conf, HDF& hdf) {
    double h0 = conf.get<double>("num.h0");
    double h1 = conf.get<double>("num.h1");

    vec_t p1 = 0, p2 = 0, p3 = 0;
    p1[0] = 0.25;
    p1[1] = std::sqrt(3)/4.0;
    p2[0] = 0.25;
    p2[1] = -std::sqrt(3)/4.0;
    p3[0] = -0.5;
    p3[1] = 0;
    
    auto h = [=](const vec_t& p) { return h0 + std::min(std::min((p-p1).norm(), (p-p2).norm()), (p-p3).norm())*(h1-h0); };
        
    DemoShape<vec_t> sh;
    DomainDiscretization<vec_t> domain(sh);
    BoxShape<Vec2d> par_sh({0, 0}, {PI, 2*PI});
    DomainDiscretization<Vec2d> param_domain(par_sh); 
//     param_domain.addInternalNode(0.0, 1);
    
    GeneralSurfaceFill<vec_t, Vec2d> sfill; sfill.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    sfill(domain, param_domain, sh.template map<2>, sh.template dmap<2>, h, -1);

    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    fill_randomized(domain, h, 1);

    prn("N", domain.size());
    
//    int ss = conf.get<int>("num.ss");
//    domain.findSupport(FindClosest(ss));
//
//    hdf.atomic().writeInt2DArray("supp", domain.supports());
    hdf.atomic().writeDomain("domain", domain);
}

int main(int argc, const char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the parameter file.");
    XML conf(argv[1]);

    std::string out_file = conf.get<std::string>("meta.out_file");
    HDF hdf(out_file, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    run<Vec3d>(conf, hdf);

    return 0;
}
