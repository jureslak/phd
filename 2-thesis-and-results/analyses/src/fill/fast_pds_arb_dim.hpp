#ifndef FILLALGORITMS_FAST_PDS_ARB_DIM_HPP
#define FILLALGORITMS_FAST_PDS_ARB_DIM_HPP

#include <vector>
#include <Eigen/Core>

//namespace Eigen {
//template<typename _Scalar, int _Rows, int _Cols,
//        int _Options,
//        int _MaxRows,
//        int _MaxCols
//> class Matrix;
//}


template <int dim, typename scalar_t, typename G>
std::vector<Eigen::Matrix<scalar_t, dim, 1>> fast_pds(
        const Eigen::Matrix<scalar_t, dim, 1>& bot,
        const Eigen::Matrix<scalar_t, dim, 1>& top,
        scalar_t r, G& generator, int n);

#endif //FILLALGORITMS_FAST_PDS_ARB_DIM_HPP
