#include "fast_pds_arb_dim_impl.hpp"
#include "fast_pds_arb_dim.hpp"
#include <random>
#include <Eigen/Core>

template std::vector<Eigen::Matrix<double, 1, 1>> fast_pds(const Eigen::Matrix<double, 1, 1>& bot, const Eigen::Matrix<double, 1, 1>& top, double r, std::mt19937& generator, int n);
template std::vector<Eigen::Matrix<double, 2, 1>> fast_pds(const Eigen::Matrix<double, 2, 1>& bot, const Eigen::Matrix<double, 2, 1>& top, double r, std::mt19937& generator, int n);
template std::vector<Eigen::Matrix<double, 3, 1>> fast_pds(const Eigen::Matrix<double, 3, 1>& bot, const Eigen::Matrix<double, 3, 1>& top, double r, std::mt19937& generator, int n);
