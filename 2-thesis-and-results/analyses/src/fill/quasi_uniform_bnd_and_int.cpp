#include <vector>
#include <queue>
#include <list>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <cmath>
#include <chrono>
#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/GrainDropFill.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <medusa/bits/domains/GeneralSurfaceFill.hpp>
#include <Eigen/Core>
#include <medusa/bits/spatial_search/KDGrid.hpp>
#include "PDS.hpp"
#include "postprocess_utils.hpp"
#include "demo_shapes.hpp"

using namespace std;
using namespace Eigen;
using namespace mm;

using timer = std::chrono::steady_clock;

template <typename vec_t, typename func_t, typename sfill_t, typename fill_t>
void test_run(const DemoShape<vec_t>& shape, const sfill_t& sfill, const fill_t& fill,  const func_t& delta_r,  int runs,
         int i, Eigen::MatrixXd& Ns, Eigen::MatrixXd& times, Eigen::MatrixXd& hs, Eigen::MatrixXd& qs) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;

    for (int j = 0; j < runs; ++j) {
        prn(j)
        DomainDiscretization<vec_t> d(shape);

        BoxShape<pvec_t> par_sh(0, 2 * PI);
        if (pdim == 2) {
            par_sh = BoxShape<pvec_t>({0, 0}, {PI, 2*PI});
        }
        DomainDiscretization<pvec_t> param_domain(par_sh);

        auto start = timer::now();
        sfill(d, param_domain, shape.template map<pdim>, shape.template dmap<pdim>, delta_r, -1);
        fill(d, delta_r, 1);
        double sec = (timer::now() - start).count() / 1e9;
        Ns(i, j) = d.size();
        times(i, j) = sec;

        KDTree<vec_t> tree(d.positions());
        qs(i, j) = min_dist(d.positions(), tree);
        hs(i, j) = max_dist(d.positions(), shape, tree);
    }
}

template <typename vec_t>
void run_all(const XML& conf, HDF& hdf) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;

    DemoShape<vec_t> sh;

    GeneralSurfaceFill<vec_t, pvec_t> sfill; sfill.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));

    int n_runs = conf.get<int>("num.nruns");
    vector<string> ns = split(conf.get<std::string>("num.ns"), ',');

    Eigen::MatrixXd PDS_RF_N(ns.size(), n_runs);
    Eigen::MatrixXd PDS_RF_T(ns.size(), n_runs);
    Eigen::MatrixXd PDS_RF_H(ns.size(), n_runs);
    Eigen::MatrixXd PDS_RF_S(ns.size(), n_runs);

    for (size_t i = 0; i < ns.size(); ++i) {
        fill_randomized.seed(i);

        int n = std::stoi(ns[i]);
        prn("case", i);
        prn("nodes per side", n);

        auto fn = [=](const vec_t&) { return 1.0 / n; };

        prn("PDS-RF")
        test_run(sh, sfill, fill_randomized, fn, n_runs, i, PDS_RF_N, PDS_RF_T, PDS_RF_H, PDS_RF_S);
    }

    hdf.reopen();
    hdf.openGroup("/PDS-RF");
    hdf.writeEigen("times", PDS_RF_T);
    hdf.writeEigen("Ns", PDS_RF_N);
    hdf.writeEigen("h", PDS_RF_H);
    hdf.writeEigen("s", PDS_RF_S);
}

int main(int argc, const char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the parameter file.");
    XML conf(argv[1]);

    std::string out_file = conf.get<std::string>("meta.out_file");
    HDF hdf(out_file, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    int dim = conf.get<int>("case.dim");
    assert_msg(dim > 0, "Dimension must be positive, got %d.", dim);

    switch (dim) {
//        case 1: run_all<Vec1d>(conf, hdf); break;
        case 2: run_all<Vec2d>(conf, hdf); break;
        case 3: run_all<Vec3d>(conf, hdf); break;
//        case 4: run_all<Vec<double, 4>>(conf, hdf); break;
//        case 5: run_all<Vec<double, 5>>(conf, hdf); break;
        default: assert_msg(false, "Dimension %d not supported.", dim);
    }


    return 0;
}
