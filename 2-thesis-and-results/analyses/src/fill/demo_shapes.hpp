#ifndef PHD_DEMO_SHAPES_HPP
#define PHD_DEMO_SHAPES_HPP

#include <medusa/Config.hpp>
#include <medusa/bits/domains/DomainShape.hpp>
#include <cmath>
#include <iostream>
#include <utility>

template <typename vec_t>
class DemoShape : public mm::DomainShape<vec_t> {
  public:
    typedef vec_t vector_t;
    typedef typename vec_t::scalar_t scalar_t;
    bool contains(const vec_t& point) const override {
        if (vec_t::dim == 2) {
            return point.norm() < radius<1>(std::atan2(point[1], point[0]));
        } else if (vec_t::dim == 3) {
            scalar_t r = point.norm();
            scalar_t f = std::atan2(point[1], point[0]);
            scalar_t th = std::acos(point[2]/r);
            return r < radius<2>({th, f});
        }
    }

    std::pair<vec_t, vec_t> bbox() const override {
        vec_t a, b;
        if (vec_t::dim == 2) {
            a = -1; b = 1;
        } else if (vec_t::dim == 3) {
            a = {-2, -2, -1};
            b = {2, 2, 1};
        }
        return {a, b};
    }

    mm::DomainShape<vec_t>* clone() const override { return new DemoShape<vec_t>(); }
    std::ostream& print(std::ostream& os) const override { return os << "PolarShape"; }
    mm::DomainDiscretization<vec_t>
    discretizeBoundaryWithDensity(const std::function<scalar_t(vec_t)>& dr, int type) const override {
        auto d = mm::DomainDiscretization<vec_t>(*this);
        return d;
    }
    
    template <int dim>
    static scalar_t radius(const mm::Vec<scalar_t, dim>& xi) {
        if (dim == 1) {
            return 0.25 * (3 + std::cos(3 * xi[0]));
        } else if (dim == 2) {
            return 0.25*(2+xi[0]*xi[0]*(mm::PI-xi[0])*(mm::PI-xi[0])/4 * (2+std::cos(3*xi[1])));
        }
        assert_msg(false, "Invalid dimension %d", dim);
        return -1.0;
    }
        
    template <int dim>
    static mm::Vec<scalar_t, dim> dradius(const mm::Vec<scalar_t, dim>& xi) {
        mm::Vec<scalar_t, dim> dr;
        if (dim == 1) {
            dr[0] = -0.75*std::sin(3*xi[0]);
            return dr;
        } else if (dim == 2) {
            dr[0] = 1.0/8*xi[0]*(mm::PI*mm::PI - 3*mm::PI*xi[0] + 2*xi[0]*xi[0])*(2+std::cos(3*xi[1]));
            dr[1] = -3.0/16*xi[0]*xi[0]*(mm::PI-xi[0])*(mm::PI-xi[0]) * std::sin(3*xi[1]);
            return dr;
        }
        assert_msg(false, "Invalid dimension %d", dim);
        return dr;
    }
    
    template <int dim>
    static vec_t map(const mm::Vec<scalar_t, dim>& xi) {
        vec_t p;
        scalar_t r = radius(xi);
        if (dim == 1) {
            p[0] = r*std::cos(xi[0]);
            p[1] = r*std::sin(xi[0]);
            return p;
        } else if (dim == 2) {
            p[0] = r*std::sin(xi[0])*std::cos(xi[1]);
            p[1] = r*std::sin(xi[0])*std::sin(xi[1]);
            p[2] = r*std::cos(xi[0]);
            return p;
        }
        assert_msg(false, "Invalid dimension %d", dim);
        return p;
    }
    
    template <int dim>
    static Eigen::Matrix<scalar_t, vec_t::dim, dim> dmap(const mm::Vec<scalar_t, dim>& xi) {
        scalar_t r = radius(xi);
        mm::Vec<scalar_t, dim> dr = dradius(xi);
        Eigen::Matrix<scalar_t, vec_t::dim, dim> p;
        if (dim == 1) {
            p(0, 0) = -r*std::sin(xi[0]) + dr[0]*std::cos(xi[0]);
            p(1, 0) = r*std::cos(xi[0]) + dr[0]*std::sin(xi[0]);
            return p;
        } else if (dim == 2) {
            p(0, 0) =  r*std::cos(xi[0])*std::cos(xi[1]) + dr[0]*std::sin(xi[0])*std::cos(xi[1]);
            p(0, 1) = -r*std::sin(xi[0])*std::sin(xi[1]) + dr[1]*std::sin(xi[0])*std::cos(xi[1]);
            p(1, 0) =  r*std::cos(xi[0])*std::sin(xi[1]) + dr[0]*std::sin(xi[0])*std::sin(xi[1]);
            p(1, 1) =  r*std::sin(xi[0])*std::cos(xi[1]) + dr[1]*std::sin(xi[0])*std::sin(xi[1]);
            p(2, 0) =  -r*std::sin(xi[0]) + dr[0]*std::cos(xi[0]);
            p(2, 1) =  dr[1]*std::cos(xi[0]);
            return p;
        }
        assert_msg(false, "Invalid dimension %d", dim);
        return p;
    }
};


#endif //PHD_DEMO_SHAPES_HPP
