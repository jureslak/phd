#ifndef ADAPTIVE_ESTIMATORS_HPP
#define ADAPTIVE_ESTIMATORS_HPP

#include <medusa/Medusa_fwd.hpp>

using namespace mm;
using namespace Eigen;

class DeviationEstimator {
    template < typename values_t>
    double get_deviation(const Range<int>& ind, const values_t& solution) {
        double mu = 0;
        for (int i : ind) {
            mu += solution[i];
        }
        mu /= ind.size();

        double var = 0;
        for (int i : ind) {
            var += (solution[i] - mu)*(solution[i] - mu);
        }
        return std::sqrt(var);
    }
  public:

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, 
                      const std::pair<VectorField<typename vec_t::scalar_t, vec_t::dim>, 
                                      VectorField<typename vec_t::scalar_t, vec_t::dim*(vec_t::dim+1)/2>>& sol) {
        auto stress = sol.second;
        int N = domain.size();
        VectorXd error = VectorXd::Zero(N);

        assert_msg(N == stress.rows(), "Solution size %d must match domain size %d.", stress.rows(), N);
        int d = stress.cols();
        
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < d; ++j) {
                error(i) += get_deviation(domain.support(i), stress.c(j));
            }
        }
        return error;
    }

    
    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, const VectorXd& sol) {
        int N = domain.size();
        assert_msg(N == sol.size(), "Solution size %d must match domain size %d.", sol.size(), N);

        VectorXd error = VectorXd::Zero(N);
        for (int i = 0; i < N; ++i) {
            error(i) += get_deviation(domain.support(i), sol);
        }
        return error;
    }
};


class NormalizedDeviationEstimator {
    template < typename values_t>
    double get_deviation(const Range<int>& ind, const values_t& solution) {
        double mu = 0;
        for (int i : ind) {
            mu += solution[i];
        }
        mu /= ind.size();

        double var = 0;
        for (int i : ind) {
            var += (solution[i] - mu)*(solution[i] - mu);
        }
        return std::sqrt(var/ind.size());
    }
  public:

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, const VectorXd& sol) {
        int N = domain.size();
        assert_msg(N == sol.size(), "Solution size %d must match domain size %d.", sol.size(), N);

        VectorXd error = VectorXd::Zero(N);
        for (int i = 0; i < N; ++i) {
            error(i) += get_deviation(domain.support(i), sol);
        }
        return error;
    }
};

class MinMaxEstimator {
    template <typename values_t>
    double min_max(const Range<int>& ind, const values_t& solution) {
        return solution(ind).maxCoeff() - solution(ind).minCoeff();
    }
    
public:
    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, const VectorXd& sol) {
        int N = domain.size();
        assert_msg(N == sol.size(), "Solution size %d must match domain size %d.", sol.size(), N);

        VectorXd error = VectorXd::Zero(N);
        for (int i = 0; i < N; ++i) {
            error(i) = min_max(domain.support(i), sol);
        }
        return error;
    }
};


template <typename func_t>
class AnalyticalL1Estimator {
    func_t anal;
  public:
    AnalyticalL1Estimator(func_t anal) : anal(anal) {}

    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, const VectorXd& sol) {
        int N = domain.size();
        assert_msg(N == sol.size(), "Solution size %d must match domain size %d.", sol.size(), N);

        VectorXd error = VectorXd::Zero(N);
        for (int i = 0; i < N; ++i) {
            error(i) = std::abs(anal(domain.pos(i)) - sol(i)) * domain.dr(i) * N;
        }
        return error;
    }
};

template <typename func_t>
class AnalyticalL1LogEstimator {
    func_t anal;
  public:
    AnalyticalL1LogEstimator(func_t anal) : anal(anal) {}

    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, const VectorXd& sol) {
        int N = domain.size();
        assert_msg(N == sol.size(), "Solution size %d must match domain size %d.", sol.size(), N);

        VectorXd error = VectorXd::Zero(N);
        for (int i = 0; i < N; ++i) {
            error(i) = std::abs(anal(domain.pos(i)) - sol(i)) * domain.dr(i) * N;
            error(i) = std::max(10 + std::log10(error(i)), 1.0);
        }
        return error;
    }
};


#endif  // ADAPTIVE_ESTIMATORS_HPP
