#include "lshape_case.hpp"
#include "estimators.hpp"
#include "adaptive_solver.hpp"
#include <medusa/Medusa.hpp>

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif


int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    LshapeSolver solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    BoxShape<Vec2d> box1(-1, 1);
    BoxShape<Vec2d> box2(0, {2, -2});
    auto shape = box1 - box2;

    Timer t;
    t.addCheckPoint("begin");

    DeviationEstimator estimator;
//     MinMaxEstimator estimator;
    
//     AnalyticalL1LogEstimator estimator(LshapeSolver::analytical);

    double h0 = conf.get<double>("num.h0");
    double hr = conf.get<double>("num.hr");
    double hd = conf.get<double>("num.hd");
    auto h = [&](const Vec2d& v) { return h0; };
    auto limit_d = [&](const Vec2d& v) { return hd; };
    auto limit_r = [&](const Vec2d& v) { return hr; };

    vector<string> alphas = split(conf.get<string>("num.alphas_r"), ",");
    int n = alphas.size();
    for (int i = 0; i < n; ++i) {
        double a = stod(alphas[i]);
        cout << format("######################## %d / %d (%f) ######################", i+1, n, a)  << endl;
        conf.set("adapt.alpha_r", a, true);
        file.setGroupName(format("/alpha%03d", i));
        
        Timer t2; t2.addCheckPoint("start");
        adaptive_solve(conf, shape, solver, estimator, file, h, limit_d, limit_r);
        t2.addCheckPoint("end");
        file.atomic().writeDoubleAttribute("time", t2.duration("start", "end"));
    }

    t.addCheckPoint("end");
    file.setGroupName("/");
    file.atomic().writeDoubleAttribute("total-time", t.duration("begin", "end"));
    prn(t.duration("begin", "end"));


    return 0;
}
