#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include "adaptive_solver.hpp"

using namespace std;
using namespace Eigen;
using namespace mm;

double f(double x) {
    return 3*(1-x)*(1-x)*std::exp(-x*x) + 3*std::exp(-4*(x-1)*(x-1));
}

template <typename B>
void eval_dense(int iter, const DomainDiscretization<Vec1d>& d, B& wls, int n,
                const ScalarFieldd& fx_correct,
                const DomainDiscretization<Vec1d>& d_dense, HDF& file) {
    int N_dense = d_dense.size();

    ScalarFieldd fx_correct_dense(N_dense);
    for (int i = 0; i < N_dense; ++i) {
        fx_correct_dense[i] = f(d_dense.pos(i)[0]);
    }
    KDTree<Vec1d> tree(d.positions());
    ScalarFieldd fx_approx_dense(N_dense);
    for (int i = 0; i < N_dense; ++i) {
        indexes_t idx = tree.query(d_dense.pos(i), n).first;
        wls.compute(d_dense.pos(i), d.positions()[idx].asRange());
        fx_approx_dense[i] = wls.getShape().dot(fx_correct(idx));
    }
    file.atomic().writeDoubleArray("correct_dense", fx_correct_dense);
    file.atomic().writeDoubleArray("approx_dense", fx_approx_dense);
}

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    double a = conf.get<double>("num.a");
    double b = conf.get<double>("num.b");
    BoxShape<Vec1d> box(a, b);

    double dx_dense = conf.get<double>("show.dx");
    int n = conf.get<int>("num.n");
    DomainDiscretization<Vec1d> d_dense = box.discretizeWithStep(dx_dense);
    d_dense.findSupport(FindClosest(n));
    file.atomic().writeDouble2DArray("pos_dense", d_dense.positions());

    int max_iter = conf.get<int>("adapt.maxiter");
    double dx = conf.get<double>("num.dx");
    double dxd = conf.get<double>("num.dxd");
    double dxr = conf.get<double>("num.dxr");
    double eps_r = conf.get<double>("adapt.eps_r");
    double eps_d = conf.get<double>("adapt.eps_d");
    double alpha_r = conf.get<double>("adapt.alpha_r");
    double alpha_d = conf.get<double>("adapt.alpha_d");
    double global_error_threshold = conf.get<double>("adapt.tol");
    int nn = conf.get<int>("adapt.scattered_interpolant_neighbours");

    std::function<double(Vec1d)> h = [=](const Vec1d& p) { return dx * (1 + 150 * std::abs(3 + p[0]) / 6); };
    std::function<double(Vec1d)> limit_d = [=](const Vec1d&) { return dxd; };
    std::function<double(Vec1d)> limit_r = [=](const Vec1d&) { return dxr; };

    GeneralFill<Vec1d> fill; fill.seed(1337);
    ScatteredInterpolant<Vec1d, double> interpolant(d_dense.positions(), nn);

    for (int iter = 0; iter < max_iter; ++iter) {
        prn(iter);
        file.setGroupName(format("iter%02d", iter));

        DomainDiscretization<Vec1d> d = box.discretizeBoundaryWithDensity(h);
        fill(d, h);
        int N = d.size();
        prn(N);
        d.findSupport(FindClosest(n));
        file.atomic().writeDouble2DArray("pos", d.positions());

        WLS<Monomials<Vec1d>, GaussianWeight<Vec1d>, ScaleToFarthest> wls(1, 1);
        ScalarFieldd fx_correct(N);
        for (int i = 0; i < N; ++i) {
            fx_correct[i] = f(d.pos(i)[0]);
        }
        ScalarFieldd fx_approx(N);
        for (int i = 0; i < N; ++i) {
            wls.compute(d.pos(i), d.supportNodes(i));
            fx_approx[i] = wls.getShape().dot(fx_correct(d.support(i)));
        }
        file.atomic().writeEigen("correct", fx_correct);
        file.atomic().writeEigen("approx", fx_approx);

        eval_dense(0, d, wls, n, fx_correct, d_dense, file);

        ScalarFieldd old_dx(N);
        ScalarFieldd error = (fx_approx - fx_correct).cwiseAbs();
        for (int i = 0; i < N; ++i) {
            old_dx(i) = h(d.pos(i));
            error(i) *= old_dx(i)*N;
        }
        file.atomic().writeDoubleArray("error", error);

        ScalarFieldd new_dx = adapt_dx(d, error, old_dx, eps_r, eps_d, alpha_r, alpha_d, limit_r,
                                       limit_d, file);
        file.atomic().writeEigen("new_dx", new_dx);
        file.atomic().writeEigen("factor", old_dx.cwiseQuotient(new_dx));

        prn(new_dx.minCoeff())
        prn(new_dx.maxCoeff())
        prn(error.mean())
        if (error.mean() < global_error_threshold) break;

        interpolant.setPositions(d.positions());
        interpolant.setValues(new_dx);
        h = std::ref(interpolant);
    }

    return 0;
}
