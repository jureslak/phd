#include <vector>
#include <queue>
#include <list>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <cmath>
#include <chrono>
#include <medusa/Medusa.hpp>
#include <medusa/bits/domains/GrainDropFill.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <medusa/bits/domains/GeneralSurfaceFill.hpp>
#include <Eigen/Core>
#include <medusa/bits/spatial_search/KDGrid.hpp>
#include "fill/demo_shapes.hpp"
#include "Poisson.hpp"

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif


using namespace std;
using namespace Eigen;
using namespace mm;


template <typename vec_t, typename func_t, typename sfill_t, typename fill_t>
void test_run(const XML& conf, HDF& file, const DemoShape<vec_t>& shape, sfill_t& sfill, fill_t& fill,  const func_t& delta_r,  int runs,
              int i, Eigen::MatrixXd& Ns, Eigen::MatrixXd& times, Eigen::MatrixXd& err1, Eigen::MatrixXd& err2, Eigen::MatrixXd& errinf) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;

    for (int j = 0; j < runs; ++j) {
//        prn(j)
        sfill.seed(get_seed());
        fill.seed(get_seed());
        DomainDiscretization<vec_t> d(shape);

        BoxShape<pvec_t> par_sh(0, 2 * PI);
        if (pdim == 2) {
            par_sh = BoxShape<pvec_t>({0, 0}, {PI, 2*PI});
        }
        DomainDiscretization<pvec_t> param_domain(par_sh);


        Timer t;
        t.addCheckPoint("start");

        sfill(d, param_domain, shape.template map<pdim>, shape.template dmap<pdim>, delta_r, -1);
        fill(d, delta_r, 1);

        int N = d.size();
        prn(N);
        Ns(i, j) = N;

        d.findSupport(FindClosest(conf.get<int>("approx.n")));

        VectorXd num_sol = PoissonDir::solve<vec_t>(d, conf, file, t);
        t.addCheckPoint("end");
        VectorXd ex_sol(N);
        for (int i = 0; i < N; ++i) {
            ex_sol[i] = PoissonDir::analytical(d.pos(i));
        }
        VectorXd err = num_sol - ex_sol;

        err1(i, j) = err.lpNorm<1>() / ex_sol.lpNorm<1>();
        err2(i, j) = err.lpNorm<2>() / ex_sol.lpNorm<2>();
        errinf(i, j) = err.lpNorm<Eigen::Infinity>() / ex_sol.lpNorm<Eigen::Infinity>();
        times(i, j) = t.duration("start", "end");
    }
}

template <typename vec_t>
void run_all(const XML& conf, HDF& hdf) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;

    DemoShape<vec_t> sh;

    GeneralSurfaceFill<vec_t, pvec_t> sfill; sfill.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));

    int n_runs = conf.get<int>("num.nruns");
    vector<string> ns = split(conf.get<std::string>("num.ns"), ',');

    Eigen::MatrixXd NS(ns.size(), n_runs);
    Eigen::MatrixXd TIMES(ns.size(), n_runs);
    Eigen::MatrixXd ERR1(ns.size(), n_runs);
    Eigen::MatrixXd ERR2(ns.size(), n_runs);
    Eigen::MatrixXd ERRI(ns.size(), n_runs);

    for (size_t i = 0; i < ns.size(); ++i) {

        int n = std::stoi(ns[i]);
        prn("case", i);
        prn("nodes per side", n);

        auto fn = [=](const vec_t&) { return 1.0 / n; };
        test_run(conf, hdf, sh, sfill, fill_randomized, fn, n_runs, i, NS, TIMES, ERR1, ERR2, ERRI);
    }

    hdf.reopen();
    hdf.openGroup("/");
    hdf.writeEigen("Ns", NS);
    hdf.writeEigen("err1", ERR1);
    hdf.writeEigen("err2", ERR2);
    hdf.writeEigen("errinf", ERRI);
    hdf.writeEigen("times", TIMES);
}

int main(int argc, const char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the parameter file.");
    XML conf(argv[1]);

    std::string out_file = conf.get<std::string>("meta.out_file");
    HDF hdf(out_file, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    int dim = conf.get<int>("case.dim");
    assert_msg(dim > 0, "Dimension must be positive, got %d.", dim);

    switch (dim) {
//        case 1: run_all<Vec1d>(conf, hdf); break;
        case 2: run_all<Vec2d>(conf, hdf); break;
        case 3: run_all<Vec3d>(conf, hdf); break;
//        case 4: run_all<Vec<double, 4>>(conf, hdf); break;
//        case 5: run_all<Vec<double, 5>>(conf, hdf); break;
        default: assert_msg(false, "Dimension %d not supported.", dim);
    }


    return 0;
}
