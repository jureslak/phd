#include <medusa/Medusa.hpp>
#include "Eigen/SparseCore"
#include "Eigen/IterativeLinearSolvers"

#ifdef NDEBUG
    #warning "This is a release build!"
#else
    #warning "This is a debug build!"
#endif


using namespace mm;
using namespace std;
using namespace Eigen;

typedef Vec2d vec;

void solve(int n) {
    clock_t cl1 = clock();


    double h = 1.0 / n;
    double r1 = 1.0;
    double r2 = r1/2;
    auto sh = BallShape<vec>(0.0, r1) - BallShape<vec>(0.0, r2);
    DomainDiscretization<vec> domain = sh.discretizeBoundaryWithStep(h);
    GeneralFill<vec> fill; fill.seed(0).numSamples(10);
    KDGrid<vec> grid(-r1, r1, h/2);
    fill(domain, [=](const vec&) { return h; }, grid);
    double mesher = (clock() - cl1) / (double) CLOCKS_PER_SEC;

    domain.findSupport(FindClosest(9));
    int aug = 2;
    RBFFD<Polyharmonic<double, 3>, vec, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> approx({}, aug);

    int N = domain.size();
    auto storage = domain.computeShapes<sh::lap>(approx, domain.interior());
    SparseMatrix<double, RowMajor> M(N, N);
    M.reserve(storage.supportSizes());
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);
    auto op = storage.implicitOperators(M, rhs);
    for (int i : domain.interior()) {
        double f = -2*PI * PI * (PI * domain.pos(i).array()).sin().prod();
        op.lap(i) = f;
    }
    for (int i : domain.boundary()) {
        op.value(i) = (PI * domain.pos(i).array()).sin().prod();
    }

    BiCGSTAB<decltype(M), IncompleteLUT<double>> solver;
    solver.setTolerance(1e-6);
    solver.setMaxIterations(100);
    solver.preconditioner().setDroptol(1e-2);
    solver.preconditioner().setFillfactor(5);

//    SparseLU<SparseMatrix<double>> solver;

    solver.compute(M);
    VectorXd sol = solver.solve(rhs);
    VectorXd err = sol;
    for (int i = 0; i < N; ++i) {
        err[i] -= (PI * domain.pos(i).array()).sin().prod();
    }

    double s = (clock() - cl1) / (double) CLOCKS_PER_SEC;
    cout << N << endl;
    cout << err.lpNorm<Infinity>() << endl;
    cout << s << endl;
    cout << mesher << endl;
}

int main(int argc, char* argv[]) {

    int n = stoi(argv[1]);
    solve(n);

    return 0;
}
