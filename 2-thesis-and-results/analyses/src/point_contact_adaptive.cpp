#include "point_contact_case.hpp"
#include "estimators.hpp"
#include "adaptive_solver.hpp"

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif


int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    PointContactSolver<Vec3d> solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    double a = conf.get<double>("num.a");
    double eps = conf.get<double>("num.eps");
    BoxShape<Vec3d> box(-a, -eps);


    Timer t;
    t.addCheckPoint("begin");


    DeviationEstimator estimator;

    double h0 = conf.get<double>("num.h0");
    double hr = conf.get<double>("num.hr");
    double hd = conf.get<double>("num.hd");
    auto h = [&](const Vec3d& v) { return h0; };
    auto limit_d = [&](const Vec3d& v) { return hd; };
    auto limit_r = [&](const Vec3d& v) { return hr; };

    adaptive_solve(conf, box, solver, estimator, file, h, limit_d, limit_r);

    t.addCheckPoint("end");
    file.setGroupName("/");
    file.atomic().writeDoubleAttribute("total-time", t.duration("begin", "end"));
    prn(t.duration("begin", "end"));


    return 0;
}
