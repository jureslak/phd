#include "lshape_case.hpp"
#include <medusa/Medusa.hpp>

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    LshapeSolver solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    BoxShape<Vec2d> box1(-1, 1);
    BoxShape<Vec2d> box2(0, {2, -2});
    auto shape = box1 - box2;

    vector<string> ns = split(conf.get<string>("num.nxs"), ",");
    int iter = 0;
    for (const string& s : ns) {
        int nx = stoi(s);
        cout << "-------- " << nx << " -----------\n";
        file.setGroupName(format("%02d", iter));

        Timer timer;
        timer.addCheckPoint("domain");

        double dx = 1.0/nx;
        GeneralFill<Vec2d> fill; fill.seed(conf.get<int>("num.seed"));
        auto h = [=](const Vec2d&) { return dx; };
//         DomainDiscretization<Vec2d> d = shape.discretizeWithDensity(, fill);

        auto d = shape.discretizeBoundaryWithDensity(h);
        
        // prefill with corase seed nodes
        fill(d, [&](const Vec2d& p) { return 10*h(p); });
        fill(d, h);
        
        solver.solve(d, conf, file, timer, h);

        file.atomic().writeDomain("domain", d);


        ++iter;
    }

    return 0;
}
