#include <vector>
#include <queue>
#include <list>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <cmath>
#include <chrono>
#include <medusa/Medusa.hpp>
#include <medusa/bits/domains/GrainDropFill.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <medusa/bits/domains/GeneralSurfaceFill.hpp>
#include <Eigen/Core>
#include <medusa/bits/spatial_search/KDGrid.hpp>
#include "fill/demo_shapes.hpp"
#include "Poisson.hpp"

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif


using namespace std;
using namespace Eigen;
using namespace mm;


template <typename vec_t, typename sfill_t, typename fill_t>
void test_run(XML& conf, HDF& hdf, const DemoShape<vec_t>& shape, sfill_t& sfill, fill_t& fill,
             int j, int ss, int aug, Eigen::MatrixXd& Ns, Eigen::MatrixXd& times, Eigen::MatrixXd& err1, Eigen::MatrixXd& err2, Eigen::MatrixXd& errinf) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;
    vector<string> ns = split(conf.get<std::string>("num.ns"), ',');
    
    for (int i = 0; i < ns.size(); ++i) {
        prn(i)
        
        int n = std::stoi(ns[i]);
        sfill.seed(get_seed());
        fill.seed(get_seed());
        DomainDiscretization<vec_t> d(shape);

        BoxShape<pvec_t> par_sh(0, 2 * PI);
        if (pdim == 2) {
            par_sh = BoxShape<pvec_t>({0, 0}, {PI, 2*PI});
        }
        DomainDiscretization<pvec_t> param_domain(par_sh);

        Timer t;
        t.addCheckPoint("start");

        auto fn = [=](const vec_t&) { return 1.0 / n; };
        sfill(d, param_domain, shape.template map<pdim>, shape.template dmap<pdim>, fn, -1);
        fill(d, fn, 1);

        int N = d.size();
        prn(N);
        Ns(i, j) = N;
        
        d.findSupport(FindClosest(ss));

        conf.set("approx.aug", aug, true);
        VectorXd num_sol = PoissonDir::solve<vec_t>(d, conf, hdf, t);
        t.addCheckPoint("end");
        VectorXd ex_sol(N);
        for (int i = 0; i < N; ++i) {
            ex_sol[i] = PoissonDir::analytical(d.pos(i));
        }
        VectorXd err = num_sol - ex_sol;

        err1(i, j) = err.lpNorm<1>() / ex_sol.lpNorm<1>();
        err2(i, j) = err.lpNorm<2>() / ex_sol.lpNorm<2>();
        errinf(i, j) = err.lpNorm<Eigen::Infinity>() / ex_sol.lpNorm<Eigen::Infinity>();
        times(i, j) = t.duration("start", "end");
        
        hdf.reopen();
        hdf.openGroup("/");
        hdf.writeEigen("Ns", Ns, true);
        hdf.writeEigen("err1", err1, true);
        hdf.writeEigen("err2", err2, true);
        hdf.writeEigen("errinf", errinf, true);
        hdf.writeEigen("times", times, true);
        hdf.close();
    }
}

template <typename vec_t>
void run_all(XML& conf, HDF& hdf) {
    constexpr int pdim = vec_t::dim - 1;
    typedef Vec<double, pdim> pvec_t;

    DemoShape<vec_t> sh;

    GeneralSurfaceFill<vec_t, pvec_t> sfill; sfill.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));
    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf.get<int>("num.k")).seed(conf.get<int>("num.seed"));

    vector<string> ns = split(conf.get<std::string>("num.ns"), ',');
    vector<string> sss = split(conf.get<string>("approx.ns"), ",");
    int n_runs = sss.size();
    
    Eigen::MatrixXd NS(ns.size(), n_runs);
    Eigen::MatrixXd TIMES(ns.size(), n_runs);
    Eigen::MatrixXd ERR1(ns.size(), n_runs);
    Eigen::MatrixXd ERR2(ns.size(), n_runs);
    Eigen::MatrixXd ERRI(ns.size(), n_runs);

    vector<string> augs = split(conf.get<string>("approx.augs"), ",");
    assert_msg(sss.size() == augs.size(), "Ns and augs must be of same size.");
  
      
    for (int j = 0; j < n_runs; ++j) {
        prn(j);

        test_run(conf, hdf, sh, sfill, fill_randomized, j, stoi(sss[j]), stoi(augs[j]), NS, TIMES, ERR1, ERR2, ERRI);
    
    }
}

int main(int argc, const char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the parameter file.");
    XML conf(argv[1]);

    std::string out_file = conf.get<std::string>("meta.out_file");
    HDF hdf(out_file, HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    int dim = conf.get<int>("case.dim");
    assert_msg(dim > 0, "Dimension must be positive, got %d.", dim);

    switch (dim) {
//        case 1: run_all<Vec1d>(conf, hdf); break;
        case 2: run_all<Vec2d>(conf, hdf); break;
        case 3: run_all<Vec3d>(conf, hdf); break;
//        case 4: run_all<Vec<double, 4>>(conf, hdf); break;
//        case 5: run_all<Vec<double, 5>>(conf, hdf); break;
        default: assert_msg(false, "Dimension %d not supported.", dim);
    }


    return 0;
}
