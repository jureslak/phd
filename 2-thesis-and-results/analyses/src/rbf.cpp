#include <medusa/Medusa.hpp>
#include <iostream>

using namespace mm;
using namespace std;
using namespace Eigen;

int main() {

    vector<vector<double>> pts_data = CSV::read2d("plotdata/sample_data_2d.txt", ' ');
    VectorXd val = CSV::readEigen("plotdata/sample_values_2d.txt");
    
    int n = pts_data.size();
    Range<Vec2d> pts(n);
    for (int i = 0; i < n; ++i) {
        pts[i] = {pts_data[i][0], pts_data[i][1]};
    }
        
    Vec2d c = {0.5, 0.5};
    double R = 0.25;
    double sigma = 0.25;
    RBFFD<Gaussian<double>, Vec2d, NoScale> grbf(sigma);
    RBFFD<InverseMultiquadric<double>, Vec2d, NoScale> irbf(sigma);
    RBFFD<Multiquadric<double>, Vec2d, NoScale> mrbf(sigma, 0);
    RBFFD<Polyharmonic<double, 3>, Vec2d, NoScale> prbf({}, 1);
    
    auto gappr = grbf.getApproximant(c, pts, val);
    auto iappr = irbf.getApproximant(c, pts, val);
    auto mappr = mrbf.getApproximant(c, pts, val);
    auto pappr = prbf.getApproximant(c, pts, val);
    
    int nn = 50;
    MatrixXd V(nn, nn), V2(nn, nn), X(nn, nn), Y(nn, nn), V3(nn, nn), V4(nn, nn);
    Range<double> x = linspace(0., 1., nn);
    Range<double> y = linspace(0., 1., nn);
    for (int i = 0; i < nn; ++i) {
        for (int j = 0; j < nn; ++j) {
            Vec2d p = {x[i], y[j]};
            X(i, j) = x[i];
            Y(i, j) = y[j];
          
            V(i, j) = gappr(p);
            V2(i, j) = iappr(p);
            V3(i, j) = mappr(p);
            V4(i, j) = pappr(p);
        }
    }
    
    HDF hdf("plotdata/rbf.h5", HDF::DESTROY);
    hdf.writeEigen("gau", V);
    hdf.writeEigen("imq", V2);
    hdf.writeEigen("mq", V3);
    hdf.writeEigen("ph", V4);
//     hdf.writeEigen("X", X);
//     hdf.writeEigen("Y", Y);
    hdf.writeDoubleArray("x", x);
    hdf.writeDoubleArray("y", y);


    return 0;
}
