#ifndef ADAPTIVE_ADAPTIVE_SOLVER_HPP
#define ADAPTIVE_ADAPTIVE_SOLVER_HPP

#include <iostream>
#include <vector>
#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseLU>

using namespace std;
using namespace mm;
using namespace Eigen;

/**
 * Scattered node interpolant averaging over small neighbourhood.
 * Implements modified Sheppard's method.
 */
template <class vec_t, class value_t>
class ScatteredInterpolant {
    KDTree<vec_t> tree;  ///< Tree of all points.
    Range<value_t> values;  ///< Function values at given points.
    int num_closest;  ///< Number of closest points to include in calculations.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar data type.
  public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param values Function values at given positions.
     * @param num_closest How many neighbours to take into account when calculating function value.
     */
    ScatteredInterpolant(const Range<vec_t>& pos, int num_closest)
            : tree(pos), num_closest(num_closest) {}

    ScatteredInterpolant(int num_closest)
            : num_closest(num_closest) {}

    template <typename values_t>
    void setValues(const values_t& new_values) { values = Range<value_t>(new_values.begin(), new_values.end()); }

    void setPositions(const Range<vec_t>& pos) { tree.reset(pos); }

    /// Evaluate the distribution function.
    value_t operator()(const vec_t& point) const {
        auto [idx, d2] = tree.query(point, num_closest);
        if (d2[0] < 1e-6) return values[idx[0]];
        int n = idx.size();
        scalar_t s = 0.0, r = 0.0;
        Range<scalar_t> id2(n);
        for (int i = 0; i < n; ++i) { id2[i] = 1.0/d2[i]; s += id2[i]; }
        for (int i = 0; i < n; ++i) { id2[i] /= s; }
        for (int i = 0; i < n; ++i) { r += id2[i]*values[idx[i]]; }
        return r;
    }
};

template <typename vec_t, typename func_t, typename func_t2>
ScalarFieldd adapt_dx(
        const DomainDiscretization<vec_t>& d, const ScalarFieldd& error, const ScalarFieldd& old_dx,
        double eps_r, double eps_d, double alpha_r, double alpha_d, func_t r_limit, func_t2 d_limit,
        HDF& file) {
    int ref = 0, deref = 0, same = 0, ref_limit = 0, deref_limit = 0;
    double min_e = error.minCoeff();
    double max_e = error.maxCoeff();
    int N = d.size();
    assert_msg(N == error.size(), "Domain size %d must match error size %d.", N, error.size());
    ScalarFieldd new_dx(N);
    for (int i = 0; i < N; ++i) {
        if (error(i) >= eps_r) {  // refine
            double badness = (error(i) - eps_r) / (max_e - eps_r);  // in [0, 1]
            new_dx[i] = old_dx[i] / (badness * (alpha_r - 1) + 1);
            double lim = r_limit(d.pos(i));
            if (new_dx[i] > old_dx[i]) { prn("Refine is wrong."); exit(1); }
            if (new_dx[i] < lim) {
                new_dx[i] = lim; ref_limit++;
            } else { ref++; }
        } else if (error(i) <= eps_d) {  // derefine
            double badness = (eps_d - error(i)) / (eps_d - min_e);
            new_dx[i] = old_dx[i] / (1+badness*(1.0/alpha_d - 1));
            if (new_dx[i] < old_dx[i]) { prn("Derefine is wrong."); exit(1); }
            double lim = d_limit(d.pos(i));
            if (new_dx[i] > lim) {
                new_dx[i] = lim;  deref_limit++;
            } else { deref++; }
        } else {
            new_dx[i] = old_dx[i];
            same++;
        }
    }

    std::cerr << ref << " nodes refined, "
              << ref_limit << " hit refine limit, "
              << deref << " derefined, "
              << deref_limit << " hit derefine limit, "
              << same << " left same.\n";

    file.atomic().writeIntAttribute("ref", ref);
    file.atomic().writeIntAttribute("deref", deref);
    file.atomic().writeIntAttribute("same", same);
    file.atomic().writeIntAttribute("deref_limit", deref_limit);
    file.atomic().writeIntAttribute("ref_limit", ref_limit);

    return new_dx;
}

void save_solution(HDF& out_file, const VectorXd& solution) {
    out_file.atomic().writeEigen("sol", solution);
}
void save_solution(HDF& out_file, const Range<array<double, 3>>& solution) {
    out_file.atomic().writeDouble2DArray("sol", solution);
}
void save_solution(HDF& out_file, const pair<VectorField2d, VectorField<double, 3>>& solution) {
    out_file.atomic().writeEigen("stress", solution.second);
    out_file.atomic().writeEigen("displ", solution.first);
}
void save_solution(HDF& out_file, const pair<VectorField3d, VectorField<double, 6>>& solution) {
    out_file.atomic().writeEigen("stress", solution.second);
    out_file.atomic().writeEigen("displ", solution.first);
}

template <typename vec_t, typename Estimator, typename Solver, typename Func1, typename Func2, typename Func3>
void adaptive_solve(const XML& conf, const DomainShape<vec_t>& shape, Solver& solver, Estimator& e,
                    HDF& out_file, const Func1& h0, const Func2& limit_d, const Func3& limit_r) {

    int max_iter = conf.get<int>("adapt.maxiter");
    int force_iter = (conf.exists("adapt.forceiter")) ? conf.get<int>("adapt.forceiter") : 0;
    double eps_r = conf.get<double>("adapt.eps_r");
    double eps_d = conf.get<double>("adapt.eps_d");
    double alpha_r = conf.get<double>("adapt.alpha_r");
    double alpha_d = conf.get<double>("adapt.alpha_d");
    double global_error_tolerance = conf.get<double>("adapt.tol");
    int nn = conf.get<int>("adapt.scattered_interpolant_neighbours");

    ScatteredInterpolant<vec_t, double> interpolant(nn);
    std::function<double(vec_t)> h = h0;

    int seed = conf.get<int>("num.seed");
    GeneralFill<vec_t> fill; fill.seed(seed);

    string group_name = out_file.groupName();
    for (int iter = 0; iter < max_iter; ++iter) {
        cerr << format("------------------- iter: %d -----------------------", iter) << endl;
        Timer t;
        t.addCheckPoint("start");
        out_file.setGroupName(format("%s/%03d", (group_name == "/") ? "" : group_name, iter));
        
        auto domain = shape.discretizeBoundaryWithDensity(h);
        
        // prefill with corase seed nodes
        fill(domain, [&](const vec_t& p) { return 10*h(p); });
        fill(domain, h);
        

//         int support_size = conf.get<int>("approx.n");
//         domain.findSupport(FindClosest(support_size));

       if (domain.size() > 1e6) {
           out_file.atomic().writeIntAttribute("converged",  0);
           break;
       }
        prn("N", domain.size());        

        int N = domain.size();
        out_file.atomic().writeIntAttribute("N", N);
        out_file.atomic().writeDomain("domain", domain);
        
        auto solution = solver.solve(domain, conf, out_file, t, h);
       
        VectorXd error = e.estimate(domain, solution);
       
        out_file.atomic().writeEigen("error_indicator", error);
        std::cerr << "min: " << error.minCoeff() << ", avg: " << error.mean() << ", max: " << error.maxCoeff() << std::endl;
//         error.maxCoeff());
        if (iter > force_iter && error.mean() < global_error_tolerance) {
            cout << "Converged after " << iter << " iterations." << endl;
            t.addCheckPoint("end");
            prn(t.duration("start", "end"));
            return;
        }

        ScalarFieldd old_dx(N);
        for (int i = 0; i < N; ++i) {
            old_dx(i) = h(domain.pos(i));
        }
        ScalarFieldd new_dx = adapt_dx(domain, error, old_dx, eps_r, eps_d, alpha_r, alpha_d, limit_r,
                                       limit_d, out_file);
        out_file.atomic().writeEigen("dx", new_dx);

        interpolant.setPositions(domain.positions());
        interpolant.setValues(new_dx);
        h = std::ref(interpolant);
        t.addCheckPoint("end");
        prn(t.duration("start", "end"));
    }
    
    out_file.setGroupName(group_name);

    cout << "Stopped after " << max_iter << " iterations." << endl;
}

#endif  // ADAPTIVE_ADAPTIVE_SOLVER_HPP
