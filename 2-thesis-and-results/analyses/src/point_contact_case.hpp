#ifndef ADAPTIVE3D_POINT_CONTACT_CASE_HPP
#define ADAPTIVE3D_POINT_CONTACT_CASE_HPP

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace std;
using namespace Eigen;


template <typename vec_t>
struct PointContactSolver {
    typedef typename vec_t::scalar_t scalar_t;
    enum { dim = vec_t::dim };
    typedef std::pair<VectorField<scalar_t, dim>, VectorField<scalar_t, dim*(dim+1)/2>> solution_t;

    static Vec3d analytical(const Vec3d& p, double P, double E, double nu) {
        double x = p[0], y = p[1], z = p[2];
        double r = std::sqrt(x*x+y*y);
        double c = x/r, s = y/r;
        double R = p.norm();
        double mu = E / 2 / (1+nu);
        double u = P*r/4/PI/mu * (z/R/R/R - (1-2*nu) / (R*(R+z)));
        double w = P/4/PI/mu * (z*z/R/R/R + 2*(1-nu)/R);
        return {u*c, u*s, w};
    }

    static void preprocess(XML& conf) {
        double E = conf.get<double>("case.E");
        double nu = conf.get<double>("case.nu");

        // parameter logic
        double mu = E / 2. / (1+nu);
        double lam = E * nu / (1-2*nu) / (1+nu);
        conf.set("case.mu", mu);
        conf.set("case.lam", lam);
    }

    template<typename func_t>
    solution_t solve(DomainDiscretization<vec_t>& domain, const XML& conf, HDF& file, Timer& timer, const func_t& h) {
        int basis_size = conf.get<int>("approx.m");
        double sigmaB = conf.get<double>("approx.sigmaB");
        double sigmaW = conf.get<double>("approx.sigmaW");
        string basis = conf.get<string>("approx.basis_type");

        if (basis == "gau") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "mq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Multiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "phs") {
            int k = conf.get<int>("approx.k");
            int aug = conf.get<int>("approx.aug");
            RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(k, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "imq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<InverseMultiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine, h);
        } else if (basis == "mon") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>(m), sigmaW);
            return solve_(conf, domain, file, timer, approx, h);
        } else if (basis == "mont") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>::tensorBasis(m), sigmaW);
            return solve_(conf, domain, file, timer, approx, h);
        }
        assert_msg(false, "Unknown basis type '%s'.", basis);
        throw "";
    }

    template<typename approx_t, typename func_t>
    solution_t solve_(const XML& conf, DomainDiscretization<vec_t>& d, HDF& file, Timer& timer,
                      const approx_t& approx, const func_t& h) {
        int N = d.size();
        prn(N);

        const double P = conf.get<double>("case.P");
        const double E = conf.get<double>("case.E");
        const double nu = conf.get<double>("case.nu");
        const double lam = conf.get<double>("case.lam");
        const double mu = conf.get<double>("case.mu");

        const int ss = conf.get<int>("approx.n");
        d.findSupport(FindClosest(ss));


        timer.addCheckPoint("shapes");
        prn("shapes");

        auto storage = d.computeShapes(approx);

        timer.addCheckPoint("matrix");
        prn("matrix");

        const int dim = vec_t::dim;
        SparseMatrix<double, RowMajor> M(dim*N, dim*N);
        M.reserve(Range<int>(dim*N, ss*dim));
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(dim*N);

        auto op = storage.implicitVectorOperators(M, rhs);

        for (int i : d.interior()) {
            (lam+mu)*op.graddiv(i) + mu*op.lap(i) = 0;
        }
        for (int i : d.boundary()) {
            op.value(i) = analytical(d.pos(i), P, E, nu);
        }

//        file.atomic().writeSparseMatrix("M", M);
//        file.atomic().writeDoubleArray("rhs", rhs);

        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();
        timer.addCheckPoint("compute");
        prn("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        prn("solve");

        VectorXd sol = solver.solve(rhs);
        timer.addCheckPoint("postprocess");
        VectorField3d disp = VectorField3d::fromLinear(sol);
        VectorField<double, 6> stress(N);
        auto eop = storage.explicitVectorOperators();
        for (int i = 0; i < N; ++i) {
            auto grad = eop.grad(disp, i);
            Matrix3d eps = 0.5*(grad + grad.transpose());
            Matrix3d s = lam * eps.trace() * Matrix3d::Identity(3, 3) + 2*mu*eps;
            stress[i][0] = s(0, 0);
            stress[i][1] = s(1, 1);
            stress[i][2] = s(2, 2);
            stress[i][3] = s(0, 1);
            stress[i][4] = s(0, 2);
            stress[i][5] = s(1, 2);
        }
//         timer.addCheckPoint("end");

        file.atomic().writeEigen("displ", disp);
        file.atomic().writeEigen("stress", stress);

        return {disp,stress};
    }
};


#endif //ADAPTIVE3D_POINT_CONTACT_CASE_HPP
