prepare

run([plotdatapath 'sheppard_example_data_2d.m'])
n = length(pts);
x = pts(:, 1)';
y = pts(:, 2)';
pos = pts;

val = uex(x, y)';
v = val';
z = zeros(size(v));

m = 50;
[X, Y] = meshgrid(deal(linspace(0, 1, m)));
X = X(:);
Y = Y(:);

P = [X Y];

% V = rbffd_interp(pos', val', P', 'RBFFunction', 'cubic', 'Neighbours', 9);
tic
V = mls_interp(pos', val', P', 'WeightFunction', 'gaussian',...
               'WeightConstant', 100, 'Neighbours', 9);
toc

X = reshape(X, [m m]);
Y = reshape(Y, [m m]);
V = reshape(V, [m m]);

setfig('a2'); view(3);
scatter3(x, y, v, 15, 'ok', 'filled');
surf(X, Y, V, 'EdgeColor', 'none');
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
