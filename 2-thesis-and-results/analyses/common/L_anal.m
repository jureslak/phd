function v = L_anal(x, y, omega)

r = sqrt(x.^2 + y.^2);
phi = mod(atan2(y, x), 2*pi);
alpha = pi / omega;

v = r.^alpha .* sin(alpha .* phi);

end