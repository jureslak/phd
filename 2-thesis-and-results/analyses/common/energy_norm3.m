function [x, k] = energy_norm3(sxx, syy, szz, sxy, sxz, syz, lam, mu)
% assuming points are equidistant and norms will always be relative
% we can always factor out the dx^2 it gets cancelled away, making integration into summation

f = 2*mu*(3*lam + 2*mu);

k = sxy.^2/mu + sxz.^2/mu + syz.^2/mu + ...
    sxx.*(sxx/(2.*mu) - lam*(sxx + syy + szz)/f) + ...
    syy.*(syy/(2.*mu) - lam*(sxx + syy + szz)/f) + ...
    szz.*(szz/(2.*mu) - lam*(sxx + syy + szz)/f);

x = sqrt(sum(k(:)));
end

