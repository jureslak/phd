function x = l1_norm(M)
    x = sum(abs(M(:))) / numel(M);
end