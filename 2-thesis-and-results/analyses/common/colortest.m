prepare

setfig('b1');
for i = 1:7
    x = linspace(0, 3, 100);
    y = i*x + sin(x);
    
    plot(x, y);
end


setfig('b2');
C = linespecer(10);
for i = 1:10
    x = linspace(0, 3, 100);
    y = i*x + sin(x);
    
    plot(x, y, 'Color', C(i, :));
end

setfig('b4');
for i = 1:7
    x = linspace(0, 3, 100);
    y = i*x + sin(x);
    
    plot(x, y, 'Color', COLORS(i, :));
end

