function v = fichera_anal(x, y, z)

r = sqrt(x.^2 + y.^2 + z.^2);
v = sqrt(r);

end