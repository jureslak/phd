function x = energy_norm(sxx, syy, sxy, lam, mu)
% assuming points are equidistant and norms will always be relative
% we can always factor out the dx^2 it gets cancelled away, making integration into summation
f = 4*mu*(lam+mu);
e = (lam*(2*sxy.^2 + (sxx-syy).^2) + 2*mu*(sxx.^2+sxy.^2+syy.^2)) / f;
x = sqrt(sum(e(:)));
end
