function v = L_grad_anal(x, y, omega)

r = sqrt(x.^2 + y.^2);
phi = mod(atan2(y, x), 2*pi);
alpha = pi / omega;

if isrow(x)
    d = 1;
    x = x';
    y = y';
elseif iscolumn(x)
    d = 1;
else
    d = 2;
end

g = alpha*r.^(-2*alpha);
v = cat(d+1, g.*(x.*sin(alpha*phi) - y.*cos(alpha*phi)),...
             g.*(y.*sin(alpha*phi) + x.*cos(alpha*phi)));

end