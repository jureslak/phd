function [pts, prev] = PNP(box, r_target, n, nmax, inside, init, candidates)
% Fills box [xmin, ymin, xmax, ymax]
% with points spaced according to r_target with at most nmax points.
% Parameter inside is optional and represents the characteristic function 
% of the domain. Parameter init represents the initial seed nodes.
if nargin < 7, candidates = @(n) linspace(0, 2*pi, n+1); end
if nargin < 6, init = []; end
if nargin < 5, inside = @(x, y) 1; end
if nargin < 4, nmax = 1e4; end
if nargin < 3, n = 15; end

pts = zeros(nmax, 2);
prev = -1*ones(nmax, 1);
pts(1:length(init), :) = init;
types = zeros(nmax, 1);
num = length(init);

xmin = box(1); xmax = box(3);
ymin = box(2); ymax = box(4);

% x = xmin;
% y = ymin;
% f = (1+1e-10);
% while x < xmax-r_target(xmax, ymin)
%     num = num+1;
%     pts(num, :) = [x, y];
%     x = x + r_target(x, y)*f;
% end
% x = xmax;
% while y < ymax-r_target(xmax, ymax)
%     num = num+1;
%     pts(num, :) = [x, y];
%     y = y + r_target(x, y)*f;
% end
% y = ymax;
% while x > xmin+r_target(xmin, ymax)
%     num = num+1;
%     pts(num, :) = [x, y];
%     x = x - r_target(x, y)*f;
% end
% x = xmin;
% while y > ymin+r_target(xmin, ymin)
%     num = num+1;
%     pts(num, :) = [x, y];
%     y = y - r_target(x, y)*f;
% end


dr = 1e-10;
c = 0;
while num < nmax
    % select node from not expanded nodes
    c = c+1;
    if c > num, break, end
    types(c) = 1; %set node to expanded status
        
    x = pts(c, 1);
    y = pts(c, 2);
    r = r_target(x, y); % get desired spatial step

    phis = candidates(n);
        
    for phi = phis
        xn = x + (r+dr)*cos(phi);
        yn = y + (r+dr)*sin(phi);

        if xmin <= xn && xn <= xmax && ymin <= yn && yn <= ymax && inside(xn, yn) &&...
                all((pts(1:num, 1) - xn).^2 + (pts(1:num, 2) - yn).^2 >= r*r)
            num = num + 1;
            pts(num, :) = [xn, yn];
            prev(num) = c;
        end
    end
end
pts = pts(1:num, :);
prev = prev(1:num, :);

if num == nmax, warning('Maximal number of points reached!'); end

end
