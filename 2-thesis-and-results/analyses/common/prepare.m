clear
close all
format compact
datapath = '/mnt/data/faks/3_stopnja/phd/thesis/analyses/';
plotdatapath = 'plotdata/';
paramspath = 'params/';
imagepath = 'images/';
exportpath = 'exportdata/';

% matplotlib colors in order
BLUE = [31, 119, 180]/256;  % #1f77b4
ORANGE = [255, 127, 14]/256; % #ff7f0e
GREEN = [44, 160, 44]/256; % #2ca02c
RED = [214, 39, 40]/256; % #d62728
VIOLET = [148, 103, 189]/256; % #9467bd
BROWN = [140, 86, 75]/256; % #8c564b
PINK = [227, 119, 194]/256; % #e377c2
GREY = [127, 127, 127]/256; % #7f7f7f
YELLOW = [188, 189, 34]/256; % #bcbd22
CYAN = [23, 190, 207]/256; % #17becf

COLORS = [BLUE; ORANGE; GREEN; RED; VIOLET; BROWN; PINK; GREY; YELLOW; CYAN];
MARKS = {'o', 'x', '^', '*', 's', '+', 'p', '<', 'h'};

uex = @(x, y) cos(5*x.*(1-y)).*exp(-6*((x-0.5).^2 + (y-0.6).^2))+0.5;

ex2d = @(f) 0.25*(3+cos(3*f));
inside2d = @(x, y) sqrt(x.^2 + y.^2) < ex2d(atan2(y, x));

ex3d = @(th, f) 0.25*(3 + th.^2.*(pi - th).^2 / 4 .* (2 + cos(3*f)));
inside3d = @(x, y, z) sqrt(x.^2 + y.^2 + z.^2) < ex3d(acos(z./sqrt(x.^2 + y.^2 + z.^2)), atan2(y, x));


