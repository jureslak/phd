function x = linf_norm(M)
   x = max(abs(M(:)));
end
