function newy = rbffd_interp(x, y, newx, varargin)
%RBFCREATE Creates an RBF interpolation
%   OPTIONS = RBFSET(X, Y, 'NAME1',VALUE1,'NAME2',VALUE2,...) creates an   
%   radial base function interpolation 
%   
%   RBFCREATE with no input arguments displays all property names and their
%   possible values.
%   
%RBFCREATE PROPERTIES
% 

%
% Alex Chirokov, alex.chirokov@gmail.com
% 16 Feb 2006
tic;
% Print out possible values of properties.
if (nargin == 0) & (nargout == 0)
  fprintf('               x: [ dim by n matrix of coordinates for the data sites ]\n');
  fprintf('               y: [   1 by n vector of values at nodes ]\n');
  fprintf('            newx: [ dim by n matrix of coordinates for the evaluation points ]\n');
  fprintf('     RBFFunction: [ gaussian  | thinplate | cubic | multiquadrics | {linear} ]\n');
  fprintf('     RBFConstant: [ positive scalar     ]\n');
  fprintf('      Neighbours: [ positive integer     ]\n');
  fprintf('\n');
  return;
end
Names = [
    'RBFFunction      '
    'RBFConstant      '
    'Neighbours       '
];
[m,n] = size(Names);
names = lower(Names);

options = [];
for j = 1:m
  options.(deblank(Names(j,:))) = [];
end

%**************************************************************************
%Check input arrays 
%**************************************************************************
[nXDim nXCount]=size(x);
[nYDim nYCount]=size(y);
[nnXDim N] = size(newx);

if (nXCount~=nYCount)
  error(sprintf('x and y should have the same number of rows'));
end

if (nYDim~=1)
  error(sprintf('y should be n by 1 vector'));
end

if nXDim ~= nnXDim
    error('x and newx should have the same dimension, got %d vs. %d', nXDim, nnXDim);
end

%**************************************************************************
%Default values 
%**************************************************************************
options.('RBFFunction') = 'linear';
options.('RBFConstant') = (prod(max(x')-min(x'))/nXCount)^(1/nXDim); %approx. average distance between the nodes 
options.('Neighbours') = 3^nXDim;

%**************************************************************************
% Argument parsing code: similar to ODESET.m
%**************************************************************************

i = 1;
% A finite state machine to parse name-value pairs.
if rem(nargin-3,2) ~= 0
  error('Arguments must occur in name-value pairs.');
end
expectval = 0;                          % start expecting a name, not a value
while i <= nargin-3
  arg = varargin{i};
    
  if ~expectval
    if ~isstr(arg)
      error(sprintf('Expected argument %d to be a string property name.', i));
    end
    
    lowArg = lower(arg);
    j = strmatch(lowArg,names);
    if isempty(j)                       % if no matches
      error(sprintf('Unrecognized property name ''%s''.', arg));
    elseif length(j) > 1                % if more than one match
      % Check for any exact matches (in case any names are subsets of others)
      k = strmatch(lowArg,names,'exact');
      if length(k) == 1
        j = k;
      else
        msg = sprintf('Ambiguous property name ''%s'' ', arg);
        msg = [msg '(' deblank(Names(j(1),:))];
        for k = j(2:length(j))'
          msg = [msg ', ' deblank(Names(k,:))];
        end
        msg = sprintf('%s).', msg);
        error(msg);
      end
    end
    expectval = 1;                      % we expect a value next
    
  else
    options.(deblank(Names(j,:))) = arg;
    expectval = 0;      
  end
  i = i + 1;
end

if expectval
  error(sprintf('Expected value for property ''%s''.', arg));
end

    
%**************************************************************************
% Creating RBF Interpolatin
%**************************************************************************

switch lower(options.('RBFFunction'))
      case 'linear'          
        options.('rbfphi')   = @rbfphi_linear;
      case 'cubic'
        options.('rbfphi')   = @rbfphi_cubic;
      case 'multiquadric'
        options.('rbfphi')   = @rbfphi_multiquadrics;
      case 'thinplate'
        options.('rbfphi')   = @rbfphi_thinplate;
      case 'gaussian'
        options.('rbfphi')   = @rbfphi_gaussian;
    otherwise
        error('unknown choice %s', options.('RBFFunction'));
end

phi       = options.('rbfphi');

n = options.('Neighbours');
[I, d] = knnsearch(x', newx', 'K', n);
newy = zeros(1, N);
for i = 1:N
    A = rbfAssemble(x(:, I(i, :)), n, nXDim, phi, options.('RBFConstant'));
    b = [y(I(i, :))'; zeros(nXDim+1, 1)];
    coeff = A \ b;
    phiv = feval(phi, d(i, :)', options.('RBFConstant'));

    newy(i) = coeff(1:n)'*phiv;
    newy(i) = newy(i) + coeff(n+1);
    newy(i) = newy(i) + coeff(n+2:end)'*newx(:, i);    
end



function [A]=rbfAssemble(x, n, dim, phi, const)
r = squareform(pdist(x', 'euclidean'));
A = feval(phi,r, const);
% Polynomial part
P=[ones(n,1) x'];
A = [ A      P
      P' zeros(dim+1,dim+1)];
  
%**************************************************************************
% Radial Base Functions
%************************************************************************** 
function u=rbfphi_linear(r, const)
u=r;

function u=rbfphi_cubic(r, const)
u=r.*r.*r;

function u=rbfphi_gaussian(r, const)
u=exp(-0.5*r.*r/(const*const));

function u=rbfphi_multiquadrics(r, const)
u=sqrt(1+r.*r/(const*const));

function u=rbfphi_thinplate(r, const)
u=r.*r.*log(r+1);