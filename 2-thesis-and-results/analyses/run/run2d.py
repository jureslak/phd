ns = [11, 12, 13, 14, 15, 17, 18, 19, 21, 23, 25, 27, 29, 31, 34, 36, 39, 43, 46, 50, 54, 58, 63, 68, 74,
        80, 87, 94, 102, 110, 119, 129, 140, 151, 164, 178, 192, 208, 226, 244, 264, 286, 310, 336,
        364, 394, 427, 462, 500]

import sys
CASE = sys.argv[1]

CMDS = {
    'ff': "FreeFem++ ../ff++/annulus2d.edp -v 0 {}",
    'medusa': "../build/time2d {}",
}

if len(sys.argv) <= 2:
    repeat = 1
else:
    repeat = int(sys.argv[2])


from subprocess import run

for rep in range(repeat):

    data = []
    for n in ns:
        cmd = CMDS[CASE].format(n).split()
        print(cmd)
        out = run(cmd, capture_output=True)
        print(out.stderr.decode('utf8'))
        stdout = out.stdout.decode('utf8').strip().splitlines()

        N = float(stdout[-4])
        err = float(stdout[-3])
        time = float(stdout[-2])
        mesher = float(stdout[-1])

        data.append((N, err, time, mesher))

    with open("../wip/results2d-{}-wip-rep{:02d}.csv".format(CASE, rep+1), 'w') as f:
        for i in range(len(ns)):
            s = ' '.join("{:.16f}".format(x) for x in data[i])
            f.write(s+"\n")

print("Done!")
