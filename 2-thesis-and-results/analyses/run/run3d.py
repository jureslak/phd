ns = [10,10.1892649935926,10.3821121109651,10.578609149181,10.7788251884648,10.9828306164879,11.1906971531136,11.4024978756117,11.6183072443484,11.8382011289642,12.0622568350463,12.290553131306,12.5231702772706,12.7601900514992,13.0016957803329,13.2477723671886,13.4985063224078,13.7539857936698,14.0143005969809,14.2795422482501,14.5498039954621,14.8251808514595,15.1057696273456,15.3916689665186,15.6829793793513,15.9798032785258,16.2822450150379,16.5904109148823,16.9044093164326,17.2243506085287,17.5503472692847,17.8825139056315,18.2209672936084,18.5658264194159,18.917212521247,19.2752491319094,19.640062122254,20.0117797454266,20.390532681956,20.7764540856959,21.1696796306365,21.5703475586014,21.9785987278483,22.3945766625882,22.8184276034436,23.2503005588594,23.6903473574892,24.1387227015713,24.5955842213159,25.0610925303211];

# ns = ns[:10];

import sys
CASE = sys.argv[1]

CMDS = {
    'ff': "FreeFem++ ../ff++/annulus3d.edp -v 0 {}",
    'medusa': "../build/time3d {}",
}

if len(sys.argv) <= 2:
    repeat = 1
else:
    repeat = int(sys.argv[2])


from subprocess import run

for rep in range(repeat):

    data = []
    for n in ns:
        cmd = CMDS[CASE].format(n).split()
        print(cmd)
        out = run(cmd, capture_output=True)
        print(out.stderr.decode('utf8'))
        stdout = out.stdout.decode('utf8').strip().splitlines()

        N = float(stdout[-4])
        err = float(stdout[-3])
        time = float(stdout[-2])
        mesher = float(stdout[-1])

        data.append((N, err, time, mesher))

    with open("../wip/results3d-{}-wip-rep{:02d}.csv".format(CASE, rep+1), 'w') as f:
        for i in range(len(ns)):
            s = ' '.join("{:.16f}".format(x) for x in data[i])
            f.write(s+"\n")

print("Done!")
