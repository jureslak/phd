prepare


run([plotdatapath 'sheppard_example_data_2d.m'])
n = length(pts);
x = pts(:, 1)';
y = pts(:, 2)';

val = uex(x, y)';
v = val';
z = zeros(size(v));

m = 50;
[X, Y] = meshgrid(deal(linspace(0, 1, m)));

C = uex(X, Y);
S = scatteredInterpolant(x', y', val, 'linear');
L = S(X, Y);


f1 = setfig('b1', [350 350]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, v, 20, 'kd')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.3);
surf(X, Y, C, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('$u_{\mathrm{ex}}$')


T = delaunayTriangulation(x', y');
f3 = setfig('b3', [350 350]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, v, 20, 'kd')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.3);
triplot(T, 'k')

surf(X, Y, L, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('$\hat u$')

% exportf(f1, [imagepath 'uex.png'], '-m4')
% exportf(f3, [imagepath 'triangular_interp.png'], '-m4')