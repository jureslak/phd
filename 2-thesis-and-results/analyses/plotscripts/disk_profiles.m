prepare

casename = 'disk_adaptive_01_profiles';

load([plotdatapath casename '.mat']);

niter = length(profiles);

xd_all = R-eps-yd_all;
[~, ~, asxy_all] = disk_anal(xd_all, yd_all, P, R);

if strcmp(casename, 'disk_adaptive_01_profiles')
    xl = [0.4828 0.4862];
    yl = [11.23 11.55];
    yu = 15;
    cf = 30;
else
    xl = [0.498250731258165   0.498616377475822];
    yl = 1e2*[1.127985895653489   1.155228550839683];
    cf = 26;
    yu = 150;
end

rectx = [xl(1) xl(2) xl(2) xl(1) xl(1)];
recty = [yl(1) yl(1) yl(2) yl(2) yl(1)];

fsize = [350 300];

f1 = setfig('a2', fsize);
plot(yd_all,asxy_all, '-k', 'LineWidth', 2, 'DisplayName', 'analytical');
cm = colormap('viridis');
for i = 1:niter
   plot(yd_all, profiles{i}.sxy_all, '-', 'Color', cm(cf*i, :), ...
       'DisplayName', sprintf('iteration %d', i-1));
end
legend({}, 'Location', 'NW');
ylim([0, yu])
xlim([0, R]);
xlabel('$y;\; x = R-\gamma-y$')
ylabel('$\sigma_{xy}$')
xticks(0:0.1:0.5)
title(sprintf('$\\gamma = %g$', eps))

f2 = setfig('a4', fsize);

xd_zoom = R-eps-yd_zoom;
[~, ~, asxy_zoom] = disk_anal(xd_zoom, yd_zoom, P, R);


plot(yd_zoom, asxy_zoom, '-k', 'LineWidth', 4, 'DisplayName', 'analytical');
cm = colormap('viridis');
for i = 1:niter
   plot(yd_zoom, profiles{i}.sxy_zoom, '-', 'Color', cm(cf*i, :), ...
        'DisplayName', sprintf('iteration %d', i-1));
end
xlim(xl)
ylim(yl)
xlabel('$y;\; x = R-\gamma-y$')
ylabel('$\sigma_{xy}$')
title(sprintf('$\\gamma = %g$', eps))    

% legend({}, 'Location', 'NW');

% exportf(f1, [imagepath casename '.pdf']);
% exportf(f2, [imagepath casename '_zoom.pdf']);

