prepare

run([plotdatapath 'sheppard_example_data_2d.m'])
n = length(pts);
x = pts(:, 1)';
y = pts(:, 2)';


f1 = setfig('b1', [350 350]);
scatter(x, y, 25, 'k', 'filled')
axis equal
xlim([0 1])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
ylim([0 1])
% exportf(f1, [imagepath 'scattered_nodes.pdf'])

v = uex(x, y);
z = zeros(size(x));

f2 = setfig('b2', [350 350]); view([-34.3000   22.0000]);
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, v, 20, 'kd')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.3);
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
% exportf(f2, [imagepath 'scattered_nodes_values.pdf'])