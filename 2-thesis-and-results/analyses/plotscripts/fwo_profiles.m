prepare

casename = 'fwo_adaptive';
load([plotdatapath casename '.mat']);

load([plotdatapath 'fwo_abaqus.mat'])
FF = importdata([plotdatapath 'freefem_surface.dat']);

profiles = data.profiles;

for i = 1:data.niter
    
f1 = setfig(sprintf('ao%d', i), [350 250]);
leg = {};

mark = '-';
if i <= 2
    mark = ['o' mark];
    ms = 4;
else
    mark = ['o' mark]; 
    ms = 2;
end
if i == data.niter, ms = 0.1; end
plot(x_aba*1e3, sxx_aba, '-', 'Color', COLORS(2, :));
plot(FF(:, 1)*1e3, FF(:, 3)/1e6, '-', 'Color', COLORS(3, :));
plot(data.profiles{i}.x*1e3, data.profiles{i}.sxx/1e6, mark, ...
    'Color', COLORS(1, :), 'MarkerSize', ms);

xlim([-2e3*a, 2e3*a])
ylim([-450, 410])
ylabel('$\sigma_{xx}$ [MPa]');
xlabel('$x$ [mm]')
legend('ABAQUS', 'FreeFEM++', 'adaptive', 'Location', 'NW');
title(sprintf('iteration %d', i-1))

% exportf(f1, sprintf('%s%s%d.pdf', imagepath, casename, i));

end


