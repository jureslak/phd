prepare

run([plotdatapath 'sheppard_example_data_1d.m'])
n = length(pts);
x = pts(:, 1);

f = @(x) cos(5*x).*exp(-6*(x-0.5).^2);

val = f(x);

m = 10000;
X = linspace(0, 1, m)';

k = 2;
V = sheppard(pts, val, X, k, 2);
V2 = sheppard(pts, val, X, n, 2);
% V3 = sheppardm(pts, val, X, 0.15);

C = f(X);
N = interp1(x, val, X, 'nearest');

L = interp1(x, val, X, 'linear');

setfig('a2');
scatter(x, val, 15, 'ok', 'filled', 'DisplayName', 'Points');
plot(X, C, 'DisplayName', 'Exact'); 
plot(X, V, 'DisplayName', 'k-Sheppard');
plot(X, V2, 'DisplayName', 'Sheppard')
% plot(X, V3, 'DisplayName', 'Modified Sheppard')
% plot(X, N, 'DisplayName', 'Nearest');
% plot(X, L, 'DisplayName', 'Linear');
legend('Location', 'SW');

setfig('b4');
% scatter(x, val, 15, 'ok', 'filled', 'DisplayName', 'Points');
plot(X(1:end-2), diff(C, 2), 'DisplayName', 'Exact'); 
plot(X(1:end-1), diff(V), 'DisplayName', 'k-Sheppard');
plot(X(1:end-2), diff(V2, 2), 'DisplayName', 'Sheppard')
% plot(X(1:end-2), diff(V3, 2), 'DisplayName', 'Modified Sheppard')
legend('Location', 'SW');