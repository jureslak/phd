prepare


casename = 'Lshape_adaptive_alpha_analysis';
load([plotdatapath casename '.mat']);
na = length(ars);


finalerrs = nan(na, nerr);
times = nan(na, 1);
niters = nan(na, 1);
maxdens = nan(na, 1);
finalNs = nan(na, 1);
for i = 1:na
    d = data{i};
    finalerrs(i, l1) = data{i}.errors(end, l1);    
    finalerrs(i, l2) = data{i}.errors(end, l2);
    finalerrs(i, linf) = data{i}.errors(end, linf);
    times(i) = data{i}.time;
    niters(i) = data{i}.niter-1;
    maxdens(i) = data{i}.den;
    finalNs(i) = data{i}.Ns(end);
end

xt = [1 1.5 2 3 5 8 13 20];

fsize = [400 300];

f1 = setfig('a1', fsize);
plot(ars, niters, [MARKS{1} '-'], 'Color', COLORS(1, :));
set(gca, 'xscale', 'log');
set(gca, 'yscale', 'log');
xlim([1, 21]);
ylabel('number of iterations');
xlabel('$\alpha_{\textrm{r}}$')
xticks(xt)
ylim([1 500])
yticks([1 2.5 5 10 25 50 100 250 500 1000])

yyaxis right
plot(ars, times, [MARKS{2} '-'], 'Color', COLORS(2, :));
set(gca, 'yscale', 'log');
ylabel('execution time [s]');
yticks([1 2.5 5 10 25 50 100 250 500 1000])
ylim([1 500])


f2 = setfig('a2', fsize);
plot(ars, finalNs, [MARKS{1} '-'], 'Color', COLORS(1, :));
% plot(ars, , '--k');
avgars = 1+(ars-1)/4;
plot([ars nan ars], [finalNs(1)*ones(size(ars))*0.85 nan finalNs(1)*avgars+3e4], '--k');
set(gca, 'xscale', 'log');
xlim([1, 21]);
yticks([0 2 4 6 8 10 12 14]*1e5)
ylabel('number of nodes');
xlabel('$\alpha_{\textrm{r}}$')
xticks(xt)

yyaxis right
plot(ars, maxdens, [MARKS{2} '-'], 'Color', COLORS(2, :));
yticks(0:0.25:2);
ylabel('$\max \rho_i$');
ylim([0 1.75+0.125]+0.25)

legend('$N$', '$N_\ell, N_u$', '$\max \rho_i$', 'Location', 'NW')


f3 = setfig('a4', fsize); leg = {};
for i = 1:nerr
    plot(ars, finalerrs(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); leg{end+1} = errleg{i};
end
ll
xlim([1, 21]);
ylim([2e-7 1e-2])
yticks(10.^(-7:-2))
ylabel('errors');
xlabel('$\alpha_{\textrm{r}}$')
xticks(xt)
legend(leg, 'Location', 'SW');


f4 = setfig('a3', fsize); leg = {};
for i = [l1 linf]
    plot(data{1}.Ns, data{1}.errors(:, i), [MARKS{i} '-'],...
        'Color', COLORS(i, :)); leg{end+1} = [errleg{i} ',$_1$'];
end
M = {}; M{l1} = '*'; M{linf} = 'h';
for i = [l1 linf]
    plot(data{20}.Ns, data{20}.errors(:, i), [M{i} '-'],...
        'Color', COLORS(i+3, :), 'MarkerSize', 6, 'LineWidth', 2); 
    leg{end+1} = [errleg{i} ',$_2$'];
end
plot(data{1}.Ns, 2*data{1}.Ns.^-0.5, 'k--'); leg{end+1} = '$O(h)$';
plot(data{1}.Ns, 1.4*data{1}.Ns.^-1, 'k-.'); leg{end+1} = '$O(h^2)$';
ll
yticks(10.^(-6:0))
xticks(10.^(2:6))
ylim([1e-6, 2e-1])
xlim([2e2, 5e5])
title(sprintf('$\\alpha_{\\textrm{r},1} = %.2f$, $\\alpha_{\\textrm{r}, 2} = %.2f$', ars(1), ars(20)));
legend(leg, 'Location', 'SW');

% exportf(f1, [imagepath casename '_iter_time.pdf'])
% exportf(f2, [imagepath casename '_node_den.pdf'])
% exportf(f3, [imagepath casename '_err.pdf'])
% exportf(f4, [imagepath casename '_first_mid.pdf'])
