prepare

data = [plotdatapath 'mobius.h5'];
pts = h5read(data, '/domain/pos');

N = length(pts);

x = pts(:, 1);
y = pts(:, 2);
z = pts(:, 3);


f1 = setfig('b3', [500 400]); 
view([  -56.7000   31.6000])
scatter3(x, y, z, 5, 'ok', 'filled');
daspect([1 1 1])
xlim([-1.5 1.5])
ylim([-1.5 1.5])
xticks(-1.5:0.5:1.5);
yticks(-1.5:0.5:1.5);
% exportf(f1, [imagepath 'mobius.pdf'])

% r = sqrt(x.^2+y.^2+z.^2);
% ph = mod(atan2(y, x) + 2*pi, 2*pi);
% th = acos(z./r);
% setfig('b2');
% scatter(ph, th, 5, 'ok', 'filled');
% daspect([1 1 1])
% xlabel('$\varphi$');
% ylabel('$\vartheta$');
% xlim([0, 2*pi]);
% ylim([0, pi]);
% xticks([0, pi/2, pi, 3*pi/2, 2*pi]);
% xticklabels({'$0$', '$\pi/2$', '$\pi$', '$3\pi/2$', '$2\pi$'});
% yticks([0, pi/2, pi, 3*pi/2, 2*pi]);
% yticklabels({'$0$', '$\pi/2$', '$\pi$', '$3\pi/2$', '$2\pi$'});