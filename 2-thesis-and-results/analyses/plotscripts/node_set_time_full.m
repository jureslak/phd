prepare

d = 3;
cs = {'bnd_and_int', 'var'};
names = {'constant $h$ (PA)', 'variable $h$ (PA)'};

fs = [400 300];
f1 = setfig('b1', fs);

for i = 1:length(cs)
    c = 'PDS-RF';
    casename = sprintf('quasi_uniform_%s_%dd', cs{i}, d);
    data = [plotdatapath casename '.h5'];
    nm = names{i};
    
    Ns = h5read(data, ['/' c '/Ns']);
    times = h5read(data, ['/' c '/times']);
    if d == 2 && i == 1, Ns(end-1:end) = NaN; end
    if d == 2 && i == 2, Ns(1:6) = NaN; end
    
    mNs = median(Ns, 2);
    mtimes = median(times, 2);
    lotimes = quantile(times, 0.25, 2);
    hitimes = quantile(times, 0.75, 2);
    
    if strcmp(cs, 'time')
    dev = mad(times, 1, 2);
    p = 100*(dev./mtimes);
    max(p)
    end
    
    plot(mNs, mtimes, [MARKS{i} '-'], 'DisplayName', nm, 'Color', COLORS(i, :));
%     plot(mNs, mss, marks{i}, 'DisplayName', ['separation distance (' c ')'], 'Color', 0.7*COLORS(i, :));

end
Ns = [8e2; Ns; 1e7];
if d == 2, C = exp(-14.4); else, C = exp(-12.6); end
plot(Ns, C*Ns.*log(Ns), 'k--', 'DisplayName', '$O(N \log N)$');
ll
ylabel('execution time [s]')
legend({}, 'Location', 'NW')
title(sprintf('$d = %d$', d));
xticks(10.^(2:6))
yticks(10.^(-3:3))

if d == 2
    ylim([1e-2, 4e1])
    xlim([1e3, 2.5e6])
elseif d == 3
    ylim([7e-3, 2.25e2])
    xlim([3e2, 2.5e6])   
end
% exportf(f1, [imagepath 'node_set_times_full_' num2str(d) 'd.pdf']);
