prepare

d = 3;
load([plotdatapath sprintf('eigs_%d.mat', d)]);

f1 = setfig('b1');
x = real(lam);
y = imag(lam);
scatter(x, y, 5, 'ok', 'filled');
daspect([1 1 1])


[~, I] = sort(real(lam), 'descend');
I = I(1:5);
s = '';
for i = 1:5
   s = [s sprintf('\n$%.2f %+.2fi$', x(I(i)), y(I(i)))]; 
end

f1 = setfig('b1', [400 200]);
scatter(x, y, 5, 'hk', 'filled');
title(sprintf('%dD Poisson problem, order $= %d$, $n = %d$', d, ord, n));
text(0.01, 0.58, s, 'units', 'normalized');
xlabel('Re($\lambda$)')
ylabel('Im($\lambda$)')
if d == 3
   xlim([-4000 0])
%     daspect([1 1 1])
elseif d == 2
   xlim([-9e4, 0]) 
end

% exportf(f1, [imagepath sprintf('eigs_%dd.png', d)], '-m4');