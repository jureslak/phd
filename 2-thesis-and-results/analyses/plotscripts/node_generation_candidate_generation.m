prepare

h = @(x, y) 0.05*(1+norm([x y], 1));
box = [-4 -4 4 4];
n = 12;
nmax = 1000000;
r = @(th) 0.25*(3 + cos(3*th));
th = linspace(0, 2*pi, 500);

inside = @(x, y) norm([x y]) < r(atan2(y, x));

candidates_f = @(n) linspace(0, 2*pi, n+1);
candidates_rf = @(n) 2*pi*rand() + linspace(0, 2*pi, n+1);
candidates_r = @(n) 2*pi*rand(1, n);


CG = {candidates_f, candidates_rf, candidates_r};
ns = {10, 10, 10};
titles = {'fixed', 'randomized', 'random'};

for c = 1:3

[pts, p] = PNP(box, h, ns{c}, nmax, inside, [[-0.25 0.5]; [-0.25, -0.5]],...
    CG{c});

x = pts(:, 1);
y = pts(:, 2);

I = p > 0;
xn = x(I);
yn = y(I);
xp = x(p(I));
yp = y(p(I));

f1 = setfig(sprintf('bo%d', c), [300 300]);
plot(r(th).*cos(th), r(th).*sin(th), 'k');
%quiver(xp, yp, xn-xp, yn-yp, 'AutoScale','off', 'LineWidth', 1.2, 'Color', COLORS(2, :));
scatter(x, y, 15, 'k', 'filled');
% scatter(x(~I), y(~I), 30, '*', 'MarkerEdgeColor', COLORS(1, :));
% scatter(x(~I), y(~I), 10, 'o', 'MarkerEdgeColor', COLORS(1, :));
axis square
title(sprintf('%s candidates, $N = %d$', titles{c}, length(pts)))
xlim([-1, 1.1]);
xticks(-1:0.5:1)

f2 = setfig(sprintf('bo%d', c+8), [300 300]);
plot(r(th).*cos(th), r(th).*sin(th), 'k');
quiver(xp, yp, xn-xp, yn-yp, 'AutoScale','off', 'LineWidth', 1.2, 'Color', COLORS(2, :));
scatter(xn, yn, 15, 'k', 'filled');
scatter(x(~I), y(~I), 30, '*', 'MarkerEdgeColor', COLORS(1, :));
scatter(x(~I), y(~I), 10, 'o', 'MarkerEdgeColor', COLORS(1, :));
axis square
title(sprintf('%s candidates, $N = %d$', titles{c}, length(pts)))
xlim([-1, 1.1]);
xticks(-1:0.5:1)

exportf(f1, [imagepath 'candidates_' titles{c} '_nodes.pdf']);
exportf(f2, [imagepath 'candidates_' titles{c} '_forest.pdf']);

end