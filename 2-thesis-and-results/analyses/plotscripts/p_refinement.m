prepare

d = 3;
casename = sprintf('p_refinement_%dd', d);
data = [plotdatapath casename '.h5'];
% data = ['data/' casename '.h5'];
h5info(data);

% ns = str2num(h5readatt(data, '/conf', 'num.ns'));
% augs = str2num(['[' h5readatt(data, '/conf', 'approx.augs'), ']']);
augs = [2 4 6 8];

times = h5read(data, '/times');

Ns = h5read(data, '/Ns');
err1 = h5read(data, '/err1');
err2 = h5read(data, '/err2');
erri = h5read(data, '/errinf');

% erri(end, end) = nan;

naug = length(augs);

f1 = setfig('a2', [400, 300]); leg = {};
for i = 1:naug
  plot(Ns(:, i), erri(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :));
  leg{end+1} = sprintf('$m = %d$', augs(i));
end
c = [40 750 2e4 3e4];
Ns = Ns(8:end, :);
for i = 1:naug
  plot(Ns(:, i), c(i).*Ns(:, i).^(-augs(i)/d), 'k--');
end
leg{end+1} = '$O(h^m)$';
ll
ylabel('$e_\infty$')
legend(leg, 'Location', 'SW')
title(sprintf('$d = %d$', d));

xlim([1e2 1e6])
if d == 2
    ylim([1e-18, 1e0])
else
    ylim([1e-11, 1e0])
end
xticks(10.^(2:6))

% chi=get(gca, 'Children');
% set(gca, 'Children',flipud(chi));


exportf(f1, sprintf('%s/p_ref_%dd.pdf', imagepath, d));
