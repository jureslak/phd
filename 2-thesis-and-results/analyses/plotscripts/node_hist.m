prepare

d = 3;
cs = 'var';
% cs = 'bnd_and_int';
load([plotdatapath 'node_hist_' cs '_' num2str(d) 'd.mat']);
N
f1 = setfig('b1', [450 300]);
h = histogram('BinCounts', counts, 'BinEdges', edges,...
    'FaceColor', 'w', 'FaceAlpha', 1);
xticks(0.8:0.2:1.8)
xlim([0.8 1.85])
ylabel('');
xlabel('$d_{i,j}''$')
title(sprintf('$N = %d$, $d = %d$, $n = %d$', N, d, k));
if d == 2 && strcmp(cs, 'var')
    ylim([0 1.3e6])
    yticks([0 2e5, 4e5, 1e6, 1.1e6, 1.2e6, 1.3e6])
    breakyaxis([0.4e6 0.9e6]);
end
if d == 2 && strcmp(cs, 'bnd_and_int')
    ylim([0 2e6])
    yticks([0 2e5, 4e5, 6e5, 1.6e6, 1.8e6, 2e6])
    breakyaxis([0.75e6 1.5e6]);
end

% exportf(f1, [imagepath 'node_hist_' cs '_' num2str(d) '.pdf']);


