prepare


run([plotdatapath 'sheppard_example_data_2d.m'])
n = length(pts);
x = pts(:, 1)';
y = pts(:, 2)';

val = uex(x, y);
v = val;
z = zeros(size(val));

data = [plotdatapath 'wls_mls.h5'];
xx = h5read(data, '/x');
yy = h5read(data, '/y');

[X, Y] = ndgrid(xx, yy);

wls = h5read(data, '/wls_val');
mls = h5read(data, '/mls_val');
lmls = h5read(data, '/k-mls_val');
mod_mls = h5read(data, '/mod-mls_val');

f1 = setfig('b1', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, wls, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('WLS approximant around (0.5, 0.5)')

f2 = setfig('b3', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, mls, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('MLS approximant')

f3 = setfig('b2', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, lmls, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('9-MLS approximant')

f4 = setfig('b4', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, mod_mls, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('MLS approximant, $\delta = 0.25$')

% exportf(f1, [imagepath 'wls.png'], '-m4')
% exportf(f2, [imagepath 'mls.png'], '-m4')
% exportf(f3, [imagepath 'k-mls.png'], '-m4')
% exportf(f4, [imagepath 'mod-mls.png'], '-m4')
