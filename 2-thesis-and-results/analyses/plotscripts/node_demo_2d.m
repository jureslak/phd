prepare


f1 = setfig('b1', [300 300]);
f = linspace(0, 2*pi, 200);
r = ex2d(f);
x = r.*cos(f);
y = r.*sin(f);
plot(x, y, '-k', 'LineWidth', 1);
daspect([1 1 1]);
xlim([-1, 1.2]);
xticks(-1:0.5:1);

% exportf(f1, [imagepath 'domain_2d.pdf']);

data = [datapath 'node_demo_2d_wip.h5'];
pts = h5read(data, '/domain/pos');

sz = [300 300];
f3 = setfig('bo9', sz);

x = pts(:, 1);
y = pts(:, 2);

scatter(x, y, 3, 'ok', 'filled');
title(sprintf('$N = %d$', length(pts)));
daspect([1 1 1]);
xlim([-1, 1.2]);
xticks(-1:0.5:1);


f4 = setfig('bo10', sz);
scatter(x, y, 3, 'ok', 'filled');
% title(sprintf('$N = %d$', length(pts)));
daspect([1 1 1]);
xlim([-0.7, -0.2]);
ylim([-0.25 0.25])


f5 = setfig('bo11', sz);
scatter(x, y, 3, 'ok', 'filled');
% title(sprintf('$N = %d$', length(pts)));
daspect([1 1 1]);
xlim([-0.6, -0.4]);
ylim([-0.1 0.1])


f6 = setfig('bo12', sz);
scatter(x, y, 3, 'ok', 'filled');
% title(sprintf('$N = %d$', length(pts)));
daspect([1 1 1]);
xlim([-0.52, -0.46]);
ylim([-0.03 0.03])
yticks([-0.02, 0, 0.02])

% exportf(f3, [imagepath 'domain_2d_var_z0.pdf'])
% exportf(f4, [imagepath 'domain_2d_var_z1.pdf'])
% exportf(f5, [imagepath 'domain_2d_var_z2.pdf'])
% exportf(f6, [imagepath 'domain_2d_var_z3.pdf'])