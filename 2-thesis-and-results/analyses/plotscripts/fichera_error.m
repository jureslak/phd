prepare


casenameU = 'fichera_uniform_error';
casenameA = 'fichera_adaptive_error';

A = load([plotdatapath casenameA '.mat']);
U = load([plotdatapath casenameU '.mat']);


f1 = setfig('a2', [350 300]);
ll;
xlim([1e3 1e6]);
ylim([1e-3 1e1]);
leg = {};
for i = 1:A.nerr
    plot(A.Ns, A.errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); leg{end+1} = A.errleg{i};
%     k = polyfit(log(A.Ns), log(A.errors(:, i)), 1);
%     plot(A.Ns, exp(k(2)).*A.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end
A.Ns(end+1) = A.Ns(end)*1.3;
plot(A.Ns, 30*A.Ns.^-0.333, 'k--'); leg{end+1} = '$O(h)$';
plot(A.Ns, 25*A.Ns.^-0.666, 'k-.'); leg{end+1} = '$O(h^2)$';
legend(leg, 'Location', 'SW');
title('Adaptive');
xticks(10.^(3:6))
yticks(10.^(-6:1));

f2 = setfig('a4', [350 300]);
ll;
xlim([1e3 1e6]);
ylim([1e-3 1e1]);
leg = {};
for i = 1:U.nerr
    plot(U.Ns, U.errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); leg{end+1} = U.errleg{i};
    
%     k = polyfit(log(U.Ns), log(U.errors(:, i)), 1);
%     plot(U.Ns, exp(k(2)).*U.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end
U.Ns(end) = U.Ns(end)*1.1;
plot(U.Ns, 2*U.Ns.^-0.333, 'k--'); leg{end+1} = '$O(h)$';
% plot(U.Ns, 20*U.Ns.^-1, 'k--'); leg{end+1} = '$O(h^2)$';
legend(leg, 'Location', 'SW');
title('Uniform');
xticks(10.^(3:6))
yticks(10.^(-6:1));

exportf(f1, [imagepath casenameA '.pdf'])
exportf(f2, [imagepath casenameU '.pdf'])
