prepare

d = 3;
casename = sprintf('quasi_uniform_time_%dd', d);
data = [plotdatapath casename '.h5'];

cases = {'PDS-RF', 'SKF', 'FF', 'PDS-RF-grid'};
names = {'PA', 'PDS', 'GD', 'PA-grid'};

ns = str2num(h5readatt(data, '/conf', 'num.ns'));
iter = 1:length(ns);

fs = [400 300];
f1 = setfig('b1', fs);

for i = 1:length(cases)
    c = cases{i};
    nm = names{i};
    
    Ns = h5read(data, ['/' c '/Ns']);
    times = h5read(data, ['/' c '/times']);
    
    mNs = median(Ns, 2);
    mtimes = median(times, 2);
    lotimes = quantile(times, 0.25, 2);
    hitimes = quantile(times, 0.75, 2);
    dev = mad(times, 1, 2);
    p = 100*(dev./mtimes);
    max(p)
    
    plot(mNs, mtimes, [MARKS{i} '-'], 'DisplayName', nm, 'Color', COLORS(i, :));
%     plot(mNs, mss, marks{i}, 'DisplayName', ['separation distance (' c ')'], 'Color', 0.7*COLORS(i, :));
end
ll
ylabel('execution time [s]')
legend({}, 'Location', 'NW')
title(sprintf('$d = %d$', d));
xticks(10.^(2:6))
yticks(10.^(-3:3))

if d == 2
    ylim([1e-3, 4e1])
    xlim([1e3, 2.5e6])
elseif d == 3
    ylim([7e-3, 2.25e2])
    xlim([3e2, 2.5e6])   
end
% exportf(f1, [imagepath 'node_set_time' num2str(d) '.pdf']);
