prepare

casename = '2d';
% casename = '3d';
d = str2num(casename(1));
rep = 9;

FF = zeros(rep, 49, 4); % 49
ME = zeros(rep, 49, 4); % 49
for r = 1:rep
    FF(r, :, :) = dlmread(sprintf('%s/results%s-ff-rep%02d.csv', plotdatapath, casename, r));
    ME(r, :, :) = dlmread(sprintf('%s/results%s-medusa-rep%02d.csv', plotdatapath, casename, r));
end

casenames = {'FreeFem++', 'Medusa'};
data = {FF, ME};
ndata = length(data);
linetype = {'o-', '<-'};

fsize = [350 300];
f1 = setfig('a1', fsize); leg1 = {};
f2 = setfig('a3', fsize); leg2 = {};
f3 = setfig('a2', fsize); leg3 = {};
f4 = setfig('a4', fsize); leg4 = {};

for i = 1:ndata
D = data{i};

Ns = D(1, :, 1);
errs = D(1, :, 2);
times = median(D(:, :, 3));
stdtimes = std(D(:, :, 3));
maxtimes = max(D(:, :, 3));
mintimes = min(D(:, :, 3));
mesher_times = median(D(:, :, 4));

figure(f1);
k = polyfit(log(Ns), log(errs), 1);
plot(Ns, errs, linetype{i}, 'Color', COLORS(i, :)); leg1{end+1} = sprintf('%s, $k = %.2f$', casenames{i}, k(1));
% plot(Ns, exp(k(2))*Ns.^k(1), 'k--'); leg1{end+1} = ;

figure(f2);
k = polyfit(log(Ns), log(times), 1);
errorbar(Ns, times, stdtimes, linetype{i}, 'Color', COLORS(i, :)); leg2{end+1} = sprintf('%s, $k = %.2f$', casenames{i}, k(1));

figure(f3);
k = polyfit(log(Ns), log(mesher_times), 1);
plot(Ns, mesher_times, linetype{i}, 'Color', COLORS(i, :)); leg3{end+1} = sprintf('%s, $k = %.2f$', casenames{i}, k(1));

figure(f4);
k = polyfit(log(Ns), log(times - mesher_times), 1);
plot(Ns, times-mesher_times, linetype{i}, 'Color', COLORS(i, :)); leg4{end+1} = sprintf('%s, $k = %.2f$', casenames{i}, k(1));
% k = polyfit(log(Ns), log(times), 1);
% plot(Ns, exp(k(2))*Ns.^k(1), 'k--'); leg2{end+1} = sprintf('$k = %.2f$', k(1));

end

figure(f1);
ll
legend(leg1);
ylabel('$\ell_\infty$ error')
xlabel('DOF')
if d == 3
xlim([5e3 1.5e5])
ylim([2e-3, 5e-2])
yticks([0.002,  0.005, 0.01, 0.02, 0.05])
xticks([1e4, 3e4, 1e5])
else
ylim([1e-5 0.2])
yticks(10.^(-6:0))
xticks(10.^(2:6))
end
title(upper(casename))

figure(f2);
ll
legend(leg2, 'Location', 'NW');
ylabel('CPU time [s]');
xlabel('DOF')
if d == 3
yticks([0.25 0.5 1 2 5 10 20])
xticks([1e4, 3e4, 1e5])
ylim([0.2 20])
xlim([5e3 1.5e5])
else
ylim([5e-4 2e2])
yticks(10.^(-4:2))
xticks(10.^(2:6)) 
end
title(upper(casename))

figure(f3);
ll
legend(leg2, 'Location', 'NW');
ylabel('time [s]');
xlabel('DOF')
title('mesher CPU time');

figure(f4);
ll
legend(leg2, 'Location', 'NW');
ylabel('time [s]');
xlabel('DOF')
title('solution CPU time');

% exportf(f1, [imagepath 'time' casename '_err.pdf']);
% exportf(f2, [imagepath 'time' casename '_cpu.pdf']);