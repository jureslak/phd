prepare

% n = 11;
% rng(1337)

run([plotdatapath 'sheppard_example_data_2d.m'])
n = length(pts);
% A = rand(n, 2);
% x = A(:, 1);
% y = A(:, 2);

x = pts(:, 1)';
y = pts(:, 2)';


f2 = setfig('b2', [350 450]); view(3);
% scatter3(x(1), y(1), 1, 20, 'filled', 'k')

eps = 0.25;
den = 100;
for i = 1:1
    xs = linspace(0.5-3*eps, 0.5+3*eps, den);
    ys = linspace(0.5-3*eps, 0.5+3*eps, den);
    [X, Y] = meshgrid(xs, ys);
    surf(X, Y, exp(-((X-0.5).^2 + (Y-0.5).^2)/eps^2),...
        'EdgeColor', 'none');
    colormap viridis
    daspect([1 1 1])
end
d = 0.3;
xlim([-d, 1+d])
ylim([-d, 1+d])
zlim([-d, 1+d])
xlabel('$x$')
ylabel('$y$')
zlabel('')
caxis([0 1])
% colorbar
title('$\mathcal{K}((x, y), (0, 0))$')
axes.SortMethod='ChildOrder';

f1 = setfig('b1', [350, 450]); hold off
scatter3(x, y, ones(size(x)), 20, 'filled', 'k') 
hold on

eps = 0.25;
den = 100;
f = 3;
for i = 1:n
    xs = linspace(x(i)-f*eps, x(i)+f*eps, den);
    ys = linspace(y(i)-f*eps, y(i)+f*eps, den);
    [X, Y] = meshgrid(xs, ys);
    surf(X, Y, exp(-((X-x(i)).^2 + (Y-y(i)).^2)/eps^2),...
        'EdgeColor', 'none');
    colormap viridis
    daspect([1 1 1])
end
d = 0.3;
xlim([-d, 1+d])
ylim([-d, 1+d])
zlim([-d, 1+d])
caxis([0 1])
% colorbar
xlabel('$x$')
box on
ylabel('$y$')
title(sprintf('basis $\\{\\mathcal{K}(\\cdot,\\, ${\\boldmath $x_i$}$) \\}_{i=1}^{%d}$', length(x)))

% exportf(f2, [imagepath 'gaussian_kernel.png'], '-m4');
% exportf(f1, [imagepath 'kernel_basis.png'], '-m4');

