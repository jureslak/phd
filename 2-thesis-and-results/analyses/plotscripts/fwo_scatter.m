prepare

casename = 'fwo_adaptive';
load([plotdatapath casename '.mat']);

ni = data.niter;
data = data.iters;

for i = 1:ni
    
f1 = setfig(sprintf('ao%d', i), [500 250]);
leg = {};

pos = data{i}.pos;
scatter(pos(:, 1)*1000, pos(:, 2)*1000, 5, data{i}.vm/1e6, 'filled');
daspect([1 1 1])
title(sprintf('iteration %d', i-1));
colormap viridis
colorbar
xlabel('$x$ [mm]')
ylabel('$y$ [mm]')

% exportf(f1, sprintf('%s%s%d.pdf', imagepath, casename, i));

end


