prepare

load([plotdatapath 'err_ind_sample.mat'])

f1 = setfig('b1', [250, 350]);
epss = 0.0032;
eta = 0.0007;

err_ind(err_ind > 0.014) = 0.013;
M = max(err_ind);
m = min(err_ind);

alpha = 5;

err = linspace(m, M, 100);
f = ones(size(err));
I1 = find(err > epss);
f(I1) = 1 + (err(I1) - epss)/(M - epss)*(alpha-1);
I2 = find(err < eta);
f(I2) = (1 + (eta - err(I2))/(eta - m)*(1/alpha-1));

plot(f, err, 'k', 'LineWidth', 1.5)
xlabel('density increase factor $f_i$')
ylabel('value of error indicator $\hat{e}_i$')

g = 0.5;
plot([-2.5, 100], epss*[1 1], '--', 'LineWidth', 1, 'Color', [1 1 1]*g)
plot([-2.5, 100], M*[1 1], '--', 'LineWidth', 1, 'Color', [1 1 1]*g)
plot([-2.5, 100], eta*[1 1], '--', 'LineWidth', 1, 'Color', [1 1 1]*g)
plot([-2.5, 100], m*[1 1], '--', 'LineWidth', 1, 'Color', [1 1 1]*g)
xlim([0, alpha+1])
ylim([-0.0001, 0.014])

shift = 0.0006;
text(alpha*1.04, m+shift/2, '$m$')
text(alpha*1.04, eta+shift, '$\varepsilon_{\textrm{d}}$')
text(alpha*1.04, epss+shift*.8, '$\varepsilon_{\textrm{r}}$')
text(alpha*1.04, M+shift*.8, '$M$')

xticks([1/alpha, 1, alpha])
yticks(0:0.002:0.014)
yticklabels(0:0.002:0.014)
xticklabels({'$1/\alpha_{\textrm{d}}\ $', '1', '$\alpha_{\textrm{r}}$'})

grid on
exportf(f1, [imagepath 'density_change_factor.pdf'])