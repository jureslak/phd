prepare

load([plotdatapath 'err_ind_sample.mat'])

f1 = setfig('b1', [600, 350]);
threshold = 0.0032;
threshold2 = 0.0007;

err_ind(err_ind > 0.014) = 0.013;
M = max(err_ind);
m = min(err_ind);
plot(err_ind, 'o', 'MarkerFaceColor', 'k', 'MarkerSize', 4, 'MarkerEdgeColor', 'k');
plot([-2.5, 100], M*[1 1], '--', 'LineWidth', 2, 'Color', COLORS(1, :))
plot([-2.5, 100], threshold*[1 1], '--', 'LineWidth', 2, 'Color', COLORS(2, :))
plot([-2.5, 100], threshold2*[1 1], 'b--', 'LineWidth', 2, 'Color', COLORS(3, :))
plot([-2.5, 100], m*[1 1], '--k', 'LineWidth', 2, 'Color', COLORS(4, :))
fill([-2.5 -2.5 100 100], [threshold M M threshold], [0 0 0], 'FaceAlpha', 0.02, 'EdgeColor', 'none')
fill([-2.5 -2.5 100 100], [threshold threshold2 threshold2 threshold], [0 0 0], 'FaceAlpha', 0.10, 'EdgeColor', 'none')
fill([-2.5 -2.5 100 100], [threshold2 m m threshold2], [0 0 0], 'FaceAlpha', 0.2, 'EdgeColor', 'none')
[~,lh] = legend(...
       '\ \ $\hat{e}$',...
       '\ \ $M$',...
       '\ \ $\varepsilon_{\textrm{r}}$', ...
       '\ \ $\varepsilon_{\textrm{d}}$', ...
       '\ \ $m$',...
       '\ \ denser',...
       '\ \ no change',...
       '\ \ sparser',...
       'Location', 'bestoutside');
xlim([0, length(err_ind)+1])
xlabel('node number $i$')
% ylabel('value of error indicator $\hat{e}_i$')
ylabel('')

id = 20;
annotation('textarrow', [0.26464 0.22454]+0.020,...
           [0.82 0.855],'String','density increase by factor $\alpha_{\textrm{r}}$',...
           'Interpreter', 'LaTeX')
annotation('textarrow', [0.34 0.365]+0.038,...
           [0.458 0.317]+0.025,'String','almost no density increase ',...
           'Interpreter', 'LaTeX')
text(0.085, 0.18, 'no density change', 'Units', 'normalized')
annotation('textarrow', [0.72 0.68],...
           [0.17 0.15],'String','density decrease ',...
           'Interpreter', 'LaTeX')
patches = findobj(lh, 'type', 'patch');
set(patches(1), 'facea', 0.02)
set(patches(2), 'facea', 0.1)
set(patches(3), 'facea', 0.2)
grid on
% exportf(f1, [imagepath 'density_change_sample.pdf'])
