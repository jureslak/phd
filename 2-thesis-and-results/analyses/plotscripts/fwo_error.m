prepare

casename = 'fwo_adaptive';
load([plotdatapath casename '.mat']);

load([plotdatapath 'fwo_abaqus.mat'])
FF = importdata([plotdatapath 'freefem_surface.dat']);

testx = linspace(-2*a, 2*a, 100000);
femsxx = interp1(FF(:, 1), FF(:, 3)/1e6, testx);
abasxx = interp1(x_aba, sxx_aba, testx);

err1base = mean(abs(femsxx - abasxx)) / mean(abs(abasxx));

profiles = data.profiles;


ni = data.niter;
iterdata = nan(ni, 2);

for i = 1:data.niter
    rbffdsxx = interp1(profiles{i}.x, profiles{i}.sxx/1e6, testx);
    iterdata(i, 1) = mean(abs(rbffdsxx-abasxx)) / mean(abs(abasxx));
    iterdata(i, 2) = mean(abs(rbffdsxx-femsxx)) / mean(abs(femsxx));
end

f2 = setfig('a4', [500, 300]);
plot(0:ni-1, iterdata(:, 1), 'o-');
plot(0:ni-1, iterdata(:, 2), 'o-');
plot([-1, ni], [err1base, err1base], '--k');
xlim([-0.5, ni-0.3]);
set(gca, 'yscale', 'log')
ylim([2e-3, 1])
legend('error against ABAQUS', 'error against FreeFEM++',...
       'reference solution variability');
ylabel('$e_1$ on contact surface $[-2a, 2a]$')
xlabel('iteration number')
% exportf(f2, [imagepath casename '_error.pdf'])
