prepare

d = 3;
casename = sprintf('quasi_uniform_time_%dd', d);
data = [plotdatapath casename '.h5'];

cases = {'PDS-RF', 'SKF', 'FF'};
names = {'PA', 'PDS', 'GD'};

ns = str2num(h5readatt(data, '/conf', 'num.ns'));
iter = 1:length(ns);

fs = [400 300];
f1 = setfig('b1', fs);
f2 = setfig('b3', fs);
% plot(1./ns, 1./ns, 'k--');

for i = 1:length(cases)
    c = cases{i};
    n = names{i};
    
    Ns = h5read(data, ['/' c '/Ns']);
    times = h5read(data, ['/' c '/times']);
    hs = h5read(data, ['/' c '/h']);
    ss = h5read(data, ['/' c '/s']);
    
    mhs = hs(:, 1);
    mss = ss(:, 1);

    figure(f1)
    plot(ns, mhs, [MARKS{i} '-'], 'DisplayName', ['$h_{X_i, \Omega}$ (' n ')'], 'Color', COLORS(i, :));
    plot(ns, mss, [MARKS{i} '-'], 'DisplayName', ['$s_{X_i}$ (' n ')'], 'Color', 0.7*COLORS(i, :));
    
    figure(f2);
    plot(ns, mhs./mss, [MARKS{i} '-'], 'DisplayName', n, 'Color', COLORS(i, :))
end
figure(f1);
plot(ns, 0.97./ns, 'k--', 'DisplayName', 'target spacing');
ll
ylabel('distance')
xlabel('$1/h$');
title(sprintf('$d = %d$', d));
legend({}, 'Position', [0.5994    0.5224    0.3498    0.4265])
if d == 2, ylim([8e-4 1e-1]); xticks([50 100 200 500 1000]); end
if d == 3, xticks([5 7 10 20 30 50 70]); end
mh = 0.9*min(ns);
Mh = 1.1*max(ns);
xlim([mh, Mh])

figure(f2);
set(gca, 'xscale', 'log')
ylabel('$\gamma_{X_i, \Omega}$')
xlabel('$1/h$');
legend({})
title(sprintf('$d = %d$', d));
ylim([1 2.5])
xlim([mh, Mh])
if d == 3, xticks([5 7 10 20 30 50 70]); end
if d == 2, xticks([50 100 200 500 1000]); end

% exportf(f1, sprintf('%s/node_set_fill_sep_dist_%dd.pdf', imagepath, d));
% exportf(f2, sprintf('%s/node_set_fill_qu_%dd.pdf', imagepath, d));
