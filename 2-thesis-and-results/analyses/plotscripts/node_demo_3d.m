prepare


f1 = setfig('b1', [400 300]); view(3);
ff = linspace(0, 2*pi, 100);
thth = linspace(0, pi, 100);
[f, th] = meshgrid(ff, thth);
r = ex3d(th, f);
x = r.*sin(th).*cos(f);
y = r.*sin(th).*sin(f);
z = r.*cos(th);
surf(x, y, z, 'EdgeColor', 'none');
daspect([1 1 1]);
xlim([-2, 2]);
ylim([-2, 2]);
zlim([-1, 1]);
xticks(-2:1:2);
yticks(-2:1:2);
colormap viridis
material dull
lighting gouraud
camlight

% exportf(f1, [imagepath 'domain_3d.png'], '-m4');

% xx = linspace(-2, 2, 50);
% yy = linspace(-2, 2, 50);
% zz = linspace(-1, 1, 50);
% [X, Y, Z] = ndgrid(xx, yy, zz);
% 
% R = X.^2+Y.^2+Z.^2;
% 
% 
% I = inside3d(X, Y, Z);
% setfig('b2'); view(3);
% scatter3(X(I), Y(I), Z(I), 'filled');
% daspect([1 1 1])

data = [datapath 'node_demo_3d_wip.h5'];
pts = h5read(data, '/domain/pos');
types = h5read(data, '/domain/types');

N = length(pts);

x = pts(:, 1);
y = pts(:, 2);
z = pts(:, 3);

w = [-47.9000   42.8000];
sz = [350 350];

f0 = setfig('b3', sz); view(w)
scatter3(x, y, z, 3, 'ok', 'filled');
title(sprintf('All nodes, $N = %d$', length(x)))
daspect([1 1 1])
xlim([-1.3, 1.75]);
xticks(-1.5:0.5:1.5);
ylim([-1.6, 1.6]);
yticks(-1.5:0.5:1.5);
zlim([-1 1])
zticks(-1.5:0.5:1.5);

%%%

I = types < 0;
x = x(I);
y = y(I);
z = z(I);

f1 = setfig('b1', sz); view(w)
scatter3(x, y, z, 5, 'ok', 'filled');
title(sprintf('Boundary nodes, $N = %d$', length(x)))
daspect([1 1 1])
xlim([-1.3, 1.75]);
xticks(-1.5:0.5:1.5);
ylim([-1.6, 1.6]);
yticks(-1.5:0.5:1.5);
zlim([-1 1])
zticks(-1.5:0.5:1.5);


r = sqrt(x.^2+y.^2+z.^2);
ph = mod(atan2(y, x) + 2*pi, 2*pi);
th = acos(z./r);
f2 = setfig('b2', [300 600]);
scatter(th, ph, 5, 'ok', 'filled');
daspect([1 1 1])
ylabel('$\varphi$');
xlabel('$\vartheta$');
ylim([0, 2*pi]);
xlim([0, pi]);
yticks([0, pi/2, pi, 3*pi/2, 2*pi]);
yticklabels({'$0$', '$\pi/2$', '$\pi$', '$3\pi/2$', '$2\pi$'});
xticks([0, pi/2, pi, 3*pi/2, 2*pi]);
xticklabels({'$0$', '$\pi/2$', '$\pi$', '$3\pi/2$', '$2\pi$'});
title(sprintf('Parametric space $\\Lambda$ with \nnode parameters, $N = %d$', length(x)))


% exportf(f0, [imagepath 'domain_3d_var_all.pdf']);
% exportf(f1, [imagepath 'domain_3d_var_bnd.pdf']);
% exportf(f2, [imagepath 'domain_3d_var_param.pdf']);