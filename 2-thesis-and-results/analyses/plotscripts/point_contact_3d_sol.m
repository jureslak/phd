prepare

casename = 'point_contact_3d_lastiter';

load([plotdatapath casename '.mat'])


fsize = [480 350];
f1 = setfig('a3', fsize); view([126.9000   25.2000]);

scatter3(lpos(:, 1), lpos(:, 2), lpos(:, 3), 5, sv, 'filled');
daspect([1 1 1])
xlim([-f*e, -e])
ylim([-f*e, -e])
zlim([-f*e, -e])
colormap viridis

h = colorbar;
set(get(h, 'title'), 'string', '$s_v$', 'Interpreter', 'latex')

exportf(f1, [imagepath casename '.pdf']);