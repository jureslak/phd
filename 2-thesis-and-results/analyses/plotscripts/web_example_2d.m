prepare

run([plotdatapath 'web_example_2d_data.m']);


x = positions(1, :);
y = positions(2, :);

if 0

scatter(x, y, 5, solution, 'filled');
title('$u(x, y)$');
xlabel('$x$')
ylabel('$y$')
zlabel('$u(x, y)$')
xlim([-1 1])
ylim([-1 1])
axis square
grid on
box on
xticks(-1:0.5:1)
yticks(-1:0.5:1)
colormap jet
colorbar

% exportf(gcf, 'adv-dif-2d-scat.png', '-m3')

end

if 1

setfig('a2', [350 350]);
    
tri = delaunay(x,y);
trisurf(tri, x, y, solution, 'EdgeColor', 'None');
lighting phong
shading interp
view([145.3000   40.4000])
xlabel('$x$')
ylabel('$y$')
zlabel('$u(x, y)$')
title(sprintf('$u(x, y)$, 2D, $N = %d$', length(x)))
xlim([-1 1])
ylim([-1 1])
axis square
grid on
box on
xticks(-1:0.5:1)
yticks(-1:0.5:1)
colormap viridis

exportf(gcf, [imagepath 'adv-dif-2d-surf.pdf'])

end