prepare

casename = 'point_contact_3d_profiles';
load([plotdatapath casename '.mat']);
nerr = 5;

x = data.x;
asv = data.asv;
profiles = data.profiles;
niter =  data.niter;

fsize = [800 350];

f2 = setfig('a4', fsize);

xz = data.xz;
asvz = data.asvz;

cf = 18;
plot(xz, asvz, '-k', 'LineWidth', 4, 'DisplayName', 'analytical');
cm = colormap('viridis');
xi = [50 70 90 350 300 450 400 420 485 425 435 450 465 480];
dy = 0.65e4 + 1e3*[0 0 0 0 0 0 25 15 0 5 10 12 14 17];
for i = 1:niter
   plot(xz, profiles{i}.svz, '-', 'Color', cm(cf*i, :), ...
        'DisplayName', sprintf('iteration %d', i-1));
   text(xz(xi(i)), profiles{i}.svz(xi(i))+dy(i), sprintf('%d', i-1));
end
xlim([-2.5*e, -e])
ylim([0, 2.5e5])
xlabel('$x = y = z$')
ylabel('$\sigma_{v}$')
title(sprintf('$\\gamma = %g$', e))    
legend({}, 'Location', 'WestOutside');

% exportf(f2, [imagepath casename '.pdf']);
