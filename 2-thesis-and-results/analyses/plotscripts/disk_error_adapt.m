prepare

casename = '001';

casenameA = ['disk_adaptive_' casename '_error'];
load([plotdatapath casenameA '.mat']);

f1 = setfig('a2', [380 300]);
leg = {};

niter = length(errors)-1;
for i = 1:nerr
    plot(0:niter, errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); 
    leg{end+1} = [errleg{i}(1:end-1) '^{(j)}$'];
end 
set(gca, 'yscale', 'log');
yticks(10.^(-4:0));
ylim([1e-4 5])
ylabel('errors');

yyaxis right
i = 5;
plot(0:niter, Ns, [MARKS{i} '-'], 'Color', COLORS(i, :));  leg{end+1} = '$N^{(j)}$';
set(gca, 'yscale', 'log');
ylim([1e2 5e6])
yticks(10.^(2:6))
ylabel('$N^{(j)}$');

title(sprintf('$\\gamma = 0.%s$', casename)) 
xlabel('iteration number $j$')
xticks(0:niter)
xlim([0-0.5, niter+0.5])

h = legend(leg, 'Location', 'N');
pos = get(h, 'Position');
pos(1) = pos(1) + 0.06;
pos(2) = pos(2) + 0.02;
set(h, 'Position', pos);

% exportf(f1, [imagepath 'disk_' casename '_adapt_err_nodes.pdf']);
