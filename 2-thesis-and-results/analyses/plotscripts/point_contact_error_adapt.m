prepare

casename = 'point_contact_3d_error';
load([plotdatapath casename '.mat']);
nerr = 5;

f1 = setfig('a2', [380 300]);
ll
leg = {};

niter = length(errors)-1;
for i = 1:nerr
    plot(Ns, errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); 
    leg{end+1} = errleg{i};
end 
set(gca, 'yscale', 'log');
yticks(10.^(-4:2));
ylim([5e-4 100])
ylabel('errors');

% yyaxis right
% i = 5;
% plot(0:niter, Ns, [MARKS{i} '-'], 'Color', COLORS(i, :));  leg{end+1} = '$N^{(j)}$';
% set(gca, 'yscale', 'log');
xlim([2e3 2e5])
xticks(10.^(2:6))
% ylabel('$N^{(j)}$');

title(sprintf('$\\gamma = %g$', e)) 
% xlabel('iteration number $j$')
% xticks(0:niter)
% xlim([0-0.5, niter+0.5])

h = legend(leg, 'Location', 'SW');
% pos = get(h, 'Position');
% pos(1) = pos(1) + 0.06;
% pos(2) = pos(2) + 0.02;
% set(h, 'Position', pos);

% exportf(f1, [imagepath casename '.pdf']);
