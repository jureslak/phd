prepare

casename = 'hertzian_adaptive_profiles';
load([plotdatapath casename '.mat']);


x = data.x;
profiles = data.profiles;

for i = 1:5
    
f1 = setfig(sprintf('ao%d', i), [400 250]);
leg = {};

h = plot(x/a, data.asxx/p0, '-k');
plot(data.profiles{i}.x/a, data.profiles{i}.sxx/p0, 'o-', ...
    'Color', COLORS(1, :));

xlim([-2 2])
ylim([-1.5 0.25])

ylabel('$\sigma_{xx}/p_0$');
xlabel('$x/a$')
legend('analiti\v{c}na', 'adaptivna', 'Location', 'SE');
title(sprintf('iteracija %d', i-1))

% exportf(f1, sprintf('%s%s%d_slo.pdf', imagepath, casename, i));

end

for i = 5:data.niter
    
f1 = setfig(sprintf('ao%d', i+1), [400 250]);
leg = {};

h = plot(x/a, data.asxx/p0, '-k');
plot(data.profiles{i}.x/a, data.profiles{i}.sxx/p0, 'o-', ...
    'Color', COLORS(1, :));

s = -1.005;
e = -0.995;
X = linspace(s*a, e*a, 1000);
Y = zeros(size(X));
[ASXX, ~, ~] = hertzian_analytical(X, Y, a, p0);
h.XData = X/a;
h.YData = ASXX/p0;
xlim([s, e])
ylim([-0.07, 0.02])
xticks(s:0.0025:e)


ylabel('$\sigma_{xx}/p_0$');
xlabel('$x/a$')
legend('analiti\v{c}na', 'adaptivna', 'Location', 'SW');
title(sprintf('iteracija %d', i-1))

exportf(f1, sprintf('%s%s%d_zoom_slo.pdf', imagepath, casename, i));

end
