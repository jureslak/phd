prepare

xx = linspace(-1, 1, 100);
yy = linspace(-1, 1, 100);
zz = linspace(-1, 1, 100);
[X, Y, Z] = meshgrid(xx, yy, zz);
% X = X(:); Y = Y(:);
invalid = X > 0 & Y > 0 & Z > 0;

fichera_geom;

den = 50;
cubes = cell(nr, 1);
for i = 1:nr
    r = cubes_ranges{i};
    xx = linspace(r(1, 1), r(2, 1), den);
    yy = linspace(r(1, 2), r(2, 2), den);
    zz = linspace(r(1, 3), r(2, 3), den);
    [X, Y, Z] = meshgrid(xx, yy, zz);
    cubes{i}.X = X;
    cubes{i}.Y = Y;
    cubes{i}.Z = Z;
    cubes{i}.V = fichera_anal(X, Y, Z);
end

f1 = setfig('a2', [400 300]); view([-223.9000   30.0000])

% plot(poly(:, 1), poly(:, 2), 'k-');
for i = 1:nr
    c = cubes{i};
    r = cubes_ranges{i};
    h = slice(c.X, c.Y, c.Z, c.V, r(:, 1), r(:, 2), r(:, 3)); 
    set(h, 'EdgeColor', 'none');
end
for i = 1:nl
    plot3(lines{i}.x, lines{i}.y, lines{i}.z, 'k-', 'LineWidth', 1);    
end
daspect([1 1 1]);
xlim([-1.35 1.35])
ylim([-1.35 1.35])
zlim([-1.35 1.35])
xticks(-1:1:1);
yticks(-1:1:1);
zticks(-1:1:1);
colormap viridis
colorbar
caxis([0, inf])
box on

text(-0.1, -0.1, 1.25, '$\Gamma_n$')
text(-0.25, 1.1, 0, '$\Gamma_n$')
text(1.2, -0.5, 0, '$\Gamma_n$')
text(0.8, 0.8, 0.25, '$\Gamma_d$')
text(0.7, 0.05, 0.6, '$\Gamma_d$')
text(0.25, 0.6, 0.6, '$\Gamma_d$')

title('$u_F$')

f2 = setfig('a4', [300 300]); view([-223.9000   30.0000])

% plot(poly(:, 1), poly(:, 2), 'k-');
for i = 1:nr
    c = cubes{i};
    r = cubes_ranges{i};
    c.V = 0.5*sqrt(c.X.^2 + c.Y.^2 + c.Z.^2).^-0.5;
    h = slice(c.X, c.Y, c.Z, c.V, r(:, 1), r(:, 2), r(:, 3)); 
    set(h, 'EdgeColor', 'none');
end
for i = 1:nl-3
    plot3(lines{i}.x, lines{i}.y, lines{i}.z, 'k-', 'LineWidth', 1);    
end
daspect([1 1 1]);
xlim([-1.35 1.35])
ylim([-1.35 1.35])
zlim([-1.35 1.35])
xticks(-1:1:1);
yticks(-1:1:1);
zticks(-1:1:1);
colormap viridis
% colorbar
caxis([0, inf])
box on
title('$\|\nabla u_F\|$');



exportf(f2, [imagepath 'fichera_grad.png'], '-m4');
pause(2)
exportf(f1, [imagepath 'fichera_anal.png'], '-m4');

