prepare

d = 2;
casename = sprintf('fill_trumpet_%dd', d);
data = [plotdatapath casename '.h5'];
h5info(data);

ns = str2num(h5readatt(data, '/conf', 'num.ns'));
ss = h5readatt(data, '/conf', 'approx.n');

times = h5read(data, '/times');

Ns = h5read(data, '/Ns');
err1 = h5read(data, '/err1');
err2 = h5read(data, '/err2');
erri = h5read(data, '/errinf');

Nb = mean(Ns, 2);

table = zeros(length(Nb), 8);

data = {Ns, err1, err2, erri};
for i = 1:4

eb = median(data{i}, 2);
em = min(data{i}, [], 2);
eM = max(data{i}, [], 2);

var = max(eb-em, eM-eb)./eb;
% var = std(data{i}, [], 2)./eb;

table(:, 2*i-1) = eb;
table(:, 2*i) = var;

if i == 4 % infty

sz = [400 300];
f1 = setfig('b1', sz);
plot(Ns, erri, '.', 'Color', 0.5*[1 1 1])
plts(1) = plot(Nb, eM, '--', 'Color', COLORS(1, :), 'DisplayName', 'maximum');
plts(2) = plot(Nb, eb, 'x-', 'Color', COLORS(1, :), 'DisplayName', 'median');
plts(3) = plot(Nb, em, '-.', 'Color', COLORS(1, :), 'DisplayName', 'minimum');
ll
ylabel('$e_\infty$')
if d == 2, f = 2; else, f = 9; end
plts(5) = plot(Nb, f.*Nb.^(-2/d), 'k--', 'DisplayName', '$O(N^{-2/d})$');
plts(4) = plot(NaN, NaN, '.', 'Color', 0.5*[1 1 1], 'DisplayName', 'errors');
legend(plts)
title(sprintf('$d = %d$, $n = %d$', d, ss))

if d == 2
xlim([3e3, 2e5])
ylim([1e-5, 1e-2])
else
xlim([2e3 2e5]);
ylim([2e-3 2e-1]);
end

end

end

fprintf('    $N$ & $\\Delta_r(N)$ & $e_1$ & $\\Delta_r(e_1)$ & $e_2$ & $\\Delta_r(e_2)$ & $e_\\infty$ & $\\Delta_r(e_\\infty)$ \\\\\n');
for i = 1:2:length(Nb)
    s = sprintf('% 7.0f & %.1e & %.1e & %.2f & %.1e & %.2f & %.1e & %.2f \\\\\\\\\n',...
        table(i, 1), table(i, 2), table(i, 3), table(i, 4), table(i, 5), table(i, 6), table(i, 7), table(i, 8));
    s = regexprep(s, '(\d\.\d)e-0(\d)', '$$1 \\\\cdot 10^{-$2}$');
    fprintf(s);
end

% exportf(f1, sprintf('%s/node_set_accuracy_%dd.pdf', imagepath, d));
