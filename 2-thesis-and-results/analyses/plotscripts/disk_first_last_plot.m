prepare

casename = '01';

casenameA = ['disk_adaptive_' casename '_first_last'];
load([plotdatapath casenameA '.mat']);

[X, Y] = ndgrid(xx, yy);

xd = linspace(0, R-eps, 500);
[Xd, Yd] = ndgrid(xd, xd);

fsize = [350 300];

pos.first = 'a1';
pos.last = 'a3';
pos2.first = 'a2';
pos2.last = 'a4';

tt.first = '$\log_{10}(e_{E, i}^{(0)})$';
tt.last = '$\log_{10}(e_{E, i}^{(J)})$';
tt2.first = '$\rho_i^{(0)}$';
tt2.last = '$\rho_i^{(J)}$';

clim.first = [-5, 0];
clim.last = [-9, -5];
clim2.first = [0, 0.001];
clim2.last = [0, 3.15];

figs = {};

for i = {'first', 'last'}
    i = i{1};
    c = data.(i);
    
    F = griddedInterpolant(X, Y, c.err);
    Vd = F(Xd, Yd);

    valid = Xd.^2 + Yd.^2 <= (R-eps).^2;
    Vd(~valid) = nan;

    
    figs{end+1} = setfig(pos.(i), fsize);
    contourf(Xd', Yd', log10(Vd)', 100, 'EdgeColor', 'none');
    daspect([1 1 1])
    xlim([0, R])
    ylim([0, R])
    xticks(0:0.1:0.5)
    yticks(0:0.1:0.5)
    colormap viridis
    colorbar
    caxis(clim.(i))
    title(tt.(i))
    
    N = length(c.pos)
    
    x = c.pos(:, 1);
    y = c.pos(:, 2);
    [~, d] = knnsearch(c.pos, c.pos, 'K', 2);
    d = d(:, 2);
    rho = -log10(d/max(d));
    max(rho)
    
    figs{end+1} = setfig(pos2.(i), fsize);
%     contourf(Xd', Yd', rho', 100, 'EdgeColor', 'none');
    scatter(x, y, 4, rho, 'filled');
    daspect([1 1 1])
    xlim([0, R])
    ylim([0, R])
    xticks(0:0.1:0.5)
    yticks(0:0.1:0.5)
    colormap viridis
    colorbar
    caxis(clim2.(i))
    title(tt2.(i))

end

figs{end+1} = setfig('ao8', fsize);
f = 10;
l = R/f;
l2 = l/f;
I = x < l & y > R-eps-l;
scatter(x(I), y(I), 4, rho(I), 's', 'filled');
daspect([1 1 1])
xlim([-l2, l])
ylim([R-eps-l, R-eps+l2])
colormap viridis
colorbar
% caxis(clim2.(i))
title(tt2.(i))

figs{end+1} = setfig('ao16', fsize);
l = R/f^2;
l2 = l/f;
I = x < l & y > R-eps-l;
scatter(x(I), y(I), 4, rho(I), 's', 'filled');
daspect([1 1 1])
xlim([-l2, l])
ylim([R-eps-l, R-eps+l2])
colormap viridis
colorbar
% caxis(clim2.(i))
title(tt2.(i))

% exportf(f1, [imagepath 'disk_' casename '_adapt_err_nodes.pdf']);

% pause(15)
% exportf(figs{1}, [imagepath 'disk_' casename '_first_err.png'], '-m4');
% pause(5)
% exportf(figs{2}, [imagepath 'disk_' casename '_first_den.png'], '-m4');
% pause(5)
% exportf(figs{3}, [imagepath 'disk_' casename '_last_err.png'], '-m4');
% pause(5)
% exportf(figs{4}, [imagepath 'disk_' casename '_last_den.png'], '-m4');
% pause(5)
% exportf(figs{5}, [imagepath 'disk_' casename '_last_den_zoom10.png'], '-m4');
% pause(5)
% exportf(figs{6}, [imagepath 'disk_' casename '_last_den_zoom100.png'], '-m4');
