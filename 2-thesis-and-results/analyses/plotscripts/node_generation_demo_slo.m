prepare

nmax = 10000;
pts = zeros(nmax, 2);
types = zeros(nmax, 1);
prev = zeros(nmax, 1);
num = 0;

zeta = 1-1e-10; % proximity tolerance factor

dr = 0.05;
r_target = @(x, y) dr + abs(x)*dr + abs(y)*dr;
% r_target = @(x, y) ((x-0.5)^2 + (y-0.5)^2)*0.05 + 0.005;
% r_target = @(x, y) 0.02;

r = @(th) 0.25*(3 + cos(3*th));
th = linspace(0, 2*pi, 500);

inside = @(x, y) norm([x y]) < r(atan2(y, x));

% x = 0;
% y = 0;
% f = (1+1e-10);
% while x < 1
%     num = num+1;
%     pts(num, :) = [x, y];
%     x = x + r_target(x, y)*f;
% end
% x = 1;
% while y < 1 - r_target(1, 1)
%     num = num+1;
%     pts(num, :) = [x, y];
%     y = y + r_target(x, y)*f;
% end
% y = 1;
% while x > 0
%     num = num+1;
%     pts(num, :) = [x, y];
%     x = x - r_target(x, y)*f;
% end
% x = 0;
% while y > 0
%     num = num+1;
%     pts(num, :) = [x, y];
%     y = y - r_target(x, y)*f;
% end
% pts(num, :) = [0, 0.012];
% num = num + 1;
num = num + 3;
thd = [0 2/3*pi  4/3*pi]';
pts(1:num, :) = [r(thd).*cos(thd) r(thd).*sin(thd)];


rng(645);

% export figures?
fsave = 1;

fsize = [350 330];
f1 = setfig('bo1', fsize);
plot(r(th).*cos(th), r(th).*sin(th), 'k', 'LineWidth', 0.8);
plot(-10, -10, 'ko', 'MarkerSize', 3, 'MarkerFaceColor', 'k');
title('semenske to\v{c}ke')
plot(pts(1:num, 1), pts(1:num, 2), 'o', 'MarkerSize', 4, 'Color', COLORS(2, :));
axis equal
xlabel('$x$')
ylabel('$y$')
xlim([-1 1.1])
ylim([-1 1.1])
xticks(-1:0.5:1)
axis square
legend('rob', 'raz\v{s}irjene to\v{c}ke', 'aktivne to\v{c}ke')
if fsave, exportf(f1, [imagepath 'nodes_initial_slo.pdf']), end

c = 0;
while num < nmax
    % select node from not expanded nodes
    c = c+1;
    if c > num, break, end
    types(c) = 1; %set node to expanded status
        
    x = pts(c, 1);
    y = pts(c, 2);
    h = r_target(x, y); % get desired spatial step
    N = 12; % number of new nodes
    sh = rand()*2*pi;
    phis = linspace(0+sh, 2*pi+sh, N+1);
    
    if c == 124
        f2 = setfig('bo2', fsize);
        plot(r(th).*cos(th), r(th).*sin(th), 'k', 'LineWidth', 0.8);
        axis equal
        xlabel('$x$')
        xticks(-1:0.5:1)
        ylabel('$y$')
        plot(pts(1:c, 1), pts(1:c, 2), 'ko', 'MarkerSize', 3, 'MarkerFaceColor', 'k');
        plot(pts(c, 1), pts(c, 2), 's', 'MarkerSize', 6, 'MarkerFaceColor', COLORS(1, :), 'MarkerEdgeColor', COLORS(1, :));
        plot(pts(c+1:num, 1), pts(c+1:num, 2), 'o', 'MarkerSize', 4, 'Color', COLORS(2, :));
        axis square
        title(sprintf('iteracija %d', c))
        xlim([-1 1.1])
        ylim([-1 1.1])
        if fsave, exportf(f2, [imagepath 'nodes_progress_slo.pdf']), end
    end
    
      oks = [];
    j = 1;
    for phi = phis
        xn = x + h*cos(phi);
        yn = y + h*sin(phi);

        if -1 <= xn && xn <= 1 && -1 <= yn && yn <= 1 && inside(xn, yn) && ...
                all((pts(1:num, 1) - xn).^2 + (pts(1:num, 2) - yn).^2 >= zeta*zeta*h*h)
            num = num + 1;
            pts(num, :) = [xn, yn];
            oks = [oks j];
            prev(num) = c;
        end
        j = j + 1;
    end
    
    if num == 124+length(oks)
        fc = setfig('b3', [650 350]);
        axis equal
        xlabel('$x$')
        ylabel('$y$')
        plot(pts(1:c-1, 1), pts(1:c-1, 2), 'ko', 'MarkerSize', 3, 'MarkerFaceColor', 'k');
        plot(pts(c, 1), pts(c, 2), 's', 'MarkerSize', 6, 'MarkerFaceColor', COLORS(1, :), 'MarkerEdgeColor', COLORS(1, :));
        plot(pts(c+1:num-length(oks), 1), pts(c+1:num-length(oks), 2), 'o', 'MarkerSize', 4, 'Color', COLORS(2, :));
        xlim([-0.32 0.12])
        ylim([0.2, 0.8])
        title('generiranje kandidatov');
        
        xn = x + h*cos(phis);
        yn = y + h*sin(phis);
        
        scatter(xn, yn, 15, 'Marker', '^', 'MarkerEdgeColor', COLORS(1, :));
        scatter(xn(oks), yn(oks), 50, 'Marker', '*', 'MarkerFaceColor', COLORS(2, :), 'MarkerEdgeColor', COLORS(2, :));
        h = legend('raz\v{s}irjene to\v{c}ke', 'trenutna to\v{c}ka', 'aktivne to\v{c}ke', 'kandidati', 'sprejeti kandidati', 'Location', 'EastOutside');
        set(h, 'Position', [0.5758 0.1566 0.2718 0.2640])
        if fsave, exportf(fc, [imagepath 'nodes_candidates_slo.pdf']), end
    end
end

pts = pts(1:num, :);
size(pts)
ffinal = setfig('bo10', fsize);
plot(r(th).*cos(th), r(th).*sin(th), 'k', 'LineWidth', 0.8);
axis equal
xlabel('$x$')
xticks(-1:0.5:1)
ylabel('$y$')
xlim([-1 1.1])
ylim([-1 1.1])
plot(pts(:, 1), pts(:, 2), 'ko', 'MarkerSize', 3, 'MarkerFaceColor', 'k');
% plot(pts(c, 1), pts(c, 2), 'bd', 'MarkerSize', 6, 'MarkerFaceColor', 'b');
% plot(pts(c+1:num, 1), pts(c+1:num, 2), 'ro', 'MarkerSize', 4);
title('kon\v{c}na razporeditev')
if fsave, exportf(ffinal, [imagepath 'nodes_final_slo.pdf']), end
% legend('expanded nodes', 'current node', 'queued nodes')
axis square
