prepare

run([plotdatapath 'sheppard_example_data_2d.m'])
n = length(pts);
x = pts(:, 1)';
y = pts(:, 2)';

val = uex(x, y)';
v = val';
z = zeros(size(v));

m = 200;
[X, Y] = meshgrid(deal(linspace(0, 1, m)));
X = X(:);
Y = Y(:);

k = 5;
R = 0.25;
V = sheppard(pts, val, [X Y], k, 2);
V2 = sheppard(pts, val, [X Y], n, 2);
V3 = sheppardm(pts, val, [X Y], R, 2);


X = reshape(X, [m m]);
Y = reshape(Y, [m m]);
V = reshape(V, [m m]);
V2 = reshape(V2, [m m]);
V3 = reshape(V3, [m m]);
N = griddata(x, y, val, X, Y, 'nearest');


m = 50;
[Xm, Ym] = meshgrid(deal(linspace(0, 1, m)));
Xm = Xm(:);
Ym = Ym(:);

Vm = sheppard(pts, val, [Xm Ym], k, 2);
V2m = sheppard(pts, val, [Xm Ym], n, 2);
V3m = sheppardm(pts, val, [Xm Ym], R, 2);

Xm = reshape(Xm, [m m]);
Ym = reshape(Ym, [m m]);
Vm = reshape(Vm, [m m]);
V2m = reshape(V2m, [m m]);
V3m = reshape(V3m, [m m]);
Nm = griddata(x, y, val, Xm, Ym, 'nearest');

SH = 0.2*Xm.*Ym.*(1-Xm).*(1-Ym);

fsize = [400 400];
f1 = setfig('b2', fsize); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, v, 20, 'kd')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.3);
surf(X, Y, N, 'EdgeColor', 'none');
surf(Xm, Ym, Nm, 'FaceAlpha', 0, 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
zlabel('')
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('Nearest neighbor interpolation');

f2 = setfig('b3', fsize); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, v, 20, 'kd')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.3);
surf(X, Y, V, 'EdgeColor', 'none');
surf(Xm, Ym, Vm+SH, 'FaceAlpha', 0, 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlabel('')
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title(sprintf('%d-Sheppard''s interpolant', k));

f3 = setfig('b4', fsize); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, v, 20, 'kd')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.3);
surf(X, Y, V2, 'EdgeColor', 'none');
surf(Xm, Ym, V2m+SH, 'FaceAlpha', 0, 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlabel('')
caxis([0.1395 1.2339])
zlim([0 1.25])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('Sheppard''s interpolant');

f4 = setfig('b1', fsize); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, v, 20, 'kd')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.3);
surf(X, Y, V3, 'EdgeColor', 'none');
surf(Xm, Ym, V3m+SH, 'FaceAlpha', 0, 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlabel('')
caxis([0.1395 1.2339])
zlim([0 1.25])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title(sprintf('Modified Sheppard''s interpolant, R = %g', R));


% exportf(f1, [imagepath 'nn-interp.png'], '-m4');
% exportf(f2, [imagepath 'k-sh-interp.png'], '-m4');
% exportf(f3, [imagepath 'sh-interp.png'], '-m4');
% exportf(f4, [imagepath 'mod-sh-interp.png'], '-m4');