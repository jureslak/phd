prepare

run([plotdatapath 'web_example_1d_data.m']);

setfig('a2', [400 300]);
x = positions(1, :);
scatter(x, solution, 5, solution, 'filled');
grid on
box on
xlabel('$x$')
ylabel('$u(x)$');
title(sprintf('$u(x)$, 1D, $N = %d$', length(x)))
xlim([-1.1 1.1])
ylim([-0.005 0.04])
yticks(0:0.01:0.04)
colormap viridis

exportf(gcf, [imagepath 'adv-dif-1d.pdf'], '-m3')