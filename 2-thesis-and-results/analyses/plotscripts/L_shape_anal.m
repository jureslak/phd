prepare

xx = linspace(-1, 1, 500);
yy = linspace(-1, 1, 500);
[X, Y] = meshgrid(xx, yy);
% X = X(:); Y = Y(:);
invalid = X > 0 & Y < 0;

omega = 3/2*pi;


poly = [
    0 0
    1 0
    1 1
    -1 1
    -1 -1
    0 -1
    0 0];

V = L_anal(X, Y, omega);
V(invalid) = nan;

f1 = setfig('a1', [320 300]);
contourf(X, Y, V, 100, 'EdgeColor', 'none');
plot(poly(:, 1), poly(:, 2), 'k-');

xlim([-1.35 1.35])
ylim([-1.35 1.35])
xticks(-1:0.5:1);
yticks(-1:0.5:1);
colormap viridis
colorbar
caxis([0, inf])
box on
daspect([1 1 1])
text(0, 1.15, '$\Gamma_n$')
text(-1.3, 0, '$\Gamma_n$')
text(-0.5, -1.18, '$\Gamma_n$')
text(1.05, 0.5, '$\Gamma_n$')
text(0.5, -0.18, '$\Gamma_d$')
text(0.1, -0.5, '$\Gamma_d$')
title('$u_L$')

f2 = setfig('a2', [350 250]); view([33.7000   21.2000])
G = L_grad_anal(X, Y, omega);
N = squeeze(sqrt(G(:,:,1).^2+G(:, :, 2).^2));
N(invalid) = nan;
surf(X, Y, N, 'EdgeColor', 'none');
colormap viridis
colorbar
zlim([-0.1 4])
zlabel('')
title('$\|\nabla u_L\|$')
% caxis([0.5 3])


% exportf(f2, [imagepath 'lshape_grad.png'], '-m4');
% pause(2)
% exportf(f1, [imagepath 'lshape_anal.png'], '-m4');


% contourf(X, Y, V, 100, 'EdgeColor', 'none');
% plot(poly(:, 1), poly(:, 2), 'k-');
% 
% xlim([-1.35 1.35])
% ylim([-1.35 1.35])
% xticks(-1:0.5:1);
% yticks(-1:0.5:1);

% caxis([0, inf])
% box on
% daspect([1 1 1])
% text(0, 1.15, '$\Gamma_n$')
% text(-1.25, 0, '$\Gamma_n$')
% text(-0.5, -1.18, '$\Gamma_n$')
% text(1.05, 0.5, '$\Gamma_n$')
% text(0.5, -0.18, '$\Gamma_d$')
% text(0.1, -0.5, '$\Gamma_d$')
