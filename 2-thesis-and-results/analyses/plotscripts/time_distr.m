prepare

casename = 'time_distr';
file = [plotdatapath casename '.h5'];

info = h5info(file);


nxs = str2double(strsplit(h5readatt(file, '/conf', 'case.nxs'), ','));
ths = 1;

nsize = length(nxs);
tsize = length(ths);
Ns = zeros(nsize, 1);
times = zeros(nsize, tsize, 7);
total = zeros(nsize, tsize);
err = zeros(nsize, tsize, 3);
l1 = 1; l2 = 2; linf = 3;

timesleg = {'domain', 'stencils', 'weights', 'assemble', 'decompose', 'solve'};

for i = 1:nsize
for j = 1:tsize
    try
    gname = info.Groups(i+1).Name;
    catch, break, end
    
    Ns(i) = h5readatt(file, gname, 'N');
    domain = h5readatt(file, [gname '/times'], 'domain-support');
    support = h5readatt(file, [gname '/times'], 'support-approx');
    approx = h5readatt(file, [gname '/times'], 'approx-matrix');
    matrix = h5readatt(file, [gname '/times'], 'matrix-compute');
    compute = h5readatt(file, [gname '/times'], 'compute-solve');
    solve = h5readatt(file, [gname '/times'], 'solve-postprocess');
    post = h5readatt(file, [gname '/times'], 'postprocess-end');
   
    times(i, j, :) = [domain support approx matrix compute solve post];
    total(i, j) = h5readatt(file, [gname '/times'], 'total');
    err(i, j, linf) = h5readatt(file, gname, 'err_inf');
    err(i, j, l1) = h5readatt(file, gname, 'err_1')/Ns(i);
    err(i, j, l2) = h5readatt(file, gname, 'err_2')/sqrt(Ns(i));
end
end

f2 = setfig('a2', [800 300]);
leg = cell(size(timesleg));
for j = 1:6
    k = polyfit(log(Ns), log(times(:, 1, j)), 1);
    plot(Ns, times(:, 1, j), ['-' MARKS{j}], 'Color', COLORS(j, :));
    leg{j} = [timesleg{j} sprintf(' $k = %.2f$', k(1))];    
end
plot(Ns, total(:, 1), '-xk'); leg{end+1} = 'total time';
ylim([1e-3 2e2])
yticks(10.^(-3:2))

ll
ylabel('execution time [s]')
xlim([3e3 1.2e6])
legend(leg, 'Location', 'EastOutside');

f4 = setfig('a4', [330 300]);
for j = 1:6
    plot(Ns, 100*times(:, 1, j) ./ total(:, 1),  ['-' MARKS{j}], 'Color', COLORS(j, :));
end
xlabel('$N$')
xticks(10.^(3:6))
xlim([3e3 1.2e6])
ylim([0 55])
ylabel('\% of total time')
set(gca, 'xscale', 'log')
% legend(timesleg, 'Location', 'EastOutside');

f3 = setfig('a3', [480 300]);
times = squeeze(times(:, threadidx, 1:6));
times = 100*times ./ sum(times, 2);

area(Ns, times, 'FaceColor','flat');
C = colormap('viridis');
n = length(C);
nt = size(times, 2);


xlabel('$N$')
xlim([3e3 1.2e6])
xticks(10.^(3:6))
ylim([-5 105])
ylabel('\% of total time')
set(gca, 'xscale', 'log')
legend(timesleg, 'Location', 'EastOutside');

% exportf(f2, [imagepath 'time_distr.pdf']);
% exportf(f3, [imagepath 'time_distr_stacked.pdf']);
% exportf(f4, [imagepath 'time_distr_ratio.pdf']);