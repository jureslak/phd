prepare

load([plotdatapath 'hertzian_manual_domain_density.mat'])
a = b;
% assert(abs(a-b) < eps)

xx = xi';
yy = yi';

bb = 3;
I = -bb < xx/a & xx/a < bb & -bb < yy/a;
xxf = xx(I);
yyf = yy(I);

% f3=setfig('b3');

[X, Y] = meshgrid(linspace(min(xxf), max(xxf), 100), linspace(min(yyf), max(yyf), 100));
RHO = griddata(xx, yy, avgd, X, Y);

fs = [400 200];
f2 = setfig('a2', fs);
contourf(X/a, Y/a, -log10(RHO), 100, 'EdgeColor', 'none')
daspect([1 1 1])
colormap viridis
colorbar
caxis([3, 5])
xlabel('$x/a$')
ylabel('$y/a$')
xlim([-bb, bb])
ylim([-bb, 0.01])
set(gca,'TickDir','out');
title(sprintf('$\\rho_{\\mathrm{manual}}$, $\\max = %.2f$', max(-log10(avgd))))
xticks(-bb:bb);
% exportf(f2, [imagepath 'hertzian_manual_density.png'], '-m4');

load([plotdatapath 'hertzian_adaptive_density.mat'])

[X, Y] = meshgrid(data.x, data.y);

f4 = setfig('a4', fs);
contourf(X/a, Y/a, data.rho, 100, 'EdgeColor', 'none');
daspect([1 1 1])
xlabel('$x/a$')
ylabel('$y/a$')
xlim([-bb, bb])
ylim([-bb, 0.01])
caxis([3, 5.5])
colormap viridis
colorbar
set(gca,'TickDir','out');
title(sprintf('$\\rho_{\\mathrm{adaptive}}$, $\\max = %.2f$', data.maxd))
xticks(-bb:bb);
% exportf(f4, [imagepath 'hertzian_adaptive_density.png'], '-m4');


f3 = setfig('a3', [400 440]);
histogram('BinEdges', edges, 'BinCounts', counts, 'FaceColor', 'w', 'FaceAlpha', 1);
xlim([-0.2, 7])
xticks(0:7)
set(gca,'TickDir','out');
xlabel('$\rho_{\mathrm{adaptive}}$')
ylabel('count')
% exportf(f3, [imagepath 'hertzian_adaptive_hist.pdf']);

