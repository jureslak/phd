prepare

casename = 'gauss_scaled_shape';
data = [plotdatapath casename '.h5'];

hs = h5read(data, '/hs');
% gns = h5read(data, '/gau_no_scale_lu');
% gnsj = h5read(data, '/gau_no_scale_svd');
gnsc = h5read(data, '/gau_no_scale_chol');

kappans = h5read(data, '/cond_ns');
kappas = h5read(data, '/cond_s');

gs = h5read(data, '/gau_scaled');
eps = h5readatt(data, '/', 'eps');
eps0 = h5readatt(data, '/', 'eps0');


% u00 = -0.5 + exp(-183/50);
lapu00 = 24.11008446910158;
% u4 = -2782559/625*exp(-183/50);

% err = @(eps, h) abs(2*eps.^4.*h.^2*u00 + eps.^2.*h.^2*lapu00 + 1/12*h.^2*u4);
err = @(eps, h) 0.0833333*abs(-267.504 + 289.321*eps.^2 - 65.2388*eps.^4).*h.^2;
kappa = @(chi) 6.37377./(chi).^4 - 7.2563./(chi).^2 + 18.936 - 25.863*(chi).^2 + 33.4851*(chi).^4;
hst = logspace(-4, -1, 1000)';

fsize = [400 300];

f1 = setfig('b1', fsize);
plot(hst, err(eps, hst), 'k--', 'DisplayName', sprintf('theoretical, $\\varepsilon = %.3f$', eps))
plot(hst, err(eps0./hst, hst), 'k-', 'DisplayName', sprintf('theoretical, $\\varepsilon = \\varepsilon_0/h$, $\\varepsilon_0 = %g$', eps0))
% plot(hs, abs(gns-lapu00), 'DisplayName', 'no scale, LU')
plot(hs, abs(gnsc-lapu00), 'DisplayName', sprintf('computed, $\\varepsilon = %.3f$', eps), 'Color', COLORS(1, :))
% plot(hs, abs(gnsj-lapu00), 'DisplayName', 'no scale, SVD')
plot(hs, abs(gs-lapu00),'DisplayName', sprintf('computed, $\\varepsilon = \\varepsilon_0/h$, $\\varepsilon_0 = %g$', eps0), 'Color', COLORS(2, :))
ll
xlabel('$h$')
xlim([5e-4, 2e-2])
xticks([5e-4, 1e-3, 2e-3, 5e-3, 1e-2, 2e-2, 5e-2])
legend({})

f2 = setfig('b3', fsize);
plot(hst, kappa(eps*hst), 'k--', 'DisplayName', sprintf('theoretical, $\\varepsilon = %.3f$', eps))
plot(hst, kappa(eps0*ones(size(hst))), 'k-', 'DisplayName', sprintf('theoretical, $\\varepsilon = \\varepsilon_0/h$, $\\varepsilon_0 = %g$', eps0))
% plot(hs, abs(gns-lapu00), 'DisplayName', 'no scale, LU')
% multiply by some offset to graphs don't cover up
plot(hs, 1.2*kappans, 'DisplayName', sprintf('computed, $\\varepsilon = %.3f$', eps), 'Color', COLORS(1, :))
% plot(hs, abs(gnsj-lapu00), 'DisplayName', 'no scale, SVD')
plot(hs, 1.2*kappas, 'DisplayName', sprintf('computed, $\\varepsilon = \\varepsilon_0/h$, $\\varepsilon_0 = %g$', eps0), 'Color', COLORS(2, :))
ll
xlabel('$h$')
ylabel('condition number')
xlim([5e-4, 2e-2])
xticks([5e-4, 1e-3, 2e-3, 5e-3, 1e-2, 2e-2, 5e-2])
legend({})

% exportf(f1, [imagepath casename '_error.pdf'])
% exportf(f2, [imagepath casename '_cond.pdf'])
