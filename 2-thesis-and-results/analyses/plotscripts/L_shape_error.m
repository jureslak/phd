prepare


casenameU = 'L_shape_uniform_error';
casenameA = 'L_shape_adaptive_error';

A = load([plotdatapath casenameA '.mat']);
U = load([plotdatapath casenameU '.mat']);


f1 = setfig('a2', [350 300]);
ll;
xlim([2e2 1e6]);
ylim([1e-5 1e-1]);
leg = {};
for i = 1:A.nerr
    plot(A.Ns, A.errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); leg{end+1} = A.errleg{i};
%     k = polyfit(log(A.Ns), log(A.errors(:, i)), 1);
%     plot(A.Ns, exp(k(2)).*A.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end 
plot(A.Ns, 3.5*A.Ns.^-1, 'k--'); leg{end+1} = '$O(h^2)$';
legend(leg, 'Location', 'SW');
title('Adaptive');
xticks(10.^(3:6))
yticks(10.^(-6:-1));

f2 = setfig('a4', [350 300]);
ll;
xlim([2e2 1e6]);
ylim([1e-5 1e-1]);
leg = {};
for i = 1:U.nerr
    plot(U.Ns, U.errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); leg{end+1} = U.errleg{i};
    
%     k = polyfit(log(U.Ns), log(U.errors(:, i)), 1);
%     plot(U.Ns, exp(k(2)).*U.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end
U.Ns(end) = U.Ns(end)*1.1;
plot(U.Ns, 0.6*U.Ns.^-0.5, 'k--'); leg{end+1} = '$O(h)$';
% plot(U.Ns, 20*U.Ns.^-1, 'k--'); leg{end+1} = '$O(h^2)$';
legend(leg, 'Location', 'SW');
title('Uniform');
xticks(10.^(3:6))
yticks(10.^(-6:-1));

% exportf(f1, [imagepath casenameA '.pdf'])
% exportf(f2, [imagepath casenameU '.pdf'])
