prepare

casename = '01';

casenameA = ['disk_adaptive_' casename '_error'];
A = load([plotdatapath casenameA '.mat']);

casenameU = ['disk_uniform_' casename '_error'];
U = load([plotdatapath casenameU '.mat']);


f1 = setfig('a2', [380 300]);
ll;
if strcmp(casename, '01')
xlim([10 5e5]);
else
xlim([2e2 1e6]);
end
ylim([1e-4 5]);
leg = {};

for i = 1:U.nerr
    plot(U.Ns, U.errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :));
    leg{end+1} = [U.errleg{i}(1:3) '{' U.errleg{i}(4:end-1) ', u}$'];
end

for i = 1:A.nerr
    plot(A.Ns, A.errors(:, i), [MARKS{i} '-.'], 'Color', COLORS(i, :)); 
    leg{end+1} = [A.errleg{i}(1:3) '{' A.errleg{i}(4:end-1) ', a}$'];
end 

xticks(10.^(2:6))
yticks(10.^(-4:0));
title(sprintf('$\\gamma = 0.%s$', casename)) 

U.Ns = U.Ns(end-25:end);
plot(U.Ns, 0.3*U.Ns.^(-1/2), 'k--'); leg{end+1} = sprintf('$O(h)$');

legend(leg, 'Location', 'SW')

% exportf(f1, [imagepath 'disk_' casename '_errors.pdf']);
