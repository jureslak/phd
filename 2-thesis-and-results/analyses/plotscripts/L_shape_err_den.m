prepare


casename = 'L_shape_adaptive_err_den';
load([plotdatapath casename '.mat']);

[X, Y] = meshgrid(xx, yy);
X = X(:); Y = Y(:);
valid = ~(X > 0 & Y < 0);
X = X(valid); Y = Y(valid);
P = [X Y];

ng = length(adapt);

errlo = [0.04 0.04 0.005 0.005 0.005 0.005];
errhi = [0.3 0.16 0.06 0.04 0.02 0.015];

figs = cell(ng, 3);

s = 5;
for g = 1:ng
    E = adapt{g}.E;
    Eh = adapt{g}.Eh;
    D = adapt{g}.D;
    
    figs{g, 1} = setfig('bo1', [250 200]);
    scatter(X, Y, s, log10(E), 's', 'filled')
%     [min(log10(E)) max(log10(E))]
    axis equal
    colormap viridis
    colorbar
    caxis([-6 -1])
    xlim([-1 1])
    ylim([-1 1])
    box on
    title(['$\log_{10} e_i^{(' num2str(g-1) ')}$']);
    
    figs{g, 2} = setfig('bo2', [250 200]);
    scatter(X, Y, s, log10(Eh), 's', 'filled')
%     [min(log10(Eh)) max(log10(Eh))]
    axis equal
    
    colormap viridis;
    caxis([-2.25 -0.3])
    colorbar
    
    xlim([-1 1])
    ylim([-1 1])
    box on
    title(['$\hat{e}_i^{(' num2str(g-1) ')}$']);
    
%     text(140, 70, sprintf('iter %d', g), 'Units', 'points')
%     text(125, 55, sprintf('$N = %d$', Ns(g)), 'Units', 'points')

    figs{g, 3} = setfig('bo3', [250 200]);
    [min(D) max(D)]
    scatter(X, Y, s, D, 's', 'filled')
    axis equal
    colormap viridis
    colorbar
    caxis([0, 1.15])
    xlim([-1 1])
    ylim([-1 1])
    title(['$\rho^{(' num2str(g-1) ')}$']);
end

if 0
names = {'err', 'ind', 'den'};
for g = 1:ng
    pause(3)
    for j = 1:3
        exportf(figs{g, j}, sprintf('%s%s_%s_%d.pdf', imagepath, casename, names{j}, g));
        pause(1)
    end
end
end