prepare

% 3*(1-x)*(1-x)*std::exp(-x*x) + 3*std::exp(-4*(x-1)*(x-1))
f = @(x) 3*(1-x).^2.*exp(-x.^2) + 3*exp(-4*(x-1).^2);
dx = 0.001;
xd = -3:dx:3;
fx = f(xd);
dfx = diff(fx)/dx;
ddfx = diff(dfx)/dx;

filename = 'adaptive_demo.h5';
datafile = [plotdatapath filename];

xd = h5read(datafile, '/pos_dense');
[xd, Id] = sort(xd);

figs = {'bo1', 'bo6', 'bo11', 'bo16', 'bo5', 'bo6', 'bo7', 'bo8', 'bo9'};

iter = 0;
while 1

folder = sprintf('/iter%02d', iter);
try
x = h5read(datafile, [folder '/pos']);
catch, break; end
[x, I] = sort(x);
N = length(x);
% fprintf('N = %d\n', N);
fx_c = h5read(datafile, [folder '/correct']);
fx_c = fx_c(I);
fx_a = h5read(datafile, [folder '/approx']);
fx_a = fx_a(I);

[~, D] = knnsearch(x, x, 'k', 2);
width = D(:, 2);

fx_cd = h5read(datafile, [folder '/correct_dense']);
fx_cd = fx_cd(Id);
fx_ad = h5read(datafile, [folder '/approx_dense']);
fx_ad = fx_ad(Id);

if iter == 3, w = 315; else, w = 350; end

f1 = setfig(figs{iter+1}, [w, 280]);
% if iter == 3, fx_c = NaN(size(fx_c)); fx_a = NaN(size(fx_a)); end
% pfx = plot(x, fx_c, 'xk');
pfxc = plot(xd, fx_cd, '-k');
pfh = plot(x, fx_a, 'o', 'Color', COLORS(1, :));
pfhc = plot(xd, fx_ad, '-', 'Color', COLORS(1, :));
ylim([-0.75, 6])
yticks(0:1:6)
xticks(-3:1:3)
ylabel(sprintf('$g$, $g^{(%d)}$', iter));
title(sprintf('Iteration %d, $e^{(%d)} = %.3f$', iter, iter, mean(abs(fx_cd-fx_ad))));

error = h5read(datafile, [folder '/error']); error = error(I);

ref = h5readatt(datafile, folder, 'ref');
deref = h5readatt(datafile, folder, 'deref');
same = h5readatt(datafile, folder, 'same');
ref_limit = h5readatt(datafile, folder, 'ref_limit');
deref_limit = h5readatt(datafile, folder, 'deref_limit');
fprintf('%d & %d & %.4f & %.4f & %d & %d & %d & %d & %d \\\\ \n', iter, N, mean(error),  mean(abs(fx_cd-fx_ad)), ref, ref_limit, same, deref, deref_limit);

if iter ~= 3
yyaxis right
ylabel('$f_i$')
yt = -4:1:4;
yticks(yt);
ylv = yt+1;
ylv(ylv<1) = -1./((ylv(ylv<1)-1)-1);
yl = cell(size(yt));
for i = 1:length(yt), yl{i} = sprintf('%.2f', ylv(i)); end
yticklabels(yl)
end


factor = h5read(datafile, [folder '/factor']); factor = factor(I);
factor = factor-1;
If = factor < 0;
factor(If) = -1./(factor(If)+1)+1;
% fprintf('%.6f\n', mean(factor));
% bar(x, error, 'FaceAlpha', 0.9, 'EdgeColor', 'none')

g = 0.7;
vd = interp1(x, factor, xd, 'nearest');
if iter == 3
vd = nan*vd;
end
plot(xd, vd, '-', 'Color', COLORS(2, :));
% bar(x, factor, , 'EdgeColor', 'none', 'FaceAlpha', 0.5);

if iter ~= 3
plot(-3:1:3, zeros(size(-3:1:3)), '--', 'Color', [g g g]);
ylim([-4.5 4.5])
ax = gca;
ax.YAxis(2).Color = COLORS(2, :);
end
xlim([-3, 3])

new_dx = h5read(datafile, [folder '/new_dx']); new_dx = new_dx(I);

if iter == 3
legend('$g$', '$g^{(j)}_i$', '$g^{(j)}$', '$f_i$', 'Location', 'NE')
end


% pfx.ZData = ones(size(pfx.XData));
if iter == 3, f = 2; else, f = 1; end
pfxc.ZData = f*ones(size(pfxc.XData));
pfh.ZData = ones(size(pfh.XData));
pfhc.ZData = ones(size(pfhc.XData));
set(gca, 'SortMethod', 'depth');


% exportf(f1, [imagepath sprintf('adaptive_demo_iter%d.pdf', iter)]);

iter = iter+1;

end

f2 = setfig('b3', [500 200]);

plot(xd(2:end-1), abs(ddfx), 'k-')
ylabel('$|g''''(x)|$')
% plot(xd(2:end), dfx)
[~, D] = knnsearch(x, x, 'K', 2); d = D(:, 2);
yyaxis right
plot(x, 1./d, 'x-', 'Color', COLORS(1, :));
ylabel('$1/h_i^{(3)}$')
ylim([4 51])

% exportf(f2, [imagepath 'adaptive_demo_final_distr.pdf']);