prepare

% initialize random seed
load([plotdatapath 'node_set_fill_sep_dist.mat'], 'S');
rng(S);

r = @(fi) 2 + cos(3*fi);
phi = linspace(0, 2*pi, 500);
x = r(phi).*cos(phi);
y = r(phi).*sin(phi);

inside = @(X, Y) inpolygon(X, Y, x, y);

phiI = [0 0.2 0.4 0.6 0.87 1.32 1.55 1.75 1.95 2.15 2.35 2.55 2.7...
    2.9 3.25 3.45 3.63 3.8 3.95 4.1 4.25 4.45 4.63 4.8 5 5.5 5.7...
    5.87 6.05]';
xI = r(phiI).*cos(phiI);
yI = r(phiI).*sin(phiI);


n = 150;
rad = @(x, y) 2*(0.25 + 0.05*sin(x));
pts = PNP([-5 -5 5 5], rad, 15, 1000, inside, [xI yI]);
X = pts(:, 1);
Y = pts(:, 2);

[mind, i, j] = closest_pair([X, Y]);

[XC, YC] = meshgrid(deal(linspace(-3, 3, 200)));
I = inpolygon(XC, YC, x, y);
XC(~I) = [];
YC(~I) = [];
XC = XC(:);
YC = YC(:);
[I, D] = knnsearch([X Y], [XC YC], 'K', 1);
[d, k] = max(D);

xc = XC(k);
yc = YC(k);
xs = xc + d*cos(phi);
ys = yc + d*sin(phi);


f1 = setfig('a2', [350 350]);
plot(x, y, '-k');
scatter(X, Y, 'ok', 'filled')
plot([X(i), X(j)], [Y(i), Y(j)], '-', 'LineWidth', 2, 'Color', BLUE)
plot(xs, ys, '-', 'Color', ORANGE);
phi = [0, pi]-0.2;
xs = xc + d*cos(phi);
ys = yc + d*sin(phi);
plot(xs, ys, '-', 'Color', ORANGE);
%axis off
grid off
daspect([1 1 1])
xlim([-2.5, 3.5])
xticks(-2:3)
ylim([-3, 3])
text(0.25, -2, '$\Omega$', 'fontSize', 12)
text(X(i), Y(i)+0.2, '$s_X$', 'fontSize', 12)
text(xc-0.25, yc+0.2, '$h_{X\!,\,\Omega}$', 'fontSize', 12)
text(1.2, -2, sprintf('$\\gamma_{X\\!,\\,\\Omega} = %.2f$', d/mind), 'fontSize', 12)

% exportf(f1, [imagepath 'node_set_fill_sep_dist_demo.pdf']);
