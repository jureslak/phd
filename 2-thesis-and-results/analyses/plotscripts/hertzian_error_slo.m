prepare

casename = 'hertzian_adaptive_error';
load([plotdatapath casename '.mat']);

f1 = setfig('a2', [250 300]);
leg = {};

niter = length(errors)-1;
for i = 1:nerr
    plot(0:niter, errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); 
    leg{end+1} = [errleg{i}(1:end-1) '^{(j)}$'];
end 
set(gca, 'yscale', 'log');
yticks(10.^(-4:1));
ylim([5e-4 10])
ylabel('napaka');
xlabel('\v{s}t. iteracije $j$')
xticks(0:niter)
xlim([0-0.5, niter+0.5])
legend(leg);

f2 = setfig('a4', [250 300]);
i = 1;
plot(0:niter, Ns, [MARKS{i} '-'], 'Color', COLORS(i, :));  leg{end+1} = '$N^{(j)}$';
set(gca, 'yscale', 'log');
ylim([3e3 1e6])
yticks(10.^(2:6))
ylabel('$N^{(j)}$');
xlabel('\v{s}t. iteracije $j$')
xticks(0:niter)
xlim([0-0.5, niter+0.5])

f3 = setfig('ao8', [250 300]);
leg = {};
for i = 1:nerr
    plot(Ns, errors(:, i), [MARKS{i} '-'], 'Color', COLORS(i, :)); 
    leg{end+1} = [errleg{i}(1:end-1) '^{(j)}$'];
end
ll
ylabel('napaka');
yticks(10.^(-4:1));
xlim([3e3, 1e6])
h = legend(leg);
pos = get(h, 'Position');
pos(2) = pos(2) - 0.25;
set(h, 'Position', pos);

% exportf(f1, [imagepath casename '_iter_slo.pdf'])
% exportf(f2, [imagepath casename '_only_N_slo.pdf'])
% exportf(f3, [imagepath casename '_vs_N_slo.pdf'])