prepare


run([plotdatapath 'sheppard_example_data_2d.m'])
n = length(pts);
x = pts(:, 1)';
y = pts(:, 2)';

val = uex(x, y);
v = val;
z = zeros(size(val));

data = [plotdatapath 'rbf.h5'];
xx = h5read(data, '/x');
yy = h5read(data, '/y');

[X, Y] = ndgrid(xx, yy);

gau = h5read(data, '/gau');
imq = h5read(data, '/imq');
mq = h5read(data, '/mq');
ph = h5read(data, '/ph');

f1 = setfig('b1', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, gau, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('Guassian, $\varepsilon = 4$')

f2 = setfig('b3', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, imq, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('Inverse multiquadric, $\beta = 1/2,\, \varepsilon = 4$')

f3 = setfig('b2', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, mq, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('Multiquadric, $\beta = 1/2,\, \varepsilon = 4$')

f4 = setfig('b4', [400 400]); view(3)
scatter3(x, y, z, 15, 'ko', 'filled')
scatter3(x, y, val, 15, 'ko')
plot3([x; x], [y; y], [z; v], 'color', 0.5*[1 1 1], 'LineWidth', 0.5);
surf(X, Y, ph, 'EdgeColor', [0 0 0], 'EdgeAlpha', 0.3);
colormap viridis
axis equal
xlim([0 1])
ylim([0 1])
zlim([0 1.25])
caxis([0.1395 1.2339])
yticks([0 0.25 0.5 0.75 1])
xticks([0 0.25 0.5 0.75 1])
zticks([0 0.25 0.5 0.75 1])
title('Polyharmonic, $\beta = 3$')

% exportf(f1, [imagepath 'gau.png'], '-m4')
% exportf(f2, [imagepath 'imq.png'], '-m4')
% exportf(f3, [imagepath 'mq.png'], '-m4')
% exportf(f4, [imagepath 'ph.png'], '-m4')
