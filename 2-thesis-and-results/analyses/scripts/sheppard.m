function newval = sheppard(pos, val, newpos, k, p)
% Performs Modified Sheppard's interpolation.
%
% Interpolates function given by (pos, val) to
% new positions newpos.
%
% pos = N x d matrix of d-dimensional points
% val = N x 1 matrix of scalar values
% newpos = M x d matrix of d-dimensional points
% k = number of nearest neighbours used.
% p = power (2 by default)
%
% Time complexity: O(k M 2^d \log N)
if nargin < 5, p = 2; end
newval = zeros(size(newpos, 1), 1);
[I, d] = knnsearch(pos, newpos, 'k', k);
w = 1./d.^p;
w = w ./ sum(w, 2);
w(isnan(w)) = 1;

for i = 1:k
    newval = newval + val(I(:, i), :).*w(:, i);
end

end
