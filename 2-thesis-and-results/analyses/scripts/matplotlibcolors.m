% matplotlib colors in order
BLUE = [31, 119, 180]/256;  % #1f77b4
ORANGE = [255, 127, 14]/256; % #ff7f0e
GREEN = [44, 160, 44]/256; % #2ca02c
RED = [214, 39, 40]/256; % #d62728
VIOLET = [148, 103, 189]/256; % #9467bd
BROWN = [140, 86, 75]/256; % #8c564b
PINK = [227, 119, 194]/256; % #e377c2
GREY = [127, 127, 127]/256; % #7f7f7f
YELLOW = [188, 189, 34]/256; % #bcbd22
CYAN = [23, 190, 207]/256; % #17becf

COLORS = [BLUE; ORANGE; GREEN; RED; VIOLET; BROWN; PINK; GREY; YELLOW; CYAN];
