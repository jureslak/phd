% !TeX spellcheck = en_US
\documentclass{beamer}

\usefonttheme[onlymath]{serif}

\usepackage[slovene]{babel}    % slovenian language and hyphenation
\usepackage[T1]{fontenc}       % make čšž work on output
\usepackage[utf8]{inputenc}
\usepackage{url}               % \url and \href for links
\usepackage{units}
\usepackage{bibentry}

\newtheorem{izrek}{Izrek}
\newtheorem{posledica}{Posledica}

\theoremstyle{definition}
\newtheorem{definicija}{Definicija}
\newtheorem{opomba}{Opomba}
\newtheorem{zgled}{Zgled}

% basic sets
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\renewcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\T}{\ensuremath{\mathsf{T}}}

\newcommand{\X}{\mathcal{X}}

% greek letters
\let\oldphi\phi
\let\oldtheta\theta
\newcommand{\eps}{\varepsilon}
\renewcommand{\phi}{\varphi}
\renewcommand{\theta}{\vartheta}

% vektorska analiza
\newcommand{\grad}{\operatorname{grad}}
\newcommand{\rot}{\operatorname{rot}}
\renewcommand{\div}{\operatorname{div}}
\newcommand{\dpar}[2]{\frac{\partial #1}{\partial #2}}

\renewcommand{\b}{\boldsymbol}

\usepackage{minted}

\usetheme[outer/progressbar=foot]{metropolis}

\institute{
  PhD topic presentation \\
  
  Numerical analysis seminar, 24.\ 4.\ 2019}
\title{Adaptive RBF-FD method \\ (Adaptivna RBF-FD metoda)}
\date{}
\author{Jure Slak}
\hypersetup{pdftitle={Adaptive RBF-FD method},
pdfauthor={Jure Slak}, pdfsubject={}, pdfkeywords={}}  % setup pdf metadata

\newcommand{\ijs}{\hfill JSI \raisebox{-4pt}{\includegraphics[height=16pt]{ijs_logo_inverse.pdf}}}

% \pagestyle{empty}              % vse strani prazne
% \setlength{\parindent}{0pt}    % zamik vsakega odstavka
% \setlength{\parskip}{10pt}     % prazen prostor po odstavku
\setlength{\overfullrule}{0pt}  % oznaci predlogo vrstico z veliko črnine
%\usepackage{libertine}

\definecolor{Purple}{HTML}{911146}
\definecolor{Orange}{HTML}{CF4A30}
\definecolor{Dark}{HTML}{333333}
\setbeamercolor{frametitle}{bg=Dark}

\setbeamercolor{alerted text}{fg=Orange}
\setbeamercolor{normal text}{fg=Dark}

\metroset{block=fill}

\renewcommand{\b}{\boldsymbol}
\renewcommand{\x}{\b{x}}
\newcommand{\s}{\b{s}}
\newcommand{\p}{\b{p}}
\newcommand{\f}{\b{f}}
\newcommand{\g}{\b{g}}
\renewcommand{\t}{\b{t}}
\renewcommand{\u}{\b{u}}
\renewcommand{\L}{\mathcal{L}}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\begin{frame}{Overview \ijs}
  \begin{enumerate}
    \setlength{\parskip}{10pt}
    \item History and motivation
    \item RBF-FD method
    \begin{itemize}
      \item Approximation properties
    \end{itemize}
    \item Fully automatic adaptivity    
    \begin{itemize}
      \item Node placing
      \item Refinement
      \item Error indicators
    \end{itemize}
    \item Technical aspects
  \end{enumerate}
\end{frame}


\begin{frame}{History and motivation \ijs}
\begin{table}
  \renewcommand*{\arraystretch}{1.7}
\begin{tabular}{c||c|c}
  & mesh/grid-based & meshless \\ \hline \hline 
  strong-form & FDM & FPM, RBF-FD, \ldots \\ \hline 
  weak form & FEM, IGA & \hspace{-19pt} BEM \hspace{10pt} EFG, MLPG, \ldots \\   
\end{tabular}
\end{table}
\begin{figure}
  \includegraphics[width=0.35\linewidth]{fig/domain-mesh.png}
  \includegraphics[width=0.35\linewidth]{fig/domain-meshless.png}
\end{figure}
\end{frame}


\begin{frame}{RBF-FD method: RBFs \ijs}
  RBF: function of form $\phi(\|p\|)$
  \begin{figure}
    \includegraphics[width=0.65\linewidth]{fig/rbfs.png}
%    \includegraphics[width=0.45\linewidth]{fig/domain_theoretical.pdf}
  \end{figure}
  [Bu03] Buhmann, Martin D. Radial basis functions: theory and implementations. Vol. 12. \emph{Cambridge university press}, 2003.
\end{frame}

\begin{frame}{RBF-FD method: operator approximations \ijs}
  Strong form approximations:
  \[
    (\L u)(p) \approx \sum_{i=1}^n w_i u(p_i)
  \]
  Enforce exactness for a class of functions:
  \begin{equation*}
  \renewcommand*{\arraycolsep}{1pt}
  \begin{bmatrix}
  \phi(\|p_1 - p_1\|) &\cdots & \phi(\|p_n - p_1\|)  \\
  \vdots & \ddots & \vdots    \\
  \phi(\|p_1 - p_n\|) &\cdots & \phi(\|p_n - p_n\|)
  \end{bmatrix}
  \begin{bmatrix}
  w_1 \\ \vdots \\ w_n
  \end{bmatrix}
  =
  \begin{bmatrix}
    \L\phi(\|p-p_1\|) \\
    \vdots \\
    \L\phi(\|p-p_n\|) \\
  \end{bmatrix},
  \end{equation*}
  Obtain $\b w_{\L, p} \approx \L|_p$.
\end{frame}

\begin{frame}{RBF-FD method: accuracy and conditioning}
  Sometimes exactly computable: {\footnotesize
  \[
    \textstyle w=\left\{\frac{4 h^2 e^{\frac{3 h^2}{\sigma ^2}}}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2},-\frac{2 \left(\sigma ^2 e^{\frac{4 h^2}{\sigma ^2}}+e^{\frac{2 h^2}{\sigma ^2}} \left(4 h^2-2 \sigma
    ^2\right)+\sigma ^2\right)}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2},\frac{4 h^2 e^{\frac{3 h^2}{\sigma ^2}}}{\sigma ^4 \left(e^{\frac{2 h^2}{\sigma ^2}}-1\right)^2}\right\}  
  \]}
  Approximation error:
  \[\textstyle
    w^\T \b u =  u''(x_0)+h^2 \left(\frac{u(x_0)}{\sigma ^4}+\frac{u''(x_0)}{\sigma ^2}+\frac{1}{12} u^{(4)}(x_0)\right)+O\left(h^3\right)
  \]
  \vspace{-4ex}
  \begin{figure}
    \includegraphics[width=0.7\linewidth]{fig/cantilever_beam_conv.pdf}
  \end{figure}
\end{frame}

\begin{frame}{RBF-FD method: accuracy and conditioning}
  Some proposed solutions: 
  
  \begin{itemize}
    \item Stable computation of $\b w_{\L, p}$
    \item Polyharmonic RBF $\phi(r) = r^k$ and monomial augmentation
  \end{itemize}
  
  \begin{figure}
    \includegraphics[width=0.8\linewidth]{fig/phs-error.pdf}
  \end{figure} 

  \vspace{-10pt} 

  Challenges: computational time, stability wrt.\ nodal positioning
\end{frame}

\begin{frame}{Fully automatic adaptivity \ijs}
  \begin{figure}
    \includegraphics[width=0.35\linewidth]{fig/disk.pdf}\ \ \
    \includegraphics[width=0.48\linewidth]{fig/half2d_convergence_v4_002.pdf}
  \end{figure}
\end{frame}


\begin{frame}{Fully automatic adaptivity: general procedure \ijs}
  \begin{enumerate} \setlength{\parskip}{10pt}
    \item Discretize domain
    \item Solve the problem
    \item Estimate error
    \item If error is below threshold, return the solution. Else:
    \item Refine the discretization
    \item Go to 2    
  \end{enumerate}
\end{frame}

\begin{frame}{Node placing}

Requirements: \vspace{-1ex}
\begin{itemize}
  \item Input: domain $\Omega$, nodal spacing \\ function
  $h(p)$ represents approx. \\ distance between $p$ and its nbrs.
  \item Output: $N$ nodes, locally regular
  \item Works with irregular domains
  \item Dimension and direction indep.
  \item Minimal spacing guarantees
\end{itemize}

\vspace{-4.5cm}
\hspace*{6.5cm} \includegraphics[width=0.4\linewidth]{fig/ff_nodes.png}

\footnotesize
[FF15] Fornberg, Bengt, and Natasha Flyer. ``Fast generation of 2-D node distributions for mesh-free PDE discretizations.'' \emph{Computers \& Mathematics with Applications} 69.7 (2015): 531-544.

[SKF18] Shankar, Varun, Robert M. Kirby, and Aaron L. Fogelson. ``Robust node generation for mesh-free discretizations on irregular domains and surfaces.'' \emph{SIAM Journal on Scientific Computing} 40.4 (2018): A2584-A2608.

\end{frame}

\begin{frame}{Node placing}
\raisebox{-1.5cm}{\includegraphics[width=0.5\linewidth]{fig/example_3d_density.png}}
\hfill
\includegraphics[width=0.2\linewidth]{fig/density_demo_plot.png}
\includegraphics[width=0.2\linewidth]{fig/sample_density_PNP.png} \\[-1cm]
\hspace*{\fill}
\includegraphics[width=0.2\linewidth]{fig/density_demo_plot_zoom.png}
\includegraphics[width=0.2\linewidth]{fig/sample_density_PNP_zoom.png}
\end{frame}

\begin{frame}{Node placing}
Future work: generalization to (parametric) surfaces

\begin{figure}
  \includegraphics[width=0.5\linewidth]{fig/earth1.png}\ \ \
  \includegraphics[width=0.4\linewidth]{fig/earth2.png}
\end{figure}
\end{frame}

\begin{frame}{Refinement}
  Testing how the method behaves with variable density.
    
  \includegraphics[width=0.49\linewidth]{fig/hertzian_manual_density.png}
  \includegraphics[width=0.49\linewidth]{fig/hertzian_solution_deformed_vm.png}
  
  \includegraphics[width=0.52\linewidth]{fig/fwo_sample_contour.png}
  \includegraphics[width=0.45\linewidth]{fig/cantilever_beam_with_holes.png}
  
  \footnotesize
  [SK19] Slak, Jure, and Gregor Kosec. ``Refined Meshless Local Strong Form solution of Cauchy–Navier equation on an irregular domain.'' \emph{Engineering analysis with boundary elements} 100 (2019): 3--13.
\end{frame}

\begin{frame}{Error indicators}
  Current work.  
  
  \includegraphics[width=0.45\linewidth]{fig/poisson_convergence_2d_sample_err.png}\ \ \
  \includegraphics[width=0.45\linewidth]{fig/poisson_convergence_2d_sample_errest.png}
  
  Until then: ad-hoc error indicators
  \[
    e_i = \sqrt{\frac{1}{n} \sum_{j \in N_i} \|u_j - u_i^\ast\|^2}, \qquad
    u_i^\ast = \frac{1}{n} \sum_{j \in N_i} u_j
  \]
\end{frame}

\begin{frame}{Fully automatic adaptivity}
\includegraphics[width=0.9\linewidth]{fig/adaptivity-sample.png}
\end{frame}

\begin{frame}{Fully automatic adaptivity}
  \includegraphics[width=0.9\linewidth]{fig/hertz-density.png}
  \includegraphics[width=0.9\linewidth]{fig/hertz-adaptive.png}
\end{frame}

\begin{frame}{Fully automatic adaptivity}
\includegraphics[width=0.9\linewidth]{fig/fwo-adaptive.png}
\end{frame}

\begin{frame}{Fully automatic adaptivity}
\includegraphics[width=0.9\linewidth]{fig/fwo-adaptive-prof.png}
\end{frame}

\begin{frame}{Fully automatic adaptivity}
\includegraphics[width=0.9\linewidth]{fig/3d-adaptive.png}

\footnotesize
[SK19] Slak, Jure, and Gregor Kosec. ``Adaptive radial basis function-generated finite differences method for contact problems.'' \emph{International Journal for Numerical Methods in Engineering} (2019).
\end{frame}

\begin{frame}{Technical aspects}
Most code is a part of an open source Medusa library. \\[4ex]

\begin{columns}  
  \begin{column}{0.2 \linewidth}
    \includegraphics[width=\linewidth]{fig/logo-medusa.png}
  \end{column}
  \begin{column}{0.7\linewidth}
    \textbf{\Large Medusa} \\ 
    Coordinate Free  Meshless Method implementation \\
    \url{http://e6.ijs.si/medusa/}
  \end{column}
\end{columns}

\vspace{6ex}

Thank you for your attention!

\vspace{3ex}
\end{frame}

\begin{frame}{References \ijs}
  [1] Fornberg, Bengt, and Natasha Flyer. ``Solving PDEs with radial basis functions.'' \emph{Acta Numerica} 24 (2015): 215-258.
  
  [2] Nguyen, Vinh Phu, et al. ``Meshless methods: a review and computer implementation aspects.'' \emph{Mathematics and computers in simulation} 79.3 (2008): 763-813.
\end{frame}

\end{document}
% vim: syntax=tex
% vim: spell spelllang=sl
% vim: foldlevel=99
% Latex template: Jure Slak, jure.slak@gmail.com

